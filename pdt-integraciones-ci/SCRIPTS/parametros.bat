@echo off
REM parse arguments in the format key=value
REM Example codename=g26 action=new lang=en:chs

SETLOCAL
SET CMDLINE=%*
SET USER=
SET PASSWORD=
SET DBNAME=
SET HOST=
SET PORT=
SET BADARGS=
SET VALIDATION=
GOTO main

:SplitArgs
  REM recursive procedure to split off the first two tokens from the input
  echo SplitArgs(%*)
  if "%*" NEQ "" (
    REM %%i = KEY, %%j = VALUE, %%k = remainder of input
    REM delimiters are space character and equals character
    for /F "tokens=1,2,* delims== " %%i in ("%*") do call :AssignKeyValue %%i %%j & call :SplitArgs %%k
  )
  goto :eof

:AssignKeyValue
  REM KEY %1, VALUE %2
  echo   AssignKeyValue(%1, %2)
  if /i %1 EQU user (
    SET USER=%2
  ) else if /i %1 EQU password (
    SET PASSWORD=%2
  ) else if /i %1 EQU dbname (
    SET DBNAME=%2
  ) else if /i %1 EQU host (
    SET HOST=%2
  ) else if /i %1 EQU port (
    SET PORT=%2
  ) else (
    REM Append unrecognised [key,value] to BADARGS
    echo Unknown KEY %1
    SET BADARGS=%BADARGS%[%1, %2]
  )
  goto :eof

:Validate
 REM VALIDATION == SUCCESS|FAIL
 echo Validating
 SET VALIDATION=FAIL
 if defined USER (
   echo   user ok
   if defined PASSWORD (
     echo   password ok
     if defined DBNAME (
       echo   dbname ok
       if defined HOST (
        echo   host ok
         if defined PORT (
          echo   port ok
            if defined BADARGS (
              echo  badargs found
            ) ELSE (
              SET VALIDATION=SUCCESS
            )
         )
       )
     )
   )
 )
 goto :eof

:main
  cls
  echo command line is %CMDLINE%
  call :SplitArgs %CMDLINE%
  call :Validate
  if "%VALIDATION%" EQU "SUCCESS" (
    echo **************
    echo Et voila
    echo using parameters %USER% %PASSWORD% %DBNAME% %HOST% %PORT%
    echo **************
  ) else (
    echo Missing or unrecognised tokens in the command line
    echo %BADARGS%
  )

pause