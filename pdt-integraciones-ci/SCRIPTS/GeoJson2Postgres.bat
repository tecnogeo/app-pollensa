@echo off
setlocal enabledelayedexpansion

::Get PG M.App Enterprise Connection
SET HOST=%1
SET USER=%2
SET PASSWORD=%3
SET DBNAME=%4
SET PORT=%5
SET PATH_GEOJSON=%6
SET ENTIDAD=%7
SET PGPASSWORD=%PASSWORD%
SET VALIDATION=SUCCESS



	::echo psql -h %HOST% -p %PORT% -U %USER% -w -d %DBNAME% -c "TRUNCATE TABLE %ENTIDAD%_TEMP;"
	psql -h %HOST% -p %PORT% -U %USER% -w -d %DBNAME% -c "TRUNCATE TABLE %ENTIDAD%_TEMP;" && (
		
		::echo ogr2ogr -append -f PostgreSQL "PG:dbname=%DBNAME% user=%USER% password=%PASSWORD% host=%HOST%" "%PATH_GEOJSON%\%ENTIDAD%.geojson" -nln "%%~nf_TEMP"
		ogr2ogr -append -f PostgreSQL "PG:dbname=%DBNAME% user=%USER% password=%PASSWORD% host=%HOST%" "%PATH_GEOJSON%\%ENTIDAD%.geojson" -nln "%ENTIDAD%_TEMP"	&& (
			echo command 2 success
		) || (
			echo command 2 fail
			SET VALIDATION=FAIL
		)
	) || (
		echo command 1 fail
		SET VALIDATION=FAIL
	)	


if "%VALIDATION%" EQU "SUCCESS" (
	echo %VALIDATION%
	exit 0
) else (
	echo %VALIDATION%
	exit 999
)