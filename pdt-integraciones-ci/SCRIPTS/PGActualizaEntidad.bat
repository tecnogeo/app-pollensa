::Get PG M.App Enterprise Connection
@echo off

SET HOST=%1
SET USER=%2
SET PASSWORD=%3
SET DBNAME=%4
SET PORT=%5
SET ENTIDADID=%6
SET PGPASSWORD=%PASSWORD%
SET VALIDATION=SUCCESS
SET RESULT=


::psql -h %HOST% -p %PORT% -U %USER% -w -d %DBNAME% -c "SELECT public.f_actualizaDatos();"
FOR /F "tokens=* USEBACKQ" %%F IN (`psql -h %HOST% -p %PORT% -U %USER% -w -d %DBNAME% -t -c "SELECT public.f_actualizaDatosEntidad(%ENTIDADID%);"`) DO (
	SET RESULT=%%F
)
ECHO %RESULT%

IF "%RESULT%" == "f" (
	SET VALIDATION=FAIL
)

if "%VALIDATION%" EQU "SUCCESS" (
	echo %VALIDATION%
	exit 0
) else (
	echo %VALIDATION%
	exit 999
)
