@echo off
setlocal enabledelayedexpansion

::Get PG M.App Enterprise Connection
SET HOST=%1
SET USER=%2
SET PASSWORD=%3
SET DBNAME=%4
SET PORT=%5
SET PATH_GEOJSON=%6
SET PGPASSWORD=%PASSWORD%
SET VALIDATION=SUCCESS

for %%f in (%PATH_GEOJSON%"\*.geojson") do (

    ::echo psql -h %HOST% -p %PORT% -U %USER% -w -d %DBNAME% -c "TRUNCATE TABLE %%~nf_TEMP;"
    psql -h %HOST% -p %PORT% -U %USER% -w -d %DBNAME% -c "TRUNCATE TABLE %%~nf_TEMP;" && (
		
		::echo ogr2ogr -append -f PostgreSQL "PG:dbname=%DBNAME% user=%USER% password=%PASSWORD% host=%HOST%" "%%f" -nln "%%~nf_TEMP"
		ogr2ogr -append -f PostgreSQL "PG:dbname=%DBNAME% user=%USER% password=%PASSWORD% host=%HOST%" "%%f" -nln "%%~nf_TEMP"	&& (
			echo command 2 success
		) || (
			echo command 2 fail
			SET VALIDATION=FAIL
		)
	) || (
		echo command 1 fail
		SET VALIDATION=FAIL
	)	
)


if "%VALIDATION%" EQU "SUCCESS" (
	echo %VALIDATION%
	exit 0
) else (
	echo %VALIDATION%
	exit 999
)