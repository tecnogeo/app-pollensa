CREATE OR REPLACE FUNCTION f_ins_identificador_year_seq()
  RETURNS text AS
$$
DECLARE
  current_year text = to_char(now(), 'YYYY');
  last_row_year text;
  identificador text;
BEGIN


LOOP
   BEGIN
      RETURN current_year ||'-'|| to_char(nextval('ins_identificador_year_'|| current_year ||'_seq'), 'FM0000000');

   EXCEPTION WHEN undefined_table THEN   -- error code 42P01
      EXECUTE 'CREATE SEQUENCE ins_identificador_year_' || current_year || '_seq MINVALUE 1 START 1';
   END;
END LOOP;


EXCEPTION 
    WHEN others THEN        
        RAISE WARNING '% - %', SQLERRM, SQLSTATE;
		RETURN 'WARNING '|| SQLERRM || ' - ' || SQLSTATE;
END;
$$ LANGUAGE plpgsql;



DO $$
DECLARE
   epsg integer;
   q text;
   
BEGIN

    SELECT c.epsg into epsg from config_cliente c;



   /*==============================================================*/
   /* Tabla: INSPECCIONES                                          */
   /*==============================================================*/

   q := 'CREATE table INSPECCIONES (
            INS_UID                    CHARACTER(36)              not null,
            INS_IDENTIFICADOR          CHAR(12)                   not null DEFAULT f_ins_identificador_year_seq(),
            TIPO_ENTIDAD_ID            INTEGER                    REFERENCES TIPOS_ENTIDAD(TIPO_ENTIDAD_ID),
            ENT_MERCADOS_UID           CHARACTER(36)              REFERENCES MERCADOS(ENT_UID),
            ENT_TERRAZAS_UID           CHARACTER(36)              REFERENCES TERRAZAS(ENT_UID),
            ENT_OBRAS_UID              CHARACTER(36)              REFERENCES OBRAS(ENT_UID),
            ENT_OVP_UID                CHARACTER(36)              REFERENCES OVP(ENT_UID),
            ENT_ACTIVIDADES_UID        CHARACTER(36)              REFERENCES ACTIVIDADES(ENT_UID),
            INS_FECHA_REVISION         TIMESTAMP                  null,
            INS_FECHA_INSPECCION       TIMESTAMP                  null,
            INS_TECNICO                CHARACTER VARYING(50)      null,
            INS_UBICACION              GEOMETRY(POINT,'||epsg||') null,
            INS_UBICACION_PREC         NUMERIC                    null,
            INS_REPRESENTANTE          CHARACTER VARYING(50)      null,
            INS_TIPO_DOCUMENTO         CHARACTER VARYING(50)      null,
            INS_NUM_DOCUMENTO          CHARACTER VARYING(20)      null,
            INS_EMAIL_REPRESENTANTE    CHARACTER VARYING(255)     null,
            INS_ASISTENCIA             INTEGER                    REFERENCES TIPOS_ASISTENCIA_INSP (ASISTENCIA_ID),
            INS_ASISTENCIA_DESC        CHARACTER VARYING(1000)    null,
            INS_MODIFICACIONES_ESTADO  INTEGER                    REFERENCES TIPOS_MODIFICACIONES_INSP(MOD_ID),
            INS_MOD_DATOS_PERS         CHARACTER VARYING(1000)    null,
            INS_REVISION               BOOLEAN                    null,
            INS_REVISION_DESC          CHARACTER VARYING(1000)    null,
            INS_OBSERVACIONES          CHARACTER VARYING(1000)    null,
            INS_PDF_GENERADO           BOOLEAN                    null,
            INS_JSON_GENERADO          BOOLEAN                    null,
            INS_ENVIADO_GEWEB          BOOLEAN                    null,
            INS_FIRMADO                BOOLEAN                    null,
            INS_DOCGUI_FIRMA           CHARACTER(36)              null,
            INS_DEVICENAME_FIRMA       CHARACTER VARYING(255)     null,
            INS_FINALIZADA             BOOLEAN                    REFERENCES TIPOS_ESTADOS_INSP(ESTADO_ID),
            INS_IMG_1                  BYTEA                      null,
            INS_IMG_1_SIZE             NUMERIC                    null,
            INS_IMG_2                  BYTEA                      null,
            INS_IMG_2_SIZE             NUMERIC                    null,
            INS_IMG_3                  BYTEA                      null,
            INS_IMG_3_SIZE             NUMERIC                    null,
            
            INS_TERRAZAS_MESAS         INTEGER                    null,
            INS_TERRAZAS_SILLAS        INTEGER                    null,
            INS_TERRAZAS_SUPERFICIE    NUMERIC                    null,
            INS_TERRAZAS_OTROS         CHARACTER VARYING(255)     null,
            INS_OBRAS_ESTADO           CHARACTER VARYING(255)     null,
            
            INS_GENERICA               BOOLEAN                    null,

            INS_INFRACCION             BOOLEAN                    null,
            INS_INF_001                BOOLEAN                    null,
            INS_INF_002                BOOLEAN                    null,
            INS_INF_003                BOOLEAN                    null,
            INS_INF_004                BOOLEAN                    null,
            INS_INF_005                BOOLEAN                    null,
            INS_INF_006                BOOLEAN                    null,
            INS_INF_007                BOOLEAN                    null,
            INS_INF_008                BOOLEAN                    null,
            INS_INF_009                BOOLEAN                    null,
            INS_INF_010                BOOLEAN                    null,
            INS_INF_011                BOOLEAN                    null,
            INS_INF_012                BOOLEAN                    null,
            INS_INF_013                BOOLEAN                    null,
            INS_INF_014                BOOLEAN                    null,
            INS_INF_015                BOOLEAN                    null,
            INS_INF_016                BOOLEAN                    null,
            INS_INF_017                BOOLEAN                    null,
            INS_INF_018                BOOLEAN                    null,
            INS_INF_019                BOOLEAN                    null,
            INS_INF_020                BOOLEAN                    null,
            INS_INF_021                BOOLEAN                    null,
            INS_INF_022                BOOLEAN                    null,
            INS_INF_023                BOOLEAN                    null,
            INS_INF_024                BOOLEAN                    null,
            INS_INF_025                BOOLEAN                    null,
            INS_INF_026                BOOLEAN                    null,
            INS_INF_027                BOOLEAN                    null,
            INS_INF_028                BOOLEAN                    null,
            INS_INF_029                BOOLEAN                    null,
            INS_INF_030                BOOLEAN                    null,
            INS_INF_031                BOOLEAN                    null,
            INS_INF_032                BOOLEAN                    null,
            INS_INF_033                BOOLEAN                    null,
            INS_INF_034                BOOLEAN                    null,
            INS_INF_035                BOOLEAN                    null,
            constraint PK_INSPECCIONES primary key (INS_UID)
            );';

   EXECUTE q;

   CREATE INDEX IDX_GEOM_INSPECCIONES ON INSPECCIONES USING GIST (INS_UBICACION);


END $$;