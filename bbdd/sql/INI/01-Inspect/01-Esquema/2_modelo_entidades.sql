/*=============================================================================================*/
/*=============================================================================================*/
/* ENTIDADES                                                                                    */
/*=============================================================================================*/
/*=============================================================================================*/

DO $$ 
DECLARE    
    epsg INTEGER;
    sql_create_entity text;    

BEGIN 

   SELECT c.epsg into epsg from config_cliente c;

   /*==============================================================*/
   /* Tabla: MERCADOS                                              */
   /*==============================================================*/
   sql_create_entity := 'CREATE table MERCADOS (
                           ENT_UID                       CHARACTER(36)                   not null,
                           ENT_FECHA_REVISION            TIMESTAMP                       null,
                           ENT_ULTIMA_INSP               TIMESTAMP                       null,
                           ENT_VIGENCIA                  INTEGER                         null,
                           AA_IDA                        INTEGER                         not null,
                           AA_GEOM                       Geometry(Polygon,'||epsg||')    null,
                           AA_DES                        CHARACTER VARYING(100)          null,
                           ID_ELEMENTO                   CHARACTER VARYING(40)           not null,
                           TIPO_ELEM                     CHARACTER VARYING(40)           null,
                           EXP_CODIGO_INTERNO            CHARACTER VARYING(255)          null,
                           EXP_DESCRIPCION               CHARACTER VARYING(100)          null,
                           EXP_DIRECCION                 CHARACTER VARYING(255)          null,
                           EXP_DIRECCION_NOTIFICACION    CHARACTER VARYING(255)          null,
                           EXP_ESTADO_EXPEDIENTE         INTEGER                         not null,
                           EXP_ESTADO_EXPEDIENTE_DESC    CHARACTER VARYING(100)          null,
                           EXP_FECHA_ALTA                TIMESTAMP                       null,
                           EXP_FECHA_BAJA                TIMESTAMP                       null,
                           EXP_INT_EMAIL                 CHARACTER VARYING(255)          null,
                           EXP_INT_TELEF                 CHARACTER VARYING(12)           null,
                           EXP_NOMBRE_COMPLETO           CHARACTER VARYING(100)          null,
                           EXP_NUMERO                    CHARACTER VARYING(25)           not null,
                           EXP_NUMERO_DOCUMENTO          CHARACTER VARYING(10)           null,
                           EXP_OBSERVACIONES             CHARACTER VARYING(1000)         null,
                           EXP_REFCAT                    CHARACTER VARYING(20)           null,
                           EXP_REFCAT_OTRAS              CHARACTER VARYING(1000)         null,
                           EXP_TIPO_DOCUMENTO            CHARACTER VARYING(50)           null,
                           EXP_TIPO_PERSONA              CHARACTER VARYING(100)          null,
                           EXP_MER_CODIGO                CHARACTER VARYING(15)           null,
                           EXP_MER_BAJA                  BOOLEAN                         null,
                           EXP_MER_BAJA_OBS              CHARACTER VARYING(500)          null,
                           EXP_MER_PAGO                  BOOLEAN                         null,
                           EXP_MER_TIPO                  CHARACTER VARYING(100)          null,                           
                           EXP_MER_IMG_AUTORIZADO        BYTEA                           null,
                           ENT_TEXTO_1                   CHARACTER VARYING(255)          null,
                           ENT_TEXTO_2                   CHARACTER VARYING(255)          null,
                           ENT_TEXTO_3                   CHARACTER VARYING(255)          null,
                           ENT_NUM_1                     NUMERIC                         null,
                           ENT_NUM_2                     NUMERIC                         null,
                           ENT_NUM_3                     NUMERIC                         null,
                           ENT_FECHA_1                   TIMESTAMP                       null,
                           ENT_FECHA_2                   TIMESTAMP                       null,
                           ENT_FECHA_3                   TIMESTAMP                       null,
                           constraint PK_MERCADOS primary key (ENT_UID)
                        );';

   EXECUTE sql_create_entity;

   CREATE INDEX IDX_GEOM_MERCADOS ON MERCADOS USING GIST (AA_GEOM);



   /*==============================================================*/
   /* Tabla: MERCADOS_TEMP                                           */
   /*==============================================================*/
   create table MERCADOS_TEMP (
      OGC_FID                       SERIAL                          not null,
      AA_IDA                        INTEGER                         not null,
      AA_GEOM                       Geometry(Polygon, 4326)         null,
      AA_DES                        CHARACTER VARYING(100)          null,   
      ID_ELEMENTO                   CHARACTER VARYING(40)           not null,   
      TIPO_ELEM                     CHARACTER VARYING(40)           null,
      EXP_CODIGO_INTERNO            CHARACTER VARYING(255)          null,
      EXP_DESCRIPCION               CHARACTER VARYING(100)          null,
      EXP_DIRECCION                 CHARACTER VARYING(255)          null,
      EXP_DIRECCION_NOTIFICACION    CHARACTER VARYING(255)          null,
      EXP_ESTADO_EXPEDIENTE         INTEGER                         not null,
      EXP_ESTADO_EXPEDIENTE_DESC    CHARACTER VARYING(100)          null,
      EXP_FECHA_ALTA                TIMESTAMP                       null,
      EXP_FECHA_BAJA                TIMESTAMP                       null,
      EXP_INT_EMAIL                 CHARACTER VARYING(255)          null,
      EXP_INT_TELEF                 CHARACTER VARYING(12)           null,
      EXP_NOMBRE_COMPLETO           CHARACTER VARYING(100)          null,
      EXP_NUMERO                    CHARACTER VARYING(25)           not null,
      EXP_NUMERO_DOCUMENTO          CHARACTER VARYING(10)           null,
      EXP_OBSERVACIONES             CHARACTER VARYING(1000)         null,
      EXP_REFCAT                    CHARACTER VARYING(20)           null,
      EXP_REFCAT_OTRAS              CHARACTER VARYING(1000)         null,
      EXP_TIPO_DOCUMENTO            CHARACTER VARYING(50)           null,
      EXP_TIPO_PERSONA              CHARACTER VARYING(100)          null,
      EXP_MER_CODIGO                CHARACTER VARYING(15)           null,
      EXP_MER_BAJA                  BOOLEAN                         null,
      EXP_MER_BAJA_OBS              CHARACTER VARYING(500)          null,
      EXP_MER_PAGO                  BOOLEAN                         null,
      EXP_MER_TIPO                  CHARACTER VARYING(100)          null,
      EXP_MER_IMG_AUTORIZADO        CHARACTER VARYING               null,
      ENT_TEXTO_1                   CHARACTER VARYING(255)          null,
      ENT_TEXTO_2                   CHARACTER VARYING(255)          null,
      ENT_TEXTO_3                   CHARACTER VARYING(255)          null,
      ENT_NUM_1                     NUMERIC                         null,
      ENT_NUM_2                     NUMERIC                         null,
      ENT_NUM_3                     NUMERIC                         null,
      ENT_FECHA_1                   TIMESTAMP                       null,
      ENT_FECHA_2                   TIMESTAMP                       null,
      ENT_FECHA_3                   TIMESTAMP                       null,
      constraint PK_MERCADOS_TEMP primary key (OGC_FID)
   );


   /*==============================================================*/
   /* Tabla: TERRAZAS                                              */
   /*==============================================================*/
   sql_create_entity := 'CREATE table TERRAZAS (
                           ENT_UID                       CHARACTER(36)                   not null,
                           ENT_FECHA_REVISION            TIMESTAMP                       null,
                           ENT_ULTIMA_INSP               TIMESTAMP                       null,
                           ENT_VIGENCIA                  INTEGER                         null,
                           AA_IDA                        INTEGER                         not null,
                           AA_GEOM                       Geometry(Polygon,'||epsg||')    null,                           
                           AA_DES                        CHARACTER VARYING(100)          null,
                           ID_ELEMENTO                   CHARACTER VARYING(40)           not null,
                           TIPO_ELEM                     CHARACTER VARYING(40)           null,
                           EXP_CODIGO_INTERNO            CHARACTER VARYING(255)          null,
                           EXP_DESCRIPCION               CHARACTER VARYING(100)          null,
                           EXP_DIRECCION                 CHARACTER VARYING(255)          null,
                           EXP_DIRECCION_NOTIFICACION    CHARACTER VARYING(255)          null,
                           EXP_ESTADO_EXPEDIENTE         INTEGER                         not null,
                           EXP_ESTADO_EXPEDIENTE_DESC    CHARACTER VARYING(100)          null,
                           EXP_FECHA_ALTA                TIMESTAMP                       null,
                           EXP_FECHA_BAJA                TIMESTAMP                       null,
                           EXP_INT_EMAIL                 CHARACTER VARYING(255)          null,
                           EXP_INT_TELEF                 CHARACTER VARYING(12)           null,
                           EXP_NOMBRE_COMPLETO           CHARACTER VARYING(100)          null,
                           EXP_NUMERO                    CHARACTER VARYING(25)           not null,
                           EXP_NUMERO_DOCUMENTO          CHARACTER VARYING(10)           null,
                           EXP_OBSERVACIONES             CHARACTER VARYING(1000)         null,
                           EXP_REFCAT                    CHARACTER VARYING(20)           null,
                           EXP_REFCAT_OTRAS              CHARACTER VARYING(1000)         null,
                           EXP_TIPO_DOCUMENTO            CHARACTER VARYING(50)           null,
                           EXP_TIPO_PERSONA              CHARACTER VARYING(100)          null,
                           EXP_TERR_NOMBRE_LOCAL         CHARACTER VARYING(100)          null,
                           EXP_TERR_REPR_NOMBRE          CHARACTER VARYING(100)          null,
                           EXP_TERR_REPR_TIPO_DOC        CHARACTER VARYING(50)           null,
                           EXP_TERR_REPR_NUMERO_DOC      CHARACTER VARYING(10)           null,
                           EXP_TERR_REPR_TELEFONO        CHARACTER VARYING(12)           null,
                           TERRAZA_CODIGO_SOLICITUD      CHARACTER VARYING(50)           not null,
                           TERRAZA_FECHA_INI             TIMESTAMP                       null,
                           TERRAZA_FECHA_FIN             TIMESTAMP                       null,
                           TERRAZA_SUPERFICIE_AUT        NUMERIC                         null,
                           TERRAZA_MESAS_AUT             INTEGER                         null,
                           TERRAZA_SILLAS_AUT            INTEGER                         null,
                           TERRAZA_OTROS_ELEM            CHARACTER VARYING(100)          null,
                           TERRAZA_UBICACION             CHARACTER VARYING(100)          null,
                           TERRAZA_OBSERVACIONES         CHARACTER VARYING(255)          null,
                           TERRAZA_ESTADO_SOLICITUD      CHARACTER VARYING(50)           null,
                           TERRAZA_MOTIVO_DENEGACION     CHARACTER VARYING(255)          null,
                           ENT_TEXTO_1                   CHARACTER VARYING(255)          null,
                           ENT_TEXTO_2                   CHARACTER VARYING(255)          null,
                           ENT_TEXTO_3                   CHARACTER VARYING(255)          null,
                           ENT_NUM_1                     NUMERIC                         null,
                           ENT_NUM_2                     NUMERIC                         null,
                           ENT_NUM_3                     NUMERIC                         null,
                           ENT_FECHA_1                   TIMESTAMP                       null,
                           ENT_FECHA_2                   TIMESTAMP                       null,
                           ENT_FECHA_3                   TIMESTAMP                       null,
                           constraint PK_TERRAZAS primary key (ENT_UID)
                        );';

   EXECUTE sql_create_entity;
   CREATE INDEX IDX_GEOM_TERRAZAS ON TERRAZAS USING GIST (AA_GEOM);




   /*==============================================================*/
   /* Table: TERRAZAS_TEMP                                           */
   /*==============================================================*/
   create table TERRAZAS_TEMP (
      OGC_FID                     SERIAL                          not null,
      AA_IDA                      INTEGER                         not null,
      AA_GEOM                     Geometry(Polygon, 4326)         null,
      AA_DES                      CHARACTER VARYING(100)          null,   
      ID_ELEMENTO                 CHARACTER VARYING(40)           not null,   
      TIPO_ELEM                   CHARACTER VARYING(40)           null,
      EXP_CODIGO_INTERNO          CHARACTER VARYING(255)          null,
      EXP_DESCRIPCION             CHARACTER VARYING(100)          null,
      EXP_DIRECCION               CHARACTER VARYING(255)          null,
      EXP_DIRECCION_NOTIFICACION  CHARACTER VARYING(255)          null,
      EXP_ESTADO_EXPEDIENTE       INTEGER                         not null,
      EXP_ESTADO_EXPEDIENTE_DESC  CHARACTER VARYING(100)          null,
      EXP_FECHA_ALTA              TIMESTAMP                       null,
      EXP_FECHA_BAJA              TIMESTAMP                       null,
      EXP_INT_EMAIL               CHARACTER VARYING(255)          null,
      EXP_INT_TELEF               CHARACTER VARYING(12)           null,
      EXP_NOMBRE_COMPLETO         CHARACTER VARYING(100)          null,
      EXP_NUMERO                  CHARACTER VARYING(25)           not null,
      EXP_NUMERO_DOCUMENTO        CHARACTER VARYING(10)           null,
      EXP_OBSERVACIONES           CHARACTER VARYING(1000)         null,
      EXP_REFCAT                  CHARACTER VARYING(20)           null,
      EXP_REFCAT_OTRAS            CHARACTER VARYING(1000)         null,
      EXP_TIPO_DOCUMENTO          CHARACTER VARYING(50)           null,
      EXP_TIPO_PERSONA            CHARACTER VARYING(100)          null,
      EXP_TERR_NOMBRE_LOCAL       CHARACTER VARYING(100)          null,
      EXP_TERR_REPR_NOMBRE        CHARACTER VARYING(100)          null,
      EXP_TERR_REPR_TIPO_DOC      CHARACTER VARYING(50)           null,
      EXP_TERR_REPR_NUMERO_DOC    CHARACTER VARYING(10)           null,
      EXP_TERR_REPR_TELEFONO      CHARACTER VARYING(12)           null,
      TERRAZA_CODIGO_SOLICITUD    CHARACTER VARYING(50)           not null,
      TERRAZA_FECHA_INI           TIMESTAMP                       null,
      TERRAZA_FECHA_FIN           TIMESTAMP                       null,
      TERRAZA_SUPERFICIE_AUT      NUMERIC                         null,
      TERRAZA_MESAS_AUT           INTEGER                         null,
      TERRAZA_SILLAS_AUT          INTEGER                         null,
      TERRAZA_OTROS_ELEM          CHARACTER VARYING(100)          null,
      TERRAZA_UBICACION           CHARACTER VARYING(100)          null,
      TERRAZA_OBSERVACIONES       CHARACTER VARYING(255)          null,
      TERRAZA_ESTADO_SOLICITUD    CHARACTER VARYING(50)           null,
      TERRAZA_MOTIVO_DENEGACION   CHARACTER VARYING(255)          null,
      ENT_TEXTO_1                 CHARACTER VARYING(255)          null,
      ENT_TEXTO_2                 CHARACTER VARYING(255)          null,
      ENT_TEXTO_3                 CHARACTER VARYING(255)          null,
      ENT_NUM_1                   NUMERIC                         null,
      ENT_NUM_2                   NUMERIC                         null,
      ENT_NUM_3                   NUMERIC                         null,
      ENT_FECHA_1                 TIMESTAMP                       null,
      ENT_FECHA_2                 TIMESTAMP                       null,
      ENT_FECHA_3                 TIMESTAMP                       null,
      constraint PK_TERRAZAS_TEMP primary key (OGC_FID)
   );






   /*==============================================================*/
   /* Tabla: OBRAS                                                 */
   /*==============================================================*/
   sql_create_entity := 'CREATE table OBRAS (
                           ENT_UID                       CHARACTER(36)                   not null,
                           ENT_FECHA_REVISION            TIMESTAMP                       null,
                           ENT_ULTIMA_INSP               TIMESTAMP                       null,
                           ENT_VIGENCIA                  INTEGER                         null,
                           AA_IDA                        INTEGER                         not null,
                           AA_GEOM                       Geometry(Point,'||epsg||')      null,                          
                           AA_DES                        CHARACTER VARYING(100)          null,
                           ID_ELEMENTO                   CHARACTER VARYING(40)           not null,
                           TIPO_ELEM                     CHARACTER VARYING(40)           null,
                           EXP_CODIGO_INTERNO            CHARACTER VARYING(255)          null,
                           EXP_DESCRIPCION               CHARACTER VARYING(100)          null,
                           EXP_DIRECCION                 CHARACTER VARYING(255)          null,
                           EXP_DIRECCION_NOTIFICACION    CHARACTER VARYING(255)          null,
                           EXP_ESTADO_EXPEDIENTE         INTEGER                         not null,
                           EXP_ESTADO_EXPEDIENTE_DESC    CHARACTER VARYING(100)          null,
                           EXP_FECHA_ALTA                TIMESTAMP                       null,
                           EXP_FECHA_BAJA                TIMESTAMP                       null,
                           EXP_INT_EMAIL                 CHARACTER VARYING(255)          null,
                           EXP_INT_TELEF                 CHARACTER VARYING(12)           null,
                           EXP_NOMBRE_COMPLETO           CHARACTER VARYING(100)          null,
                           EXP_NUMERO                    CHARACTER VARYING(25)           not null,
                           EXP_NUMERO_DOCUMENTO          CHARACTER VARYING(10)           null,
                           EXP_OBSERVACIONES             CHARACTER VARYING(1000)         null,
                           EXP_REFCAT                    CHARACTER VARYING(20)           null,
                           EXP_REFCAT_OTRAS              CHARACTER VARYING(1000)         null,
                           EXP_TIPO_DOCUMENTO            CHARACTER VARYING(50)           null,
                           EXP_TIPO_PERSONA              CHARACTER VARYING(100)          null,
                           EXP_OBR_REPR_NOMBRE           CHARACTER VARYING(100)          null,
                           EXP_OBR_REPR_TIPO_DOC         CHARACTER VARYING(50)           null,
                           EXP_OBR_REPR_NUMERO_DOC       CHARACTER VARYING(10)           null,
                           EXP_OBR_REPR_TELEFONO         CHARACTER VARYING(12)           null,
                           EXP_OBR_REPR_EMAIL            CHARACTER VARYING(100)          null,
                           EXP_OBR_REPR_DIRECCION_NOTIF  CHARACTER VARYING(255)          null,
                           EXP_OBR_TIPO                  CHARACTER VARYING(50)           null,
                           EXP_OBR_CONSISTE_EN           CHARACTER VARYING(150)          null,
                           EXP_OBR_FECHA_NOTI            TIMESTAMP                       null, 
                           EXP_OBR_FECHA_FIN             TIMESTAMP                       null,
                           ENT_TEXTO_1                   CHARACTER VARYING(255)          null,
                           ENT_TEXTO_2                   CHARACTER VARYING(255)          null,
                           ENT_TEXTO_3                   CHARACTER VARYING(255)          null,
                           ENT_NUM_1                     NUMERIC                         null,
                           ENT_NUM_2                     NUMERIC                         null,
                           ENT_NUM_3                     NUMERIC                         null,
                           ENT_FECHA_1                   TIMESTAMP                       null,
                           ENT_FECHA_2                   TIMESTAMP                       null,
                           ENT_FECHA_3                   TIMESTAMP                       null,                           
                           constraint PK_OBRAS primary key (ENT_UID)
                        );';

   EXECUTE sql_create_entity;

   CREATE INDEX IDX_GEOM_OBRAS ON OBRAS USING GIST (AA_GEOM);


   /*==============================================================*/
   /* Table: OBRAS_TEMP                                            */
   /*==============================================================*/
   create table OBRAS_TEMP (
      OGC_FID                       SERIAL                          not null,
      AA_IDA                        INTEGER                         not null,
      AA_GEOM                       Geometry(Point, 4326)           null,
      AA_DES                        CHARACTER VARYING(100)          null,   
      ID_ELEMENTO                   CHARACTER VARYING(40)           not null,   
      TIPO_ELEM                     CHARACTER VARYING(40)           null,
      EXP_CODIGO_INTERNO            CHARACTER VARYING(255)          null,
      EXP_DESCRIPCION               CHARACTER VARYING(100)          null,
      EXP_DIRECCION                 CHARACTER VARYING(255)          null,
      EXP_DIRECCION_NOTIFICACION    CHARACTER VARYING(255)          null,
      EXP_ESTADO_EXPEDIENTE         INTEGER                         not null,
      EXP_ESTADO_EXPEDIENTE_DESC    CHARACTER VARYING(100)          null,
      EXP_FECHA_ALTA                TIMESTAMP                       null,
      EXP_FECHA_BAJA                TIMESTAMP                       null,
      EXP_INT_EMAIL                 CHARACTER VARYING(255)          null,
      EXP_INT_TELEF                 CHARACTER VARYING(12)           null,
      EXP_NOMBRE_COMPLETO           CHARACTER VARYING(100)          null,
      EXP_NUMERO                    CHARACTER VARYING(25)           not null,
      EXP_NUMERO_DOCUMENTO          CHARACTER VARYING(10)           null,
      EXP_OBSERVACIONES             CHARACTER VARYING(1000)         null,
      EXP_REFCAT                    CHARACTER VARYING(20)           null,
      EXP_REFCAT_OTRAS              CHARACTER VARYING(1000)         null,
      EXP_TIPO_DOCUMENTO            CHARACTER VARYING(50)           null,
      EXP_TIPO_PERSONA              CHARACTER VARYING(100)          null,
      EXP_OBR_REPR_NOMBRE           CHARACTER VARYING(100)          null,
      EXP_OBR_REPR_TIPO_DOC         CHARACTER VARYING(50)           null,
      EXP_OBR_REPR_NUMERO_DOC       CHARACTER VARYING(10)           null,
      EXP_OBR_REPR_TELEFONO         CHARACTER VARYING(12)           null,
      EXP_OBR_REPR_EMAIL            CHARACTER VARYING(100)          null,
      EXP_OBR_REPR_DIRECCION_NOTIF  CHARACTER VARYING(255)          null,
      EXP_OBR_TIPO                  CHARACTER VARYING(50)           null,
      EXP_OBR_CONSISTE_EN           CHARACTER VARYING(150)          null,
      EXP_OBR_FECHA_NOTI            TIMESTAMP                       null,
      EXP_OBR_FECHA_FIN             TIMESTAMP                       null,
      ENT_TEXTO_1                   CHARACTER VARYING(255)          null,
      ENT_TEXTO_2                   CHARACTER VARYING(255)          null,
      ENT_TEXTO_3                   CHARACTER VARYING(255)          null,
      ENT_NUM_1                     NUMERIC                         null,
      ENT_NUM_2                     NUMERIC                         null,
      ENT_NUM_3                     NUMERIC                         null,
      ENT_FECHA_1                   TIMESTAMP                       null,
      ENT_FECHA_2                   TIMESTAMP                       null,
      ENT_FECHA_3                   TIMESTAMP                       null,          
      constraint PK_OBRAS_TEMP primary key (OGC_FID)
   );




   /*==============================================================*/
   /* Tabla: OVP                                                   */
   /*==============================================================*/
   sql_create_entity := 'CREATE table OVP (
                           ENT_UID                          CHARACTER(36)                   not null,
                           ENT_FECHA_REVISION               TIMESTAMP                       null,
                           ENT_ULTIMA_INSP                  TIMESTAMP                       null,
                           ENT_VIGENCIA                     INTEGER                         null,
                           AA_IDA                           INTEGER                         not null,
                           AA_GEOM                          Geometry(Polygon,'||epsg||')    null,                           
                           AA_DES                           CHARACTER VARYING(100)          null,
                           ID_ELEMENTO                      CHARACTER VARYING(40)           not null,
                           TIPO_ELEM                        CHARACTER VARYING(40)           null,
                           EXP_CODIGO_INTERNO               CHARACTER VARYING(255)          null,
                           EXP_DESCRIPCION                  CHARACTER VARYING(100)          null,
                           EXP_DIRECCION                    CHARACTER VARYING(255)          null,
                           EXP_DIRECCION_NOTIFICACION       CHARACTER VARYING(255)          null,
                           EXP_ESTADO_EXPEDIENTE            INTEGER                         not null,
                           EXP_ESTADO_EXPEDIENTE_DESC       CHARACTER VARYING(100)          null,
                           EXP_FECHA_ALTA                   TIMESTAMP                       null,
                           EXP_FECHA_BAJA                   TIMESTAMP                       null,
                           EXP_INT_EMAIL                    CHARACTER VARYING(255)          null,
                           EXP_INT_TELEF                    CHARACTER VARYING(12)           null,
                           EXP_NOMBRE_COMPLETO              CHARACTER VARYING(100)          null,
                           EXP_NUMERO                       CHARACTER VARYING(25)           not null,
                           EXP_NUMERO_DOCUMENTO             CHARACTER VARYING(10)           null,
                           EXP_OBSERVACIONES                CHARACTER VARYING(1000)         null,
                           EXP_REFCAT                       CHARACTER VARYING(20)           null,
                           EXP_REFCAT_OTRAS              CHARACTER VARYING(1000)            null,
                           EXP_TIPO_DOCUMENTO               CHARACTER VARYING(50)           null,
                           EXP_TIPO_PERSONA                 CHARACTER VARYING(100)          null,
                           EXP_OVP_TIPO_OCUPACION           CHARACTER VARYING(50)           null,
                           EXP_OVP_MOTIVO                   CHARACTER VARYING(150)          null,
                           EXP_OVP_TIPO_VEHICULO            CHARACTER VARYING(50)           null,
                           EXP_OVP_REF_LICENCIA             CHARACTER VARYING(20)           null,
                           EXP_OVP_CARRIL_LARGO             NUMERIC                         null,
                           EXP_OVP_CARRIL_ANCHO             NUMERIC                         null,    
                           EXP_OVP_CARRIL_FECHA_INI         TIMESTAMP                       null,
                           EXP_OVP_CARRIL_FECHA_FIN         TIMESTAMP                       null,
                           EXP_OVP_CARRIL_CORTE             BOOLEAN                         null,
                           EXP_OVP_CARRIL_CORTE_DESC        CHARACTER VARYING(200)          null,
                           EXP_OVP_APARCAMIENTO_LARGO       NUMERIC                         null,
                           EXP_OVP_APARCAMIENTO_ANCHO       NUMERIC                         null,
                           EXP_OVP_APARCAMIENTO_FECHA_INI   TIMESTAMP                       null,
                           EXP_OVP_APARCAMIENTO_FECHA_FIN   TIMESTAMP                       null,
                           EXP_OVP_ACERA_LARGO              NUMERIC                         null,
                           EXP_OVP_ACERA_ANCHO              NUMERIC                         null,
                           EXP_OVP_ACERA_FECHA_INI          TIMESTAMP                       null,
                           EXP_OVP_ACERA_FECHA_FIN          TIMESTAMP                       null,
                           ENT_TEXTO_1                      CHARACTER VARYING(255)          null,
                           ENT_TEXTO_2                      CHARACTER VARYING(255)          null,
                           ENT_TEXTO_3                      CHARACTER VARYING(255)          null,
                           ENT_NUM_1                        NUMERIC                         null,
                           ENT_NUM_2                        NUMERIC                         null,
                           ENT_NUM_3                        NUMERIC                         null,
                           ENT_FECHA_1                      TIMESTAMP                       null,
                           ENT_FECHA_2                      TIMESTAMP                       null,
                           ENT_FECHA_3                      TIMESTAMP                       null,
                           constraint PK_OVP primary key (ENT_UID)
                        );';

   EXECUTE sql_create_entity;

   CREATE INDEX IDX_GEOM_OVP ON OVP USING GIST (AA_GEOM);



   /*==============================================================*/
   /* Table: OVP_TEMP                                              */
   /*==============================================================*/
   create table OVP_TEMP (
      OGC_FID                          SERIAL                          not null,
      AA_IDA                           INTEGER                         not null,
      AA_GEOM                          Geometry(Polygon, 4326)         null,
      AA_DES                           CHARACTER VARYING(100)          null,   
      ID_ELEMENTO                      CHARACTER VARYING(40)           not null,   
      TIPO_ELEM                        CHARACTER VARYING(40)           null,
      EXP_CODIGO_INTERNO               CHARACTER VARYING(255)          null,
      EXP_DESCRIPCION                  CHARACTER VARYING(100)          null,
      EXP_DIRECCION                    CHARACTER VARYING(255)          null,
      EXP_DIRECCION_NOTIFICACION       CHARACTER VARYING(255)          null,
      EXP_ESTADO_EXPEDIENTE            INTEGER                         not null,
      EXP_ESTADO_EXPEDIENTE_DESC       CHARACTER VARYING(100)          null,
      EXP_FECHA_ALTA                   TIMESTAMP                       null,
      EXP_FECHA_BAJA                   TIMESTAMP                       null,
      EXP_INT_EMAIL                    CHARACTER VARYING(255)          null,
      EXP_INT_TELEF                    CHARACTER VARYING(12)           null,
      EXP_NOMBRE_COMPLETO              CHARACTER VARYING(100)          null,
      EXP_NUMERO                       CHARACTER VARYING(25)           not null,
      EXP_NUMERO_DOCUMENTO             CHARACTER VARYING(10)           null,
      EXP_OBSERVACIONES                CHARACTER VARYING(1000)         null,
      EXP_REFCAT                       CHARACTER VARYING(20)           null,
      EXP_REFCAT_OTRAS                 CHARACTER VARYING(1000)         null,
      EXP_TIPO_DOCUMENTO               CHARACTER VARYING(50)           null,
      EXP_TIPO_PERSONA                 CHARACTER VARYING(100)          null,
      EXP_OVP_TIPO_OCUPACION           CHARACTER VARYING(50)           null,
      EXP_OVP_MOTIVO                   CHARACTER VARYING(150)          null,
      EXP_OVP_TIPO_VEHICULO            CHARACTER VARYING(50)           null,
      EXP_OVP_REF_LICENCIA             CHARACTER VARYING(20)           null,
      EXP_OVP_CARRIL_LARGO             NUMERIC                         null,
      EXP_OVP_CARRIL_ANCHO             NUMERIC                         null,    
      EXP_OVP_CARRIL_FECHA_INI         TIMESTAMP                       null,
      EXP_OVP_CARRIL_FECHA_FIN         TIMESTAMP                       null,
      EXP_OVP_CARRIL_CORTE             BOOLEAN                         null,
      EXP_OVP_CARRIL_CORTE_DESC        CHARACTER VARYING(200)          null,
      EXP_OVP_APARCAMIENTO_LARGO       NUMERIC                         null,
      EXP_OVP_APARCAMIENTO_ANCHO       NUMERIC                         null,
      EXP_OVP_APARCAMIENTO_FECHA_INI   TIMESTAMP                       null,
      EXP_OVP_APARCAMIENTO_FECHA_FIN   TIMESTAMP                       null,
      EXP_OVP_ACERA_LARGO              NUMERIC                         null,
      EXP_OVP_ACERA_ANCHO              NUMERIC                         null,
      EXP_OVP_ACERA_FECHA_INI          TIMESTAMP                       null,
      EXP_OVP_ACERA_FECHA_FIN          TIMESTAMP                       null,
      ENT_TEXTO_1                      CHARACTER VARYING(255)          null,
      ENT_TEXTO_2                      CHARACTER VARYING(255)          null,
      ENT_TEXTO_3                      CHARACTER VARYING(255)          null,
      ENT_NUM_1                        NUMERIC                         null,
      ENT_NUM_2                        NUMERIC                         null,
      ENT_NUM_3                        NUMERIC                         null,
      ENT_FECHA_1                      TIMESTAMP                       null,
      ENT_FECHA_2                      TIMESTAMP                       null,
      ENT_FECHA_3                      TIMESTAMP                       null,                 
      constraint PK_OVP_TEMP primary key (OGC_FID)
   );




   /*==============================================================*/
   /* Tabla: ACTIVIDADES                                           */
   /*==============================================================*/
   sql_create_entity := 'CREATE table ACTIVIDADES (
                           ENT_UID                          CHARACTER(36)                   not null,
                           ENT_FECHA_REVISION               TIMESTAMP                       null,
                           ENT_ULTIMA_INSP                  TIMESTAMP                       null,
                           ENT_VIGENCIA                     INTEGER                         null,
                           AA_IDA                           INTEGER                         not null,
                           AA_GEOM                          Geometry(Point,'||epsg||')      null,                           
                           AA_DES                           CHARACTER VARYING(100)          null,
                           ID_ELEMENTO                      CHARACTER VARYING(40)           not null,
                           TIPO_ELEM                        CHARACTER VARYING(40)           null,
                           ENT_ACTIVIDAD_IDENTIFICADOR      CHARACTER VARYING(13)           null,
                           ENT_ACTIVIDAD_NOMBRE_ACT         CHARACTER VARYING(155)          null,
                           ENT_EMPRESA_RAZON_SOCIAL         CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_NUMERO_DOCUMENTO   CHARACTER VARYING(21)           null,
                           ENT_ACTIVIDAD_DIRECCION          CHARACTER VARYING(255)          null,
                           ENT_ACTIVIDAD_REFCAT             CHARACTER VARYING(25)           null,
                           ENT_ACTIVIDAD_EPIGRAFE           CHARACTER VARYING(255)          null,
                           ENT_ACTIVIDAD_CATEGORIA_1        CHARACTER VARYING(255)          null,
                           ENT_ACTIVIDAD_ACT_PRINCIPAL      BOOLEAN                         null,
                           ENT_ACTIVIDAD_LOCAL_ALQUILADO    BOOLEAN                         null,
                           ENT_ACTIVIDAD_WEB                CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_EMAIL_CONTACTO     CHARACTER VARYING(150)          null,
                           ENT_ACTIVIDAD_PERSONA_CONTACTO   CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_TELEFONO_CONT      CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_HORARIO_INVIERNO   CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_HORARIO_VERANO     CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_PUBLICABLE         BOOLEAN                         null,
                           ENT_ACTIVIDAD_FECHA_ALTA         TIMESTAMP                       null,
                           ENT_ACTIVIDAD_FECHA_BAJA         TIMESTAMP                       null,
                           ENT_ACTIVIDAD_OTROS_EPIGRAFES    text                            null,
                           ENT_TEXTO_1                      CHARACTER VARYING(255)          null,
                           ENT_TEXTO_2                      CHARACTER VARYING(255)          null,
                           ENT_TEXTO_3                      CHARACTER VARYING(255)          null,
                           ENT_NUM_1                        NUMERIC                         null,
                           ENT_NUM_2                        NUMERIC                         null,
                           ENT_NUM_3                        NUMERIC                         null,
                           ENT_FECHA_1                      TIMESTAMP                       null,
                           ENT_FECHA_2                      TIMESTAMP                       null,
                           ENT_FECHA_3                      TIMESTAMP                       null,
                           constraint PK_ACTIVIDADES primary key (ENT_UID)
                        );';

   EXECUTE sql_create_entity;

   CREATE INDEX IDX_GEOM_ACTIVIDADES ON ACTIVIDADES USING GIST (AA_GEOM);



   /*==============================================================*/
   /* Table: ACTIVIDADES_TEMP                                      */
   /*==============================================================*/
   create table ACTIVIDADES_TEMP (
      OGC_FID                          SERIAL                          not null,
      AA_IDA                           INTEGER                         not null,
      AA_GEOM                          Geometry(Point, 4326)           null,
      AA_DES                           CHARACTER VARYING(100)          null,   
      ID_ELEMENTO                      CHARACTER VARYING(40)           not null,   
      TIPO_ELEM                        CHARACTER VARYING(40)           null,
      ENT_ACTIVIDAD_IDENTIFICADOR      CHARACTER VARYING(13)           null,
      ENT_ACTIVIDAD_NOMBRE_ACT         CHARACTER VARYING(155)          null,
      ENT_EMPRESA_RAZON_SOCIAL         CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_NUMERO_DOCUMENTO   CHARACTER VARYING(21)           null,
      ENT_ACTIVIDAD_DIRECCION          CHARACTER VARYING(255)          null,
      ENT_ACTIVIDAD_REFCAT             CHARACTER VARYING(25)           null,
      ENT_ACTIVIDAD_EPIGRAFE           CHARACTER VARYING(255)          null,
      ENT_ACTIVIDAD_CATEGORIA_1        CHARACTER VARYING(255)          null,
      ENT_ACTIVIDAD_ACT_PRINCIPAL      BOOLEAN                         null,
      ENT_ACTIVIDAD_LOCAL_ALQUILADO    BOOLEAN                         null,
      ENT_ACTIVIDAD_WEB                CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_EMAIL_CONTACTO     CHARACTER VARYING(150)          null,
      ENT_ACTIVIDAD_PERSONA_CONTACTO   CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_TELEFONO_CONT      CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_HORARIO_INVIERNO   CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_HORARIO_VERANO     CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_PUBLICABLE         BOOLEAN                         null,
      ENT_ACTIVIDAD_FECHA_ALTA         TIMESTAMP                       null,
      ENT_ACTIVIDAD_FECHA_BAJA         TIMESTAMP                       null,
      ENT_ACTIVIDAD_OTROS_EPIGRAFES    text                            null,
      ENT_TEXTO_1                      CHARACTER VARYING(255)          null,
      ENT_TEXTO_2                      CHARACTER VARYING(255)          null,
      ENT_TEXTO_3                      CHARACTER VARYING(255)          null,
      ENT_NUM_1                        NUMERIC                         null,
      ENT_NUM_2                        NUMERIC                         null,
      ENT_NUM_3                        NUMERIC                         null,
      ENT_FECHA_1                      TIMESTAMP                       null,
      ENT_FECHA_2                      TIMESTAMP                       null,
      ENT_FECHA_3                      TIMESTAMP                       null,            
      constraint PK_ACTIVIDADES_TEMP primary key (OGC_FID)
   );
  

END $$;