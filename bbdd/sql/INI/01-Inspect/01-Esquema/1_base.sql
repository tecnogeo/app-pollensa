DO $$
DECLARE
   epsg integer;
   q text;
   
BEGIN

   /*==============================================================*/
   /* Tabla: TIPOS_AREA_TRABAJO                                    */
   /*==============================================================*/  
   create table TIPOS_AREAS_TRABAJO (
      TIPO_AREA_ID               SERIAL                   not null,
      TIPO_AREA_CODIGO           CHARACTER VARYING(50)    UNIQUE,
      constraint PK_TIPOS_AREAS_TRABAJO primary key (TIPO_AREA_ID)
   );
   insert into tipos_areas_trabajo (tipo_area_codigo) values ('SIN AREA'); -- + BARRIO, DISTRITOS, AT_MERCADO,


   /*==============================================================*/
   /* Tabla: AREA_TRABAJO                                          */
   /*==============================================================*/  
   SELECT c.epsg into epsg from config_cliente c;
   
   q := 'create table AREAS_TRABAJO (
               AREA_ID              SERIAL                           not null,
               TIPO_AREA_ID         INTEGER                          REFERENCES TIPOS_AREAS_TRABAJO(TIPO_AREA_ID),
               AREA_DESCRIPCION     CHARACTER VARYING(100)           not null,
               AREA_GEOM            Geometry(Polygon, '||epsg||')    null,
               constraint PK_AREAS_TRABAJO primary key (AREA_ID)
            );';
   EXECUTE q;

   CREATE INDEX IDX_GEOM_AREAS_TRABAJO ON AREAS_TRABAJO USING GIST (AREA_GEOM);


   /*==============================================================*/
   /* Tabla: TIPOS_ENTIDAD                                         */
   /*==============================================================*/
   create table TIPOS_ENTIDAD (
      TIPO_ENTIDAD_ID              INTEGER                     not null,
      TIPO_ENTIDAD_CODIGO          CHARACTER VARYING(20)       not null,
      TABLA_ENTIDAD                CHARACTER VARYING(100)      not null,
      ESQUEMA_ENTIDAD              CHARACTER VARYING(50)       not null,
      ES_EXPEDIENTE                BOOLEAN                     not null,
      COLUMNA_ENT_UID              CHARACTER VARYING(50)       not null, 
      COLUMNA_CODIGO               CHARACTER VARYING(50)       not null,
      COLUMNA_NOMBRE               CHARACTER VARYING(50)       not null,
      COLUMNA_LOCALIZACION         CHARACTER VARYING(50)       not null,
      CAMPOS_ADICIONALES            CHARACTER VARYING(255)     null,
      constraint PK_TIPOS_ENTIDAD primary key (TIPO_ENTIDAD_ID)
   );

   insert into TIPOS_ENTIDAD (TIPO_ENTIDAD_ID, TIPO_ENTIDAD_CODIGO, TABLA_ENTIDAD, ESQUEMA_ENTIDAD, ES_EXPEDIENTE, COLUMNA_ENT_UID, COLUMNA_CODIGO, COLUMNA_NOMBRE, COLUMNA_LOCALIZACION, CAMPOS_ADICIONALES ) values (1, 'MERCADOS', 'mercados', 'public', true, 'ent_mercados_uid', 'exp_numero', 'exp_nombre_completo', 'exp_mer_codigo', null);
   insert into TIPOS_ENTIDAD (TIPO_ENTIDAD_ID, TIPO_ENTIDAD_CODIGO, TABLA_ENTIDAD, ESQUEMA_ENTIDAD, ES_EXPEDIENTE, COLUMNA_ENT_UID, COLUMNA_CODIGO, COLUMNA_NOMBRE, COLUMNA_LOCALIZACION, CAMPOS_ADICIONALES ) values (2, 'TERRAZAS', 'terrazas', 'public', true, 'ent_terrazas_uid', 'exp_numero', 'exp_terr_nombre_local', 'exp_direccion,terraza_ubicacion', 'ins_terrazas_mesas,ins_terrazas_sillas,ins_terrazas_superficie,ins_terrazas_otros');
   insert into TIPOS_ENTIDAD (TIPO_ENTIDAD_ID, TIPO_ENTIDAD_CODIGO, TABLA_ENTIDAD, ESQUEMA_ENTIDAD, ES_EXPEDIENTE, COLUMNA_ENT_UID, COLUMNA_CODIGO, COLUMNA_NOMBRE, COLUMNA_LOCALIZACION, CAMPOS_ADICIONALES ) values (3, 'OBRAS', 'obras', 'public', true, 'ent_obras_uid', 'exp_numero', 'exp_nombre_completo', 'exp_direccion', 'ins_obras_estado');
   insert into TIPOS_ENTIDAD (TIPO_ENTIDAD_ID, TIPO_ENTIDAD_CODIGO, TABLA_ENTIDAD, ESQUEMA_ENTIDAD, ES_EXPEDIENTE, COLUMNA_ENT_UID, COLUMNA_CODIGO, COLUMNA_NOMBRE, COLUMNA_LOCALIZACION, CAMPOS_ADICIONALES ) values (4, 'OVP', 'ovp', 'public', true, 'ent_ovp_uid', 'exp_numero', 'exp_nombre_completo', 'exp_direccion', null);
   insert into TIPOS_ENTIDAD (TIPO_ENTIDAD_ID, TIPO_ENTIDAD_CODIGO, TABLA_ENTIDAD, ESQUEMA_ENTIDAD, ES_EXPEDIENTE, COLUMNA_ENT_UID, COLUMNA_CODIGO, COLUMNA_NOMBRE, COLUMNA_LOCALIZACION, CAMPOS_ADICIONALES ) values (5, 'ACTIVIDADES', 'actividades', 'public', false, 'ent_actividades_uid', 'ent_actividad_numero_documento', 'ent_actividad_nombre_act', 'ent_actividad_direccion', null);

   /*==============================================================*/
   /* Tabla: TIPOS_ENTIDAD_IDIOMA                                  */
   /*==============================================================*/
   create table TIPOS_ENTIDAD_IDIOMA (   
      TIPO_ENTIDAD_ID      INTEGER              REFERENCES TIPOS_ENTIDAD(TIPO_ENTIDAD_ID),
      IDIOMA               CHAR(5)              REFERENCES IDIOMAS(IDIOMA),
      LITERAL              VARCHAR(100)         not null,
      constraint PK_TIPOS_ENTIDAD_IDIOMA primary key (IDIOMA, TIPO_ENTIDAD_ID)
   );
   insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (1, 'ca-ES', 'Mercats');
   insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (1, 'es-ES', 'Mercados');
   insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (2, 'ca-ES', 'Terrasses');
   insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (2, 'es-ES', 'Terrazas');   
   insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (3, 'ca-ES', 'Obres urbanístiques');
   insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (3, 'es-ES', 'Obras urbanísticas');
   insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (4, 'ca-ES', 'Ocupació de la via pública');
   insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (4, 'es-ES', 'Ocupación de la vía pública');
   insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (5, 'ca-ES', 'Activitats econòmiques');
   insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (5, 'es-ES', 'Actividades económicas');


   /*==============================================================*/
   /* Tabla: TIPOS_HISTORICO                                       */
   /*==============================================================*/
   create table TIPOS_HISTORICO (
      TIPO_HIST_ID          SERIAL                     not null,
      TIPO_HIST_DESCRIPCION CHARACTER VARYING(100)         null,
      constraint PK_TIPOS_HISTORICO primary key (TIPO_HIST_ID)
   );

   insert into tipos_historico (tipo_hist_id, tipo_hist_descripcion) values (1, 'Histórico respecto un campo fecha');
   insert into tipos_historico (tipo_hist_id, tipo_hist_descripcion) values (2, 'Histórico respecto un intervalo');
   insert into tipos_historico (tipo_hist_id, tipo_hist_descripcion) values (3, 'Sin intervalo');
   insert into tipos_historico (tipo_hist_id, tipo_hist_descripcion) values (4, 'Histórico respecto el estado del expediente');


   /*==============================================================*/
   /* Tabla: TIPOS_ENTIDAD_CONFIG                                  */
   /*==============================================================*/
   create table TIPOS_ENTIDAD_CONFIG (
      TIPO_ENTIDAD_ID             INTEGER              REFERENCES TIPOS_ENTIDAD(TIPO_ENTIDAD_ID),
      ENTIDAD_ACTIVA              BOOLEAN              not null,
      TIPO_AREA_ID                INTEGER              REFERENCES TIPOS_AREAS_TRABAJO(TIPO_AREA_ID),
      HIST_CAMPO_FECHA_ENTIDAD    TEXT                 null,
      HIST_INTERVALO_HISTORICO    INTERVAL             null,
      HIST_INTERVALO_ACTIVO       INTERVAL             null,
      HIST_INTERVALO_UNIDAD       TEXT                 null,
      HIST_TIPO_ID                INTEGER              REFERENCES TIPOS_HISTORICO(TIPO_HIST_ID),
      constraint PK_TIPOS_ENTIDAD_CONFIG primary key (TIPO_ENTIDAD_ID)
   );

   ALTER TABLE TIPOS_ENTIDAD_CONFIG
   ADD CONSTRAINT check_configuracion CHECK( 
      (hist_tipo_id = 1 AND (hist_campo_fecha_entidad IS NOT NULL AND
                        hist_campo_fecha_entidad ~ '^[\w]*$' AND
                        hist_intervalo_historico IS NOT NULL AND 
                        hist_intervalo_historico::text ~ '^(-[0-9] [\w]+)$'::text AND
                        hist_intervalo_unidad IS NOT NULL
                     )) 
      OR (hist_tipo_id = 2 AND (hist_campo_fecha_entidad IS NOT NULL AND 
                        hist_campo_fecha_entidad ~ '^(\w+\;\w+)$' AND
                        hist_intervalo_historico IS NOT NULL AND 
                        hist_intervalo_historico::text ~ '^(-[0-9] [\w]+)$'::text AND
                        hist_intervalo_activo IS NULL AND 
                        hist_intervalo_unidad IS NOT NULL						   
                     ))
      OR(hist_tipo_id = 3 AND (hist_campo_fecha_entidad IS NULL AND
                        hist_intervalo_historico IS NULL AND 
                        hist_intervalo_activo IS NULL AND
                        hist_intervalo_unidad IS NULL
                        ))
      OR(hist_tipo_id = 4 AND (hist_campo_fecha_entidad IS NULL AND
                        hist_intervalo_historico IS NULL AND 
                        hist_intervalo_activo IS NULL AND
                        hist_intervalo_unidad IS NULL
                        ))
   );

   --Insertamos valores por defecto para cada entidad. Si se requiere, se puede modificar a posteriori la tabla.      
   --mercados(campo fecha): activos año actual, historico año anterior
   insert into TIPOS_ENTIDAD_CONFIG (tipo_entidad_id, entidad_activa, tipo_area_id, hist_campo_fecha_entidad, hist_intervalo_historico, hist_intervalo_activo, hist_intervalo_unidad, hist_tipo_id )
   values (1, true, 1, null, null, null, null, 4);
   
   --terrazas(intervalo): activos intervalo actual, historico año anterior hasta fecha_ini
   insert into TIPOS_ENTIDAD_CONFIG (tipo_entidad_id, entidad_activa, tipo_area_id, hist_campo_fecha_entidad, hist_intervalo_historico, hist_intervalo_activo, hist_intervalo_unidad, hist_tipo_id )
   values (2, true, 1, 'terraza_fecha_ini;terraza_fecha_fin', '-1 year', null, 'year', 2);

   --obras (intervalo): activos intervalo actual, historico año anterior hasta fecha_ini
   insert into TIPOS_ENTIDAD_CONFIG (tipo_entidad_id, entidad_activa, tipo_area_id, hist_campo_fecha_entidad, hist_intervalo_historico, hist_intervalo_activo, hist_intervalo_unidad, hist_tipo_id )
   values (3, true, 1, null, null, null, null, 4);

   --ovp (campo fecha): activos año actual, historico año anterior
   insert into TIPOS_ENTIDAD_CONFIG (tipo_entidad_id, entidad_activa, tipo_area_id, hist_campo_fecha_entidad, hist_intervalo_historico, hist_intervalo_activo, hist_intervalo_unidad, hist_tipo_id )
   values (4, true, 1, null, null, null, null, 4);

   --actividades: sin historico
   insert into TIPOS_ENTIDAD_CONFIG (tipo_entidad_id, entidad_activa, tipo_area_id, hist_campo_fecha_entidad, hist_intervalo_historico, hist_intervalo_activo, hist_intervalo_unidad, hist_tipo_id )
   values (5, true, 1, null, null, null, null, 3);



   /*==============================================================*/
   /* Tabla: TIPOS_INFRACCIONES                                      */
   /*==============================================================*/
   create table TIPOS_INFRACCIONES (
      TIPO_INF_ID                   SERIAL                      not null,
      TIPO_INF_CODIGO               CHARACTER VARYING(50)           null,   
      constraint PK_TIPOS_INFRACCIONES primary key (tipo_inf_id)
   );

   insert into TIPOS_INFRACCIONES (tipo_inf_id, tipo_inf_codigo) values (0, 'SIN_INFRACCION');
   insert into TIPOS_INFRACCIONES (tipo_inf_id, tipo_inf_codigo) values (1, 'INFRACION_LEVE');
   insert into TIPOS_INFRACCIONES (tipo_inf_id, tipo_inf_codigo) values (2, 'INFRACION_GRAVE');
   insert into TIPOS_INFRACCIONES (tipo_inf_id, tipo_inf_codigo) values (3, 'INFRACION_MUYGRAVE');


   /*==============================================================*/
   /* Tabla: TIPOS_INFRACCIONES_IDIOMA                               */
   /*==============================================================*/
   create table TIPOS_INFRACCIONES_IDIOMA (
      TIPO_INF_ID                   SERIAL                      not null,
      IDIOMA                        CHAR(5)                     REFERENCES IDIOMAS (IDIOMA),
      LITERAL                       CHARACTER VARYING(100)      null,
      constraint PK_TIPOS_INFRACCIONES_IDIOMA primary key (tipo_inf_id, idioma)
   );

   insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (0, 'ca-ES', 'Sense infracció');
   insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (0, 'es-ES', 'Sin infracción');
   insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (1, 'ca-ES', 'Lleu');
   insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (1, 'es-ES', 'Leve');
   insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (2, 'ca-ES', 'Greu');
   insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (2, 'es-ES', 'Grave');
   insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (3, 'ca-ES', 'Molt greu');
   insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (3, 'es-ES', 'Muy grave');



   /*==============================================================*/
   /* Tabla: INFRACCIONES                                          */
   /*==============================================================*/
   create table INFRACCIONES (   
      INF_CODIGO           CHAR(11)                    not null,   
      TIPO_ENTIDAD_ID      INTEGER                     REFERENCES TIPOS_ENTIDAD(TIPO_ENTIDAD_ID),
      TIPO_INF_ID          INTEGER                     REFERENCES TIPOS_INFRACCIONES(TIPO_INF_ID),   
      constraint PK_INFRACCIONES primary key (INF_CODIGO, TIPO_ENTIDAD_ID)
   );


   /*==============================================================*/
   /* Tabla: INFRACCIONES_IDIOMA                                   */
   /*==============================================================*/
   create table INFRACCIONES_IDIOMA (   
      INF_CODIGO           CHAR(11)                    not null,
      TIPO_ENTIDAD_ID      INTEGER                     not null,
      IDIOMA               CHAR(5)                     REFERENCES IDIOMAS(IDIOMA),
      LITERAL              TEXT                        null,   
      constraint FK_INFRACCIONES foreign key (INF_CODIGO, TIPO_ENTIDAD_ID) references INFRACCIONES (INF_CODIGO, TIPO_ENTIDAD_ID),
      constraint PK_INFRACCIONES_IDIOMA primary key (INF_CODIGO, TIPO_ENTIDAD_ID, IDIOMA)
   );



   /*==============================================================*/
   /* Tabla: TIPOS_ESTADOS_INSP                                    */
   /*==============================================================*/
   CREATE TABLE TIPOS_ESTADOS_INSP (
      ESTADO_ID            BOOLEAN                     not null,
      ESTADO_CODIGO        CHARACTER VARYING(50)       null,   
      constraint PK_TIPOS_ESTADOS_INSP primary key (ESTADO_ID)
   );
   insert into TIPOS_ESTADOS_INSP (estado_id, estado_codigo) values (true, 'FINALIZADA');
   insert into TIPOS_ESTADOS_INSP (estado_id, estado_codigo) values (false, 'PENDIENTE');
   
   /*==============================================================*/
   /* Tabla: TIPOS_ESTADOS_INSP_IDIOMA                            */
   /*==============================================================*/
   CREATE TABLE TIPOS_ESTADOS_INSP_IDIOMA (
      ESTADO_ID     BOOLEAN                     REFERENCES TIPOS_ESTADOS_INSP(ESTADO_ID),
      IDIOMA        CHAR(5)                     REFERENCES IDIOMAS(IDIOMA),
      LITERAL       CHARACTER VARYING(50)       null,
      constraint PK_TIPOS_ESTADOS_INSP_IDIOMA primary key (ESTADO_ID, IDIOMA)
   );
   insert into TIPOS_ESTADOS_INSP_IDIOMA (estado_id, idioma, literal) values (false, 'ca-ES', 'Pendent');
   insert into TIPOS_ESTADOS_INSP_IDIOMA (estado_id, idioma, literal) values (true, 'ca-ES', 'Finalitzada');
   insert into TIPOS_ESTADOS_INSP_IDIOMA (estado_id, idioma, literal) values (false, 'es-ES', 'Pendiente');
   insert into TIPOS_ESTADOS_INSP_IDIOMA (estado_id, idioma, literal) values (true, 'es-ES', 'Finalizada');


   /*==============================================================*/
   /* Tabla: TIPOS_MOD_INSP                                        */
   /*==============================================================*/
   CREATE TABLE TIPOS_MODIFICACIONES_INSP (
      MOD_ID            INTEGER                     not null,
      MOD_CODIGO        CHARACTER VARYING(50)       not null,   
      constraint PK_TIPOS_MODIFICACIONES_INSP primary key (MOD_ID)
   );
   insert into TIPOS_MODIFICACIONES_INSP (mod_id, mod_codigo) values (1, 'SIN_MODIFICACIONES');
   insert into TIPOS_MODIFICACIONES_INSP (mod_id, mod_codigo) values (2, 'PENDIENTE_MODIFICAR');
   insert into TIPOS_MODIFICACIONES_INSP (mod_id, mod_codigo) values (3, 'MODIFICADO');
   
   /*==============================================================*/
   /* Tabla: TIPOS_MOD_INSP_IDIOMA                                 */
   /*==============================================================*/
   CREATE TABLE TIPOS_MODIFICACIONES_INSP_IDIOMA (
      MOD_ID        INTEGER                     REFERENCES TIPOS_MODIFICACIONES_INSP(MOD_ID),
      IDIOMA        CHAR(5)                     REFERENCES IDIOMAS(IDIOMA),
      LITERAL       CHARACTER VARYING(50)       null,
      constraint PK_TIPOS_MODIFICACIONES_INSP_IDIOMA primary key (MOD_ID, IDIOMA)
   );
   insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (1, 'ca-ES', 'Sense modificacions');
   insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (1, 'es-ES', 'Sin modificaciones');
   insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (2, 'ca-ES', 'Sol·licitud de modificació');
   insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (2, 'es-ES', 'Solicitud de modificación');
   insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (3, 'ca-ES', 'Modificat');
   insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (3, 'es-ES', 'Modificado');


   /*==============================================================*/
   /* Tabla: TIPOS_ASISTENCIA_INSP                                 */
   /*==============================================================*/
   CREATE TABLE TIPOS_ASISTENCIA_INSP (
      ASISTENCIA_ID            INTEGER                     not null,
      ASISTENCIA_CODIGO        CHARACTER VARYING(50)       not null,   
      constraint PK_TIPOS_ASISTENCIA_INSP primary key (ASISTENCIA_ID)
   );
   insert into TIPOS_ASISTENCIA_INSP (asistencia_id, asistencia_codigo) values (1, 'EN_PRESENCIADE');
   insert into TIPOS_ASISTENCIA_INSP (asistencia_id, asistencia_codigo) values (2, 'NO_IDENTIFICACION');
   insert into TIPOS_ASISTENCIA_INSP (asistencia_id, asistencia_codigo) values (3, 'NO_NECESARIO');
   
   /*==============================================================*/
   /* Tabla: TIPOS_MOD_INSP_IDIOMA                                 */
   /*==============================================================*/
   CREATE TABLE TIPOS_ASISTENCIA_INSP_IDIOMA (
      ASISTENCIA_ID INTEGER                     REFERENCES TIPOS_ASISTENCIA_INSP(ASISTENCIA_ID),
      IDIOMA        CHAR(5)                     REFERENCES IDIOMAS(IDIOMA),
      LITERAL       CHARACTER VARYING(50)       null,
      constraint PK_TIPOS_ASISTENCIA_INSP_IDIOMA primary key (ASISTENCIA_ID, IDIOMA)
   );
   insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (1, 'ca-ES', 'En presència de');
   insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (1, 'es-ES', 'En presencia de');
   insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (2, 'ca-ES', 'No es vol identificar');
   insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (2, 'es-ES', 'No se quiere identificar');
   insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (3, 'ca-ES', 'No és necessari');
   insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (3, 'es-ES', 'No es necesario');


   /*==============================================================*/
   /* Tabla: TIPOS_ESTADOS_DISPOSITIVOS                            */
   /*==============================================================*/
   CREATE table TIPOS_ESTADOS_DISPOSITIVOS (
      ESTADO_ID               INTEGER                   not null,
      ESTADO_DESCRIPCION      CHARACTER VARYING(50)     UNIQUE,
      constraint PK_TIPOS_ESTADOS_DISPOSITIVOS primary key (ESTADO_ID)
   );
   INSERT INTO TIPOS_ESTADOS_DISPOSITIVOS (estado_id, estado_descripcion) VALUES (1, 'Activo');
   INSERT INTO TIPOS_ESTADOS_DISPOSITIVOS (estado_id, estado_descripcion) VALUES (-1, 'Baja');

   /*==============================================================*/
   /* Tabla: DISPOSITIVOS_FIRMA                                    */
   /*==============================================================*/
   CREATE table DISPOSITIVOS_FIRMA (
      DEVICE_NAME         CHARACTER VARYING (255)    not null,
      DEVICE_DESCRIPTION  CHARACTER VARYING (255)    null,
      ESTADO              INTEGER                    REFERENCES TIPOS_ESTADOS_DISPOSITIVOS(ESTADO_ID),
      constraint PK_DISPOSITIVOS_FIRMA primary key (DEVICE_NAME)
   );


   /*==============================================================*/
   /* Tabla: TECNICOS_FIRMA                                    */
   /*==============================================================*/
   CREATE table TECNICOS_FIRMA (
      USUARIO             CHARACTER VARYING (100)    not null,
      NOMBRE              CHARACTER VARYING (100)    not null,
      TIPO_DOCUMENTO      CHARACTER VARYING (50)     not null,
      NUMERO_DOCUMENTO    CHARACTER VARYING (21)     not null,
      EMAIL               CHARACTER VARYING (100)    null,
      constraint PK_TECNICOS_FIRMA primary key (USUARIO)
   );



END $$;