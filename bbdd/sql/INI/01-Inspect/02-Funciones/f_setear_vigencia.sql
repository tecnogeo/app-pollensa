CREATE OR REPLACE FUNCTION f_setearVigencia(tipo_entidad_id integer)
	RETURNS BOOLEAN AS $$
DECLARE 
	tabla_entidad text;
	tabla_entidad_temp text;
	entidad_esexpediente boolean;
	
	campo_fecha text;
	intervalo_activo interval;
	intervalo_historico interval;
	intervalo_unidad varchar(50);
	tipo_historico integer;
	esquema_entidad text;
	campo_fecha_inicio text;
	campo_fecha_fin text;

	q text;
	q_actualiza_eliminados text;
	r record;

	b_existe boolean;
	b_existe2 boolean;
	b_resultado boolean;
		
	str_fecha_entidad text;
	str_fecha_inicio_activo text;
	str_fecha_final_activo text;
	str_fecha_inicio_historico text;
	str_fecha_actual text;

BEGIN
	b_resultado := TRUE;

	--Obtener el nombre y esquema de la tabla que vamos a actualizar
	q := 'SELECT tabla_entidad, esquema_entidad, es_expediente FROM tipos_entidad WHERE tipo_entidad_id = '|| tipo_entidad_id || '';
	EXECUTE q INTO tabla_entidad, esquema_entidad, entidad_esexpediente;
	tabla_entidad_temp := tabla_entidad || '_temp';

	--Obtener la configuración del histórico para esa entidad
	q := 'SELECT hist_campo_fecha_entidad, hist_intervalo_activo, hist_intervalo_historico, hist_intervalo_unidad, hist_tipo_id 
		  FROM tipos_entidad_config WHERE tipo_entidad_id = '|| tipo_entidad_id || '';
	EXECUTE q INTO campo_fecha, intervalo_activo, intervalo_historico, intervalo_unidad, tipo_historico;

	CASE tipo_historico
	WHEN 1 THEN --(1) Histórico respecto una fecha

		--validamos que el campo fecha configurado existe en la entidad
		SELECT f_validaCampo(esquema_entidad, tabla_entidad, campo_fecha) INTO b_existe;
		IF b_existe IS TRUE THEN

			IF intervalo_activo IS NULL THEN --si es el caso 1 y el campo intervalo_activo está a null, corresponde al (mes/año...) actual cogiendo la unidad_intervalo.
				intervalo_activo = '0 ' || intervalo_unidad;
			END IF;

			str_fecha_entidad := '(date_trunc('''|| intervalo_unidad ||''', '|| campo_fecha ||'))::date';
			str_fecha_inicio_activo := '(date_trunc('''|| intervalo_unidad ||''', current_date)+'''|| intervalo_activo ||''')::date';
			str_fecha_final_activo := '(date_trunc('''|| intervalo_unidad ||''', current_date))::date';		
			str_fecha_inicio_historico := '(date_trunc('''|| intervalo_unidad ||''', current_date)+'''|| intervalo_historico ||''')::date';
					
			q := 'UPDATE ' || tabla_entidad || ' SET ent_vigencia = 
				CASE 
					WHEN '|| str_fecha_inicio_activo || ' <= '|| str_fecha_entidad ||' AND '|| str_fecha_entidad ||' <= ' || str_fecha_final_activo ||' THEN 1 
					WHEN '|| str_fecha_inicio_historico || ' <= '|| str_fecha_entidad ||' AND '|| str_fecha_entidad ||' <= ' || str_fecha_inicio_activo ||' THEN 0
					ELSE -1 
				END';

			--RAISE NOTICE '%', q;
			EXECUTE q;
		
		ELSE 
			b_resultado := FALSE;
			RAISE WARNING 'El campo [%] no existe en la entidad [%].', campo_fecha, tabla_entidad;
		END IF;

	WHEN 2 THEN --(2) Histórico respecto un intervalo		
		
		q:='select split_part(hist_campo_fecha_entidad, '';'', 1) from tipos_entidad_config where tipo_entidad_id =' || tipo_entidad_id || ' ;';
		EXECUTE q into campo_fecha_inicio;		
		
		q:='select split_part(hist_campo_fecha_entidad, '';'', 2) from tipos_entidad_config where tipo_entidad_id =' || tipo_entidad_id || ' ;';
		EXECUTE q into campo_fecha_fin;

		--validamos campos
		SELECT f_validaCampo(esquema_entidad, tabla_entidad, campo_fecha_fin) INTO b_existe;
		SELECT f_validaCampo(esquema_entidad, tabla_entidad, campo_fecha_inicio) INTO b_existe2;
	
		IF b_existe IS TRUE AND b_existe2 IS TRUE THEN

			str_fecha_inicio_historico := '(date_trunc('''|| intervalo_unidad ||''', current_date)+'''|| intervalo_historico ||''')::date';
			str_fecha_entidad := '(date_trunc('''|| intervalo_unidad ||''', '|| campo_fecha_inicio|| '))::date';
			str_fecha_actual := '(date_trunc('''|| intervalo_unidad ||''', current_date))::date';
		
			q := 'UPDATE ' || tabla_entidad || ' SET ent_vigencia = 
				CASE 
					WHEN '|| campo_fecha_inicio || ' <= current_date and current_date <= '|| campo_fecha_fin || ' THEN 1 
					WHEN '|| str_fecha_inicio_historico || ' <= '|| str_fecha_entidad ||' AND '|| str_fecha_entidad ||' < ' || str_fecha_actual || ' THEN 0 
					ELSE -1 
				END';

			--RAISE NOTICE '%', q;
			EXECUTE q;

		ELSE
			b_resultado := FALSE;
			RAISE WARNING 'El campo [%] o [%] no existen en la entidad [%].', campo_fecha_fin, campo_fecha_inicio, tabla_entidad;
		END IF;
				
	
	WHEN 3 THEN --(3) Sin historico (todos son vigentes)
		q := 'UPDATE ' || tabla_entidad || ' SET ent_vigencia = 1' ;
		
		--RAISE NOTICE '%', q;
		EXECUTE q;	


	WHEN 4 THEN --(4) Histórico respecto el estado del expediente		
		
		q := 'UPDATE ' || tabla_entidad || ' SET ent_vigencia = 
				CASE 
					WHEN exp_estado_expediente = 1 THEN 1 
					WHEN exp_estado_expediente = 2 THEN 0 
					ELSE -1
				END';
		
		--RAISE NOTICE '%', q;
		EXECUTE q;	

	END CASE;

	--Finalmente ejecutamos una query que SOLO aplica a las entidades que NO son expedientes
	--Validamos si hay entidades que se hayan eliminado en los datos origen y les
	--ponemos ent_vigencia = -1
	IF entidad_esexpediente IS FALSE THEN
        q_actualiza_eliminados := 'UPDATE '|| tabla_entidad ||' e 
								   SET ent_vigencia = -1  
								   FROM '|| tabla_entidad_temp ||' AS tmp 
								   WHERE NOT EXISTS (
									 SELECT  *
									 FROM  '|| tabla_entidad_temp ||' tmp
									 WHERE  e.id_elemento = tmp.id_elemento
								   );';
		--RAISE NOTICE '%', q_actualiza_eliminados;
		EXECUTE q_actualiza_eliminados;

    END IF;

RETURN b_resultado;

EXCEPTION 
    WHEN others THEN        
        RAISE WARNING '% - %', SQLERRM, SQLSTATE;
		RETURN FALSE;
END;
$$ LANGUAGE plpgsql;

--select f_setearVigencia(1);



CREATE OR REPLACE FUNCTION f_validaCampo(esquema text, tabla text, columna text)
	RETURNS BOOLEAN AS
$$
DECLARE
	b_resultado boolean;
	q text;

BEGIN

	q:= 'SELECT EXISTS (
			SELECT column_name 
			FROM information_schema.columns 
			WHERE table_schema = '''|| esquema ||'''  
			AND table_name = '''|| tabla ||'''  
			AND column_name = '''|| columna ||'''  );';

	EXECUTE q INTO b_resultado;
	RETURN b_resultado;

END;
$$ LANGUAGE plpgsql;

--f_validaCampo(public, mercados, exp_fecha_alta);