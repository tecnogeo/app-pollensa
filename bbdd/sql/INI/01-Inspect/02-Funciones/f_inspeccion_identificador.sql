CREATE OR REPLACE FUNCTION f_ins_identificador_year_seq()
  RETURNS text AS
$$
DECLARE
  current_year text = to_char(now(), 'YYYY');
  last_row_year text;
  identificador text;
BEGIN


LOOP
   BEGIN
      RETURN current_year ||'-'|| to_char(nextval('ins_identificador_year_'|| current_year ||'_seq'), 'FM0000000');

   EXCEPTION WHEN undefined_table THEN   -- error code 42P01
      EXECUTE 'CREATE SEQUENCE ins_identificador_year_' || current_year || '_seq MINVALUE 1 START 1';
   END;
END LOOP;


EXCEPTION 
    WHEN others THEN        
        RAISE WARNING '% - %', SQLERRM, SQLSTATE;
		RETURN 'WARNING '|| SQLERRM || ' - ' || SQLSTATE;
END;
$$ LANGUAGE plpgsql;