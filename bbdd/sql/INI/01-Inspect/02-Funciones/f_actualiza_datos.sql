CREATE OR REPLACE FUNCTION public.f_actualizaDatos()
RETURNS BOOLEAN AS $$
DECLARE
    
    q_entidades text;
    r_entidades record;
    q text;
    b_actualizado boolean;
    b_resultado boolean;
    entidades_ok character varying[];
	entidades_error character varying[];

BEGIN

    b_resultado := true;
    entidades_ok := '{}';
    entidades_error := '{}';

    --Obtener las entidades activas para el cliente, que será de las cuales se hará el volcado para actualizar los datos.
    q_entidades := 'SELECT ent.tipo_entidad_id, ent.tipo_entidad_codigo, entidad_activa 
        FROM tipos_entidad ent 
        LEFT JOIN tipos_entidad_config conf on ent.tipo_entidad_id = conf.tipo_entidad_id
        where entidad_activa=true';
    
    FOR r_entidades IN EXECUTE q_entidades
    LOOP
        BEGIN
            --Llamar a la función para actualizar los datos de la tabla temporal a la tabla entidad
            q := 'SELECT public.f_actualizaDatosEntidad('|| r_entidades.tipo_entidad_id|| ');';
            EXECUTE q into b_actualizado;

            IF b_actualizado IS TRUE THEN                
                entidades_ok := entidades_ok || r_entidades.tipo_entidad_codigo;                
            ELSE                
                entidades_error := entidades_error || r_entidades.tipo_entidad_codigo;
                b_resultado := false;
            END IF;

    	END;
	
    END LOOP;

    IF b_resultado IS TRUE THEN
        RAISE NOTICE '
================
        RESUMEN        
        Entidades actualizadas correctamente: %
================', entidades_ok;
    ELSE
        RAISE WARNING '
================
        RESUMEN
        Entidades actualizadas correctamente: %
        Entidades con error: %
================', entidades_ok, entidades_error;
    END IF;

    RETURN b_resultado;
  

EXCEPTION 
	WHEN others THEN				
		RAISE WARNING '% - %', SQLERRM, SQLSTATE;
        RETURN FALSE;

END
$$
LANGUAGE plpgsql;

--select f_actualizaDatos();