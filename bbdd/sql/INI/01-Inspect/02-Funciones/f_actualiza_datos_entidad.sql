CREATE OR REPLACE FUNCTION public.f_actualizaDatosEntidad(
    tipo_entidad_id int
)
RETURNS BOOLEAN AS $$
DECLARE
    
    entidad_tabla text;
    entidad_tabla_temp text;
    entidad_esexpediente boolean;
    entidad_esquema text;
    entidad_epsg int;

    str_campos_insert text;
    str_campos_select text;
    str_campos_update text;
    primer_atributo boolean;
    
    nombre_columna text;
    tipo_dato text;

    q_inserta_nuevos text;
    q_inserta_todos text;
    q_actualiza text;
    q_condicion_identificador text;
    b_datos boolean;
    
    num_filas_nuevas int;
    num_filas_actualizadas int;

    q text;
    r record;

    b_resultado boolean;
BEGIN

    b_resultado = TRUE;
    num_filas_nuevas := 0;
    num_filas_actualizadas := 0;

    --1.Obtener los datos de la entidad
    q := 'SELECT ent.tipo_entidad_id, ent.tipo_entidad_codigo, ent.tabla_entidad, ent.esquema_entidad, ent.es_expediente 
            FROM tipos_entidad ent 
            LEFT JOIN tipos_entidad_config conf on ent.tipo_entidad_id = conf.tipo_entidad_id
            where ent.tipo_entidad_id='|| tipo_entidad_id;
    
    EXECUTE q into r;
    entidad_tabla := r.tabla_entidad;
    entidad_tabla_temp := r.tabla_entidad || '_temp';    
    entidad_esquema := r.esquema_entidad;
    entidad_esexpediente := r.es_expediente;

    --2. Obtener EPSG de la tabla entidad
    q := 'select find_srid(''' || entidad_esquema|| ''', '''|| entidad_tabla || ''', ''aa_geom'')';
    EXECUTE q INTO entidad_epsg;

    --3. Obtener atributos de la tabla entidad
    q := 'SELECT 
            lower(column_name) as columname,
            lower(data_type) as datatype                        
            FROM information_schema.columns
            WHERE table_schema = ''' || entidad_esquema || '''
            AND lower(table_name) = lower(''' || entidad_tabla || ''') 
            AND column_name <> ''ent_uid''
            AND column_name <> ''ent_fecha_revision''
            AND column_name <> ''ent_ultima_insp''
            AND column_name <> ''ent_vigencia''            
            ORDER BY ordinal_position asc';
    
    str_campos_insert := 'ent_uid';
    str_campos_select := 'uuid_generate_v1()';
    primer_atributo := true;

    --4. Construcción dinámica de las queries
    FOR r IN EXECUTE q
    LOOP
        nombre_columna := r.columname;
        tipo_dato := r.datatype;        
        
        IF primer_atributo IS TRUE THEN
            str_campos_update := nombre_columna || ' = tmp.' || nombre_columna;
            str_campos_insert := str_campos_insert || ', ' || nombre_columna;
            str_campos_select := str_campos_select || ', ' || nombre_columna;   

            primer_atributo := false;
        
        ELSE         
                                        
            IF nombre_columna = 'aa_geom' THEN
                str_campos_insert := str_campos_insert || ', ' || nombre_columna;
                str_campos_select := str_campos_select || ', ST_Transform(aa_geom, '|| entidad_epsg ||')';
                str_campos_update := str_campos_update || ', ' || nombre_columna || ' = ST_Transform(tmp.aa_geom, '|| entidad_epsg ||')';
            
            ELSIF tipo_dato = 'bytea' THEN
                str_campos_insert := str_campos_insert || ', ' || nombre_columna;
                str_campos_select := str_campos_select || ', case when decode('|| nombre_columna ||', ''base64'') = '''' THEN NULL ELSE decode('|| nombre_columna ||', ''base64'') END ';
                str_campos_update := str_campos_update || ', ' || nombre_columna || ' = (case when decode(tmp.'|| nombre_columna ||', ''base64'') = '''' THEN NULL ELSE decode(tmp.'|| nombre_columna ||', ''base64'') END)';

            ELSIF tipo_dato = 'text' THEN
                str_campos_insert := str_campos_insert || ', ' || nombre_columna;
                str_campos_select := str_campos_select || ', replace(' || nombre_columna || ', ''##'', E''\n'') ';
                str_campos_update := str_campos_update || ', ' || nombre_columna || ' = replace( tmp.' || nombre_columna || ' , ''##'', E''\n'') ';

            ELSE
                str_campos_insert := str_campos_insert || ', ' || nombre_columna;
                str_campos_select := str_campos_select || ', ' || nombre_columna;   
                str_campos_update := str_campos_update || ', ' || nombre_columna || ' = tmp.' || nombre_columna;             
            
            END IF;
        
        END IF;        

    END LOOP;

    q_inserta_todos := 'INSERT INTO '|| entidad_tabla ||' (' || str_campos_insert || ') 
                       SELECT ' || str_campos_select || ' FROM '|| entidad_tabla_temp ||';';
    
    IF entidad_esexpediente IS TRUE THEN 
        q_condicion_identificador := 'e.exp_numero = tmp.exp_numero and e.ID_ELEMENTO = tmp.ID_ELEMENTO';
    ELSE 
        q_condicion_identificador := 'e.ent_actividad_identificador = tmp.ent_actividad_identificador';
    END IF;
    
    
    q_actualiza := 'UPDATE '|| entidad_tabla ||' e 
                    SET '|| str_campos_update || ' 
                    FROM '|| entidad_tabla_temp ||' AS tmp 
                    WHERE '|| q_condicion_identificador;
    IF entidad_esexpediente IS TRUE THEN
        q_actualiza := q_actualiza || ' AND (tmp.exp_estado_expediente <> e.exp_estado_expediente OR tmp.exp_estado_expediente <> 2);';
    END IF;
                    
    


    IF entidad_esexpediente IS TRUE THEN 
        q_condicion_identificador := 'exp_numero = l.exp_numero and ID_ELEMENTO = l.ID_ELEMENTO';
    ELSE 
        q_condicion_identificador := 'ent_actividad_identificador = l.ent_actividad_identificador';
    END IF;

    q_inserta_nuevos := 'INSERT INTO '|| entidad_tabla ||' (' || str_campos_insert || ')  
                    SELECT ' || str_campos_select || ' FROM '|| entidad_tabla_temp ||' l 
                    WHERE NOT EXISTS (SELECT 
                                        FROM '|| entidad_tabla ||'
                                        WHERE ' || q_condicion_identificador || ') ';
                                    
    IF entidad_esexpediente IS TRUE THEN
        q_inserta_nuevos := q_inserta_nuevos || ' AND l.exp_estado_expediente <> 2;';
    END IF;
                    
    -- RAISE NOTICE 'q_inserta_todos->%' , q_inserta_todos;
    -- RAISE NOTICE 'q_actualiza->%' , q_actualiza;
    -- RAISE NOTICE 'q_inserta_nuevos->%' , q_inserta_nuevos;

    
    --5. Miramos si la tabla entidad tiene datos para saber qué query ejecutar
    q := 'SELECT EXISTS (SELECT 1 FROM '|| entidad_esquema ||'.'||entidad_tabla ||');';
	EXECUTE q INTO b_datos;
    IF b_datos IS FALSE THEN 

        --5.a) No tiene datos: hacemoms el volcado masivo de todos los datos        
        EXECUTE q_inserta_todos;
        GET DIAGNOSTICS num_filas_nuevas = ROW_COUNT;

    ELSE
        --5.b) Si tiene datos
        --actualizamos: actualizamos los registros de la tabla entidad. Para el caso de expedientes, solo las que tengan estado <> 2 a partir de la temporal        
        EXECUTE q_actualiza;
        GET DIAGNOSTICS num_filas_actualizadas = ROW_COUNT;		

        --insertem nous: Insertem a la taula ENTITAT els registres de la temporal ESTAT<>TANCAT que NO estàn a la taula ENTITAT        
        EXECUTE q_inserta_nuevos;
        GET DIAGNOSTICS num_filas_nuevas = ROW_COUNT;

    END IF;


    --6.Actualizamos el campo ent_vigencia de la tabla ENTIDAD según su configuración de histórico
    q := 'SELECT f_setearVigencia ('|| tipo_entidad_id|| ');';
    EXECUTE q into b_datos;
    IF b_datos IS FALSE THEN
        b_resultado = FALSE;
    END IF;



RAISE NOTICE '
================
        ENTIDAD: [%]
        Registros nuevos: %
        Registros actualizados: %
        Histórico actualizado: %
================', entidad_tabla, num_filas_nuevas, num_filas_actualizadas, b_datos;

RETURN b_resultado;

EXCEPTION WHEN others THEN   
    RAISE WARNING '% - %', SQLERRM, SQLSTATE;
    RETURN FALSE;
END
$$
LANGUAGE plpgsql;



--select f_actualizaDatosEntidad(1);

