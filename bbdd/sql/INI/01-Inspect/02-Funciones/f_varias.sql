CREATE OR REPLACE FUNCTION f_getNextCodigoInfraccion(tipo_entidad_id integer)
RETURNS TEXT AS $$
DECLARE
    q text;
    next_code text;
    curr_idx integer;
BEGIN
    q:= 'select ''INS_INF_'' || to_char(to_number(substring(max(inf_codigo), 9, 3), ''000'')+1, ''000'') next_code , to_number(substring(max(inf_codigo), 9, 3), ''000'') as idx from infracciones where tipo_entidad_id = ' || tipo_entidad_id;
    EXECUTE q into next_code, curr_idx;

    IF next_code IS NULL THEN
        return 'INS_INF_001';
    ELSIF curr_idx = 35 THEN
        return null;
        --SI DEVUELVE NULL --> ERROR: NO SE PUEDEN INSERTAR MÁS INFRACCIONES PARA ESTE ELEMENTO.
    ELSE
        return next_code;
    END IF;

END;
$$ LANGUAGE plpgsql;