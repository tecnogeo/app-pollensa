-- View: public.v_fa_inspecciones_genericas

-- DROP VIEW public.v_fa_inspecciones_genericas;

CREATE OR REPLACE VIEW public.v_fa_inspecciones_genericas
 AS
 SELECT te.literal AS entidad,
    i.tipo_entidad_id,
    i.ins_uid,
    i.ins_identificador,
    i.ins_fecha_inspeccion,
    i.ins_tecnico,
    i.ins_asistencia,
    ( SELECT i_1.literal
           FROM tipos_asistencia_insp_idioma i_1
             JOIN config_cliente c ON i_1.idioma = c.idioma
          WHERE i_1.asistencia_id = i.ins_asistencia) AS ins_asistencia_literal,
    i.ins_asistencia_desc,
    i.ins_representante,
    i.ins_tipo_documento,
    i.ins_num_documento,
    i.ins_observaciones,
    i.ins_enviado_geweb,
    i.ins_generica,
    i.ins_finalizada,
    encode(i.ins_img_1, 'base64'::text) AS ins_img_1,
    encode(i.ins_img_2, 'base64'::text) AS ins_img_2,
    encode(i.ins_img_3, 'base64'::text) AS ins_img_3,
    st_x(st_centroid(i.ins_ubicacion)) AS ins_geom_centroid_x,
    st_y(st_centroid(i.ins_ubicacion)) AS ins_geom_centroid_y,
    i.ins_ubicacion
   FROM inspecciones i
     JOIN tipos_entidad_idioma te ON te.tipo_entidad_id = i.tipo_entidad_id
     JOIN config_cliente ce ON ce.idioma = te.idioma
  WHERE i.ins_generica = true;