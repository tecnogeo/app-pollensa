
DROP VIEW IF EXISTS v_fa_inspecciones_geom;

CREATE OR REPLACE VIEW v_fa_inspecciones_geom AS

select 
	i.ins_uid,
	i.ins_identificador,
	concat(e.exp_numero, chr(13),i.ins_fecha_inspeccion) as descripcion,
	e.exp_numero as codigo,
	i.ins_fecha_inspeccion,
	i.ins_ubicacion
from inspecciones i
join mercados e on i.ent_mercados_uid = e.ent_uid

union

select 
	i.ins_uid,
	i.ins_identificador,
	concat(e.exp_numero, chr(13),i.ins_fecha_inspeccion) as descripcion,
	e.exp_numero as codigo,
	i.ins_fecha_inspeccion,
	i.ins_ubicacion
from inspecciones i
join terrazas e on i.ent_terrazas_uid = e.ent_uid

union

select 
	i.ins_uid,
	i.ins_identificador,
	concat(e.exp_numero, chr(13),i.ins_fecha_inspeccion) as descripcion,
	e.exp_numero as codigo,
	i.ins_fecha_inspeccion,
	i.ins_ubicacion
from inspecciones i
join obras e on i.ent_obras_uid = e.ent_uid

union

select 
	i.ins_uid,
	i.ins_identificador,
	concat(e.exp_numero, chr(13),i.ins_fecha_inspeccion) as descripcion,
	e.exp_numero as codigo,
	i.ins_fecha_inspeccion,
	i.ins_ubicacion
from inspecciones i
join ovp e on i.ent_ovp_uid = e.ent_uid

union

select 
	i.ins_uid,
	i.ins_identificador,
	concat(e.ent_actividad_nombre_act, chr(13),i.ins_fecha_inspeccion) as descripcion,
	e.ent_actividad_nombre_act as codigo,
	i.ins_fecha_inspeccion,
	i.ins_ubicacion
from inspecciones i
join actividades e on i.ent_actividades_uid = e.ent_uid