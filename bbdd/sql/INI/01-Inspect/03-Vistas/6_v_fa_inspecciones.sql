
DROP VIEW IF EXISTS v_fa_inspecciones;

CREATE OR REPLACE VIEW v_fa_inspecciones AS

    /*INSP. MERCADOS*/
    select 	
        te.literal as entidad,
        e.ent_uid as entidad_uid,
        i.tipo_entidad_id,
        e.exp_numero as entidad_codigo,
        e.exp_direccion as entidad_direccion,
        at.area_descripcion,
        i.ins_uid,
        i.ins_identificador,
        i.ins_fecha_inspeccion,
        i.ins_tecnico,
        i.ins_asistencia,
        (select literal from tipos_asistencia_insp_idioma i inner join config_cliente c on i.idioma = c.idioma where asistencia_id = ins_asistencia) as ins_asistencia_literal,
        i.ins_asistencia_desc,
        i.ins_representante,
        i.ins_tipo_documento,
        i.ins_num_documento,
        i.ins_mod_datos_pers,
        (select literal from tipos_modificaciones_insp_idioma i inner join config_cliente c on i.idioma = c.idioma where mod_id = ins_modificaciones_estado) as ins_modificaciones_estado,
        i.ins_observaciones,
        i.ins_enviado_geweb,
        i.ins_generica,
        i.ins_revision,
        i.ins_revision_desc,
        i.ins_infraccion,
        i.ins_finalizada,
        encode(i.ins_img_1, 'base64') AS ins_img_1,
        encode(i.ins_img_2, 'base64') AS ins_img_2,
        encode(i.ins_img_3, 'base64') AS ins_img_3,
        ST_X(ST_CENTROID(e.aa_geom)) as ins_geom_centroid_x,
        ST_Y(ST_CENTROID(e.aa_geom)) as ins_geom_centroid_y,
		e.aa_geom,        
        vi.ins_infraccion_tipos,
        vi.ins_infraccion_desc,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '1', 'g')) as count_leves,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '2', 'g')) as count_graves,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '3', 'g')) as count_muygraves
    from inspecciones i
    join v_infracciones_mercados vi on i.ins_uid = vi.ins_uid
    join mercados e on i.ent_mercados_uid = e.ent_uid
    join tipos_entidad_config tec on tec.tipo_entidad_id = i.tipo_entidad_id
    join tipos_entidad_idioma te on te.tipo_entidad_id = tec.tipo_entidad_id
    join config_cliente ce on ce.idioma = te.idioma
    left join areas_trabajo at on at.tipo_area_id = tec.tipo_area_id and ST_Contains(at.area_geom, e.aa_geom)

    union

    /*INSP. TERRAZAS*/
    select
        te.literal as entidad,
        e.ent_uid as entidad_uid,
        i.tipo_entidad_id,
        e.exp_numero as entidad_codigo,
        e.exp_direccion as entidad_direccion,
        at.area_descripcion,
        i.ins_uid,
        i.ins_identificador,
        i.ins_fecha_inspeccion,
        i.ins_tecnico,
        i.ins_asistencia,
        (select literal from tipos_asistencia_insp_idioma i inner join config_cliente c on i.idioma = c.idioma where asistencia_id = ins_asistencia) as ins_asistencia_literal,    
        i.ins_asistencia_desc,
        i.ins_representante,
        i.ins_tipo_documento,
        i.ins_num_documento,
        i.ins_mod_datos_pers,
        (select literal from tipos_modificaciones_insp_idioma i inner join config_cliente c on i.idioma = c.idioma where mod_id = ins_modificaciones_estado) as ins_modificaciones_estado,
        i.ins_observaciones,
        i.ins_enviado_geweb,
        i.ins_generica,
        i.ins_revision,
        i.ins_revision_desc,
        i.ins_infraccion,
        i.ins_finalizada,
        encode(i.ins_img_1, 'base64') AS ins_img_1,
        encode(i.ins_img_2, 'base64') AS ins_img_2,
        encode(i.ins_img_3, 'base64') AS ins_img_3,
        ST_X(ST_CENTROID(e.aa_geom)) as ins_geom_centroid_x,
        ST_Y(ST_CENTROID(e.aa_geom)) as ins_geom_centroid_y,
		e.aa_geom,        
        vi.ins_infraccion_tipos,
        vi.ins_infraccion_desc,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '1', 'g')) as count_leves,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '2', 'g')) as count_graves,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '3', 'g')) as count_muygraves
    from inspecciones i
    join v_infracciones_terrazas vi on i.ins_uid = vi.ins_uid
    join terrazas e on i.ent_terrazas_uid = e.ent_uid
    join tipos_entidad_config tec on tec.tipo_entidad_id = i.tipo_entidad_id
    join tipos_entidad_idioma te on te.tipo_entidad_id = tec.tipo_entidad_id
    join config_cliente ce on ce.idioma = te.idioma
    left join areas_trabajo at on at.tipo_area_id = tec.tipo_area_id and ST_Contains(at.area_geom, e.aa_geom) 


    union

    /*INSP. OBRAS*/
    select
        te.literal as entidad,
        e.ent_uid as entidad_uid,
        i.tipo_entidad_id,
        e.exp_numero as entidad_codigo,
        e.exp_direccion as entidad_direccion,
        at.area_descripcion,
        i.ins_uid,
        i.ins_identificador,
        i.ins_fecha_inspeccion,
        i.ins_tecnico,
        i.ins_asistencia,
        (select literal from tipos_asistencia_insp_idioma i inner join config_cliente c on i.idioma = c.idioma where asistencia_id = ins_asistencia) as ins_asistencia_literal,    
        i.ins_asistencia_desc,
        i.ins_representante,
        i.ins_tipo_documento,
        i.ins_num_documento,
        i.ins_mod_datos_pers,
        (select literal from tipos_modificaciones_insp_idioma i inner join config_cliente c on i.idioma = c.idioma where mod_id = ins_modificaciones_estado) as ins_modificaciones_estado,
        i.ins_observaciones,
        i.ins_enviado_geweb,
        i.ins_generica,
        i.ins_revision,
        i.ins_revision_desc,
        i.ins_infraccion,
        i.ins_finalizada,
        encode(i.ins_img_1, 'base64') AS ins_img_1,
        encode(i.ins_img_2, 'base64') AS ins_img_2,
        encode(i.ins_img_3, 'base64') AS ins_img_3,
        ST_X(ST_CENTROID(e.aa_geom)) as ins_geom_centroid_x,
        ST_Y(ST_CENTROID(e.aa_geom)) as ins_geom_centroid_y,
		e.aa_geom,        
        vi.ins_infraccion_tipos,
        vi.ins_infraccion_desc,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '1', 'g')) as count_leves,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '2', 'g')) as count_graves,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '3', 'g')) as count_muygraves
    from inspecciones i
    join v_infracciones_obras vi on i.ins_uid = vi.ins_uid
    join obras e on i.ent_obras_uid = e.ent_uid
    join tipos_entidad_config tec on tec.tipo_entidad_id = i.tipo_entidad_id
    join tipos_entidad_idioma te on te.tipo_entidad_id = tec.tipo_entidad_id
    join config_cliente ce on ce.idioma = te.idioma
    left join areas_trabajo at on at.tipo_area_id = tec.tipo_area_id and ST_Contains(at.area_geom, e.aa_geom) 


   union

    /*INSP. OVP*/
    select
        te.literal as entidad,
        e.ent_uid as entidad_uid,
        i.tipo_entidad_id,
        e.exp_numero as entidad_codigo,
        e.exp_direccion as entidad_direccion,
        at.area_descripcion,
        i.ins_uid,
        i.ins_identificador,
        i.ins_fecha_inspeccion,
        i.ins_tecnico,
        i.ins_asistencia,
        (select literal from tipos_asistencia_insp_idioma i inner join config_cliente c on i.idioma = c.idioma where asistencia_id = ins_asistencia) as ins_asistencia_literal,    
        i.ins_asistencia_desc,
        i.ins_representante,
        i.ins_tipo_documento,
        i.ins_num_documento,
        i.ins_mod_datos_pers,
        (select literal from tipos_modificaciones_insp_idioma i inner join config_cliente c on i.idioma = c.idioma where mod_id = ins_modificaciones_estado) as ins_modificaciones_estado,
        i.ins_observaciones,
        i.ins_enviado_geweb,
        i.ins_generica,
        i.ins_revision,
        i.ins_revision_desc,
        i.ins_infraccion,
        i.ins_finalizada,
        encode(i.ins_img_1, 'base64') AS ins_img_1,
        encode(i.ins_img_2, 'base64') AS ins_img_2,
        encode(i.ins_img_3, 'base64') AS ins_img_3,
        ST_X(ST_CENTROID(e.aa_geom)) as ins_geom_centroid_x,
        ST_Y(ST_CENTROID(e.aa_geom)) as ins_geom_centroid_y,
		e.aa_geom,        
        vi.ins_infraccion_tipos,
        vi.ins_infraccion_desc,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '1', 'g')) as count_leves,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '2', 'g')) as count_graves,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '3', 'g')) as count_muygraves
    from inspecciones i
    join v_infracciones_ovp vi on i.ins_uid = vi.ins_uid
    join ovp e on i.ent_ovp_uid = e.ent_uid
    join tipos_entidad_config tec on tec.tipo_entidad_id = i.tipo_entidad_id
    join tipos_entidad_idioma te on te.tipo_entidad_id = tec.tipo_entidad_id
    join config_cliente ce on ce.idioma = te.idioma
    left join areas_trabajo at on at.tipo_area_id = tec.tipo_area_id and ST_Contains(at.area_geom, e.aa_geom) 


    union

    /*INSP. ACTIVIDADES*/
    select
        te.literal as entidad,
        e.ent_uid as entidad_uid,
        i.tipo_entidad_id,
        e.ent_actividad_nombre_act as entidad_codigo,
        e.ent_actividad_direccion as entidad_direccion,         
        at.area_descripcion,
        i.ins_uid,
        i.ins_identificador,
        i.ins_fecha_inspeccion,
        i.ins_tecnico,
        i.ins_asistencia,
        (select literal from tipos_asistencia_insp_idioma i inner join config_cliente c on i.idioma = c.idioma where asistencia_id = ins_asistencia) as ins_asistencia_literal,    
        i.ins_asistencia_desc,
        i.ins_representante,
        i.ins_tipo_documento,
        i.ins_num_documento,
        i.ins_mod_datos_pers,
        (select literal from tipos_modificaciones_insp_idioma i inner join config_cliente c on i.idioma = c.idioma where mod_id = ins_modificaciones_estado) as ins_modificaciones_estado,
        i.ins_observaciones,
        i.ins_enviado_geweb,
        i.ins_generica,
        i.ins_revision,
        i.ins_revision_desc,
        i.ins_infraccion,
        i.ins_finalizada,
        encode(i.ins_img_1, 'base64') AS ins_img_1,
        encode(i.ins_img_2, 'base64') AS ins_img_2,
        encode(i.ins_img_3, 'base64') AS ins_img_3,
        ST_X(ST_CENTROID(e.aa_geom)) as ins_geom_centroid_x,
        ST_Y(ST_CENTROID(e.aa_geom)) as ins_geom_centroid_y,
		e.aa_geom,        
        vi.ins_infraccion_tipos,
        vi.ins_infraccion_desc,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '1', 'g')) as count_leves,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '2', 'g')) as count_graves,
        (SELECT COUNT(*) FROM regexp_matches(vi.ins_infraccion_tipos_id, '3', 'g')) as count_muygraves
    from inspecciones i
    join v_infracciones_actividades vi on i.ins_uid = vi.ins_uid
    join actividades e on i.ent_actividades_uid = e.ent_uid
    join tipos_entidad_config tec on tec.tipo_entidad_id = i.tipo_entidad_id
    join tipos_entidad_idioma te on te.tipo_entidad_id = tec.tipo_entidad_id
    join config_cliente ce on ce.idioma = te.idioma
    left join areas_trabajo at on at.tipo_area_id = tec.tipo_area_id and ST_Contains(at.area_geom, e.aa_geom)
