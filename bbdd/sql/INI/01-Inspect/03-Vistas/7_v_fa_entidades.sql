DROP VIEW IF EXISTS public.v_fa_entidades;

CREATE OR REPLACE VIEW public.v_fa_entidades AS
 SELECT 1 AS tipo_entidad_id,
    mercados.ent_uid,
    mercados.aa_geom,
    mercados.exp_numero AS descripcion,    
    st_x(st_centroid(mercados.aa_geom)) AS geom_centroid_x,
    st_y(st_centroid(mercados.aa_geom)) AS geom_centroid_y
   FROM mercados
UNION
 SELECT 2 AS tipo_entidad_id,
    terrazas.ent_uid,
    terrazas.aa_geom,
    terrazas.exp_numero AS descripcion,    
    st_x(st_centroid(terrazas.aa_geom)) AS geom_centroid_x,
    st_y(st_centroid(terrazas.aa_geom)) AS geom_centroid_y
   FROM terrazas
UNION
 SELECT 3 AS tipo_entidad_id,
    obras.ent_uid,
    obras.aa_geom,
    obras.exp_numero AS descripcion,    
    st_x(st_centroid(obras.aa_geom)) AS geom_centroid_x,
    st_y(st_centroid(obras.aa_geom)) AS geom_centroid_y
   FROM obras
UNION
 SELECT 4 AS tipo_entidad_id,
    ovp.ent_uid,
    ovp.aa_geom,
    ovp.exp_numero AS descripcion,    
    st_x(st_centroid(ovp.aa_geom)) AS geom_centroid_x,
    st_y(st_centroid(ovp.aa_geom)) AS geom_centroid_y
   FROM ovp
UNION
 SELECT 5 AS tipo_entidad_id,
    actividades.ent_uid,
    actividades.aa_geom,
    actividades.ent_actividad_numero_documento AS descripcion,    
    st_x(st_centroid(actividades.aa_geom)) AS geom_centroid_x,
    st_y(st_centroid(actividades.aa_geom)) AS geom_centroid_y
   FROM actividades;

