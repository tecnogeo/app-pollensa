DROP VIEW IF EXISTS V_INFRACCIONES_TERRAZAS;


CREATE OR REPLACE VIEW V_INFRACCIONES_TERRAZAS AS

select
    i.ins_uid,
    i.ins_identificador,
    array_to_string(array_agg(te.ins_infraccion_tipos_id), ','::text) as ins_infraccion_tipos_id,
	case when 
		i.ins_infraccion is true then array_to_string(array_agg(te.ins_infraccion_tipos::text), ','::text)
		else (select ti.literal from tipos_infracciones t left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id  inner join config_cliente c on ti.idioma = c.idioma  where t.tipo_inf_id = 0)
	END AS ins_infraccion_tipos,
    array_to_string(array_agg((te.ins_infraccion_tipos::text || ': '::text) || te.ins_infraccion_desc::text), '<br>'::text) AS ins_infraccion_desc
  from 
   (SELECT 
        t.ins_uid,
		t.ins_infraccion, 
            unnest(ARRAY[t.inf_001, t.inf_002, t.inf_003, t.inf_004, t.inf_005, t.inf_006, t.inf_007, t.inf_008, t.inf_009, t.inf_010, t.inf_011, t.inf_012, t.inf_013, t.inf_014, t.inf_015, t.inf_016, t.inf_017, t.inf_018, t.inf_019, t.inf_020, t.inf_021, t.inf_022, t.inf_023, t.inf_024, t.inf_025, t.inf_026, t.inf_027, t.inf_028, t.inf_029, t.inf_030, t.inf_031, t.inf_032, t.inf_033, t.inf_034, t.inf_035]) AS ins_infraccion_desc,
            unnest(ARRAY[t.inf_tipo_id_001, t.inf_tipo_id_002, t.inf_tipo_id_003, t.inf_tipo_id_004, t.inf_tipo_id_005, t.inf_tipo_id_006, t.inf_tipo_id_007, t.inf_tipo_id_008, t.inf_tipo_id_009, t.inf_tipo_id_010, t.inf_tipo_id_011, t.inf_tipo_id_012, t.inf_tipo_id_013, t.inf_tipo_id_014, t.inf_tipo_id_015, t.inf_tipo_id_016, t.inf_tipo_id_017, t.inf_tipo_id_018, t.inf_tipo_id_019, t.inf_tipo_id_020, t.inf_tipo_id_021, t.inf_tipo_id_022, t.inf_tipo_id_023, t.inf_tipo_id_024, t.inf_tipo_id_025, t.inf_tipo_id_026, t.inf_tipo_id_027, t.inf_tipo_id_028, t.inf_tipo_id_029, t.inf_tipo_id_030, t.inf_tipo_id_031, t.inf_tipo_id_032, t.inf_tipo_id_033, t.inf_tipo_id_034, t.inf_tipo_id_035]) AS ins_infraccion_tipos_id,
            unnest(ARRAY[t.inf_tipo_001, t.inf_tipo_002, t.inf_tipo_003, t.inf_tipo_004, t.inf_tipo_005, t.inf_tipo_006, t.inf_tipo_007, t.inf_tipo_008, t.inf_tipo_009, t.inf_tipo_010, t.inf_tipo_011, t.inf_tipo_012, t.inf_tipo_013, t.inf_tipo_014, t.inf_tipo_015, t.inf_tipo_016, t.inf_tipo_017, t.inf_tipo_018, t.inf_tipo_019, t.inf_tipo_020, t.inf_tipo_021, t.inf_tipo_022, t.inf_tipo_023, t.inf_tipo_024, t.inf_tipo_025, t.inf_tipo_026, t.inf_tipo_027, t.inf_tipo_028, t.inf_tipo_029, t.inf_tipo_030, t.inf_tipo_031, t.inf_tipo_032, t.inf_tipo_033, t.inf_tipo_034, t.inf_tipo_035]) AS ins_infraccion_tipos
          FROM ( SELECT 
                  inspecciones.ins_uid,	
                  inspecciones.ins_infraccion,
                  CASE
                      WHEN inspecciones.ins_inf_001 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_001' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_001,
                  CASE 
                      WHEN inspecciones.ins_inf_001 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_001' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_001,
                  CASE
                      WHEN inspecciones.ins_inf_001 = true THEN ( select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_001' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_001,
                  CASE
                      WHEN inspecciones.ins_inf_002 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_002' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_002,                  
                  CASE 
                      WHEN inspecciones.ins_inf_002 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_002' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_002,
                  CASE
                      WHEN inspecciones.ins_inf_002 = true THEN ( select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_002' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_002,
                  CASE
                      WHEN inspecciones.ins_inf_003 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_003' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_003,
                  CASE 
                      WHEN inspecciones.ins_inf_003 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_003' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_003,
                  CASE
                      WHEN inspecciones.ins_inf_003 = true THEN ( select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_003' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_003,
                  CASE
                      WHEN inspecciones.ins_inf_004 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_004' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_004,
                  CASE 
                      WHEN inspecciones.ins_inf_004 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_004' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_004,
                  CASE
                      WHEN inspecciones.ins_inf_004 = true THEN ( select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_004' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_004,
                  CASE
                      WHEN inspecciones.ins_inf_005 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_005' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_005,
                  CASE 
                      WHEN inspecciones.ins_inf_005 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_005' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_005,
                  CASE
                      WHEN inspecciones.ins_inf_005 = true THEN ( select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_005' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_005,
                  CASE
                      WHEN inspecciones.ins_inf_006 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_006' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_006,
                  CASE 
                      WHEN inspecciones.ins_inf_006 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_006' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_006,
                  CASE
                      WHEN inspecciones.ins_inf_006 = true THEN ( select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_006' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_006,
                  CASE
                      WHEN inspecciones.ins_inf_007 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_007' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_007,
                  CASE 
                      WHEN inspecciones.ins_inf_007 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_007' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_007,
                  CASE
                      WHEN inspecciones.ins_inf_007 = true THEN ( select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_007' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_007,
                  CASE
                      WHEN inspecciones.ins_inf_008 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_008' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_008,
                  CASE 
                      WHEN inspecciones.ins_inf_008 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_008' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_008,
                  CASE
                      WHEN inspecciones.ins_inf_008 = true THEN ( select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_008' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_008,
                  CASE
                      WHEN inspecciones.ins_inf_009 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_009' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_009,
                  CASE 
                      WHEN inspecciones.ins_inf_009 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_009' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_009,
                  CASE
                      WHEN inspecciones.ins_inf_009 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_009' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_009,
                  CASE
                      WHEN inspecciones.ins_inf_010 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_010' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_010,
                  CASE 
                      WHEN inspecciones.ins_inf_010 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_010' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_010,
                  CASE
                      WHEN inspecciones.ins_inf_010 = true THEN (select ti.literal as ins_infraccion_tipos
                          from infracciones ic
                          left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                          left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                          inner join config_cliente c on ti.idioma = c.idioma
                          where lower(ic.inf_codigo) = 'ins_inf_010' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_010,
                  CASE
                      WHEN inspecciones.ins_inf_011 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_011' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_011,
                  CASE 
                      WHEN inspecciones.ins_inf_011 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_011' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_011,
                  CASE
                      WHEN inspecciones.ins_inf_011 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_011' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_011,
                  CASE
                      WHEN inspecciones.ins_inf_012 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_012' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_012,
                  CASE 
                      WHEN inspecciones.ins_inf_012 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_012' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_012,
                  CASE
                      WHEN inspecciones.ins_inf_012 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_012' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_012,
                  CASE
                      WHEN inspecciones.ins_inf_013 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_013' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_013,
                  CASE 
                      WHEN inspecciones.ins_inf_013 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_013' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_013,
                  CASE
                      WHEN inspecciones.ins_inf_013 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_013' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_013,
                  CASE
                      WHEN inspecciones.ins_inf_014 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_014' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_014,
                  CASE 
                      WHEN inspecciones.ins_inf_014 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_014' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_014,
                  CASE
                      WHEN inspecciones.ins_inf_014 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_014' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_014,
			CASE
                      WHEN inspecciones.ins_inf_015 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_015' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_015,
                  CASE 
                      WHEN inspecciones.ins_inf_015 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_015' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_015,
                  CASE
                      WHEN inspecciones.ins_inf_015 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_015' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_015,
			CASE
                      WHEN inspecciones.ins_inf_016 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_016' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_016,
                  CASE 
                      WHEN inspecciones.ins_inf_016 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_016' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_016,
                  CASE
                      WHEN inspecciones.ins_inf_016 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_016' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_016,
			CASE
                      WHEN inspecciones.ins_inf_017 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_017' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_017,
                  CASE 
                      WHEN inspecciones.ins_inf_017 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_017' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_017,
                  CASE
                      WHEN inspecciones.ins_inf_017 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_017' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_017,
				          CASE
                      WHEN inspecciones.ins_inf_018 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_018' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_018,
                  CASE 
                      WHEN inspecciones.ins_inf_018 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_018' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_018,
                  CASE
                      WHEN inspecciones.ins_inf_018 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_018' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_018,
				          CASE
                      WHEN inspecciones.ins_inf_019 = true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_019' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_019,
                  CASE 
                      WHEN inspecciones.ins_inf_019 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_019' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_019,
                  CASE
                      WHEN inspecciones.ins_inf_019 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_019' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_019,
				          CASE
                      WHEN inspecciones.ins_inf_020= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_020' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_020,
                  CASE 
                      WHEN inspecciones.ins_inf_020 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_020' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_020,
                  CASE
                      WHEN inspecciones.ins_inf_020 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_020' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_020,
			CASE
                      WHEN inspecciones.ins_inf_021= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_021' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_021,
                  CASE 
                      WHEN inspecciones.ins_inf_021 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_021' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_021,
                  CASE
                      WHEN inspecciones.ins_inf_021 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_021' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_021,
				          CASE
                      WHEN inspecciones.ins_inf_022= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_022' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_022,
                  CASE 
                      WHEN inspecciones.ins_inf_022 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_022' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_022,
                  CASE
                      WHEN inspecciones.ins_inf_022 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_022' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_022,	
				          CASE
                      WHEN inspecciones.ins_inf_023= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_023' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_023,
                  CASE 
                      WHEN inspecciones.ins_inf_023 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_023' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_023,
                  CASE
                      WHEN inspecciones.ins_inf_023 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_023' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_023,
				          CASE
                      WHEN inspecciones.ins_inf_024= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_024' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_024,
                  CASE 
                      WHEN inspecciones.ins_inf_024 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_024' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_024,
                  CASE
                      WHEN inspecciones.ins_inf_024 = true THEN ( select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_024' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_024,
			CASE
                      WHEN inspecciones.ins_inf_025= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_025' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_025,
                  CASE 
                      WHEN inspecciones.ins_inf_025 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_025' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_025,
                  CASE
                      WHEN inspecciones.ins_inf_025 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_025' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_025,
				          CASE
                      WHEN inspecciones.ins_inf_026= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_026' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_026,
                  CASE 
                      WHEN inspecciones.ins_inf_026 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_026' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_026,
                  CASE
                      WHEN inspecciones.ins_inf_026 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_026' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_026,
				          CASE
                      WHEN inspecciones.ins_inf_027= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_027' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_027,
                  CASE 
                      WHEN inspecciones.ins_inf_027 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_027' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_027,
                  CASE
                      WHEN inspecciones.ins_inf_027 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_027' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_027,
				          CASE
                      WHEN inspecciones.ins_inf_028= true THEN (select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_028' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_028,
                  CASE 
                      WHEN inspecciones.ins_inf_028 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_028' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_028,
                  CASE
                      WHEN inspecciones.ins_inf_028 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_028' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_028,
				          CASE
                      WHEN inspecciones.ins_inf_029= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_029' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_029,
                  CASE 
                      WHEN inspecciones.ins_inf_029 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_029' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_029,
                  CASE
                      WHEN inspecciones.ins_inf_029 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_029' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_029,
				          CASE
                      WHEN inspecciones.ins_inf_030= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_030' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_030,
                  CASE 
                      WHEN inspecciones.ins_inf_030 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_030' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_030,
                  CASE
                      WHEN inspecciones.ins_inf_030 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_030' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_030,	
				          CASE
                      WHEN inspecciones.ins_inf_031= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_031' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_031,
                  CASE 
                      WHEN inspecciones.ins_inf_031 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_031' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_031,
                  CASE
                      WHEN inspecciones.ins_inf_031 = true THEN ( select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_031' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_031,
				          CASE
                      WHEN inspecciones.ins_inf_032= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_032' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_032,
                  CASE 
                      WHEN inspecciones.ins_inf_032 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_032' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_032,
                  CASE
                      WHEN inspecciones.ins_inf_032 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_032' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_032,
				          CASE
                      WHEN inspecciones.ins_inf_033= true THEN (select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_033' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_033,
                  CASE 
                      WHEN inspecciones.ins_inf_033 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_033' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_033,
                  CASE
                      WHEN inspecciones.ins_inf_033 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_033' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_033,
				          CASE
                      WHEN inspecciones.ins_inf_034= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_034' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_034,
                  CASE 
                      WHEN inspecciones.ins_inf_034 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_034' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_034,
                  CASE
                      WHEN inspecciones.ins_inf_034 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_034' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_034,
				          CASE
                      WHEN inspecciones.ins_inf_035= true THEN ( select i.literal as inf_descripcion
                            from infracciones_idioma i
                            inner join config_cliente c on i.idioma = c.idioma
                            where lower(inf_codigo) = 'ins_inf_035' and tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_035,
                  CASE 
                      WHEN inspecciones.ins_inf_035 = true THEN ( select tipo_inf_id from infracciones i where lower(inf_codigo) = 'ins_inf_035' and tipo_entidad_id = 2) 
                  END AS inf_tipo_id_035,
                  CASE
                      WHEN inspecciones.ins_inf_035 = true THEN (select ti.literal as ins_infraccion_tipos
                            from infracciones ic
                            left join tipos_infracciones t on ic.tipo_inf_id = t.tipo_inf_id
                            left join tipos_infracciones_idioma ti on t.tipo_inf_id = ti.tipo_inf_id
                            inner join config_cliente c on ti.idioma = c.idioma
                            where lower(ic.inf_codigo) = 'ins_inf_035' and ic.tipo_entidad_id = 2)
                      ELSE NULL::character varying
                  END AS inf_tipo_035
              FROM inspecciones) t) te
  join inspecciones i on i.ins_uid = te.ins_uid
  where tipo_entidad_id = 2
  group by i.ins_uid



