DO $$ 
DECLARE
	result boolean;
BEGIN   

    /*==============================================================*/
    /* INFRACCIONES ENTIDADES                                       */
    /* Añadir queries configuradas en el Excel                      */
    /* OBLIGATORIO                                                  */
    /*==============================================================*/

    --MERCADOS
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_001', 1, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_002', 1, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_003', 1, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_004', 1, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_005', 1, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_006', 1, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_007', 1, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_008', 1, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_009', 1, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_010', 1, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_011', 1, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_012', 1, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_013', 1, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_014', 1, 3);

    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_001', 1,'ca-ES','Incomplir l''horari autoritzat.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_002', 1,'ca-ES','Començar a instal·lar o muntar els llocs abans de les 07:30 hores.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_003', 1,'ca-ES','L''ús de qualsevol dispositiu sonor amb fins de propaganda, reclam, avís o distracció.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_004', 1,'ca-ES','Col·locar la mercaderia en els espais destinats a passadissos i espais entre llocs.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_005', 1,'ca-ES','Aparcar el vehicle del titular durant l''horari de celebració del mercadet a l''espai reservat per a la ubicació del lloc.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_006', 1,'ca-ES','No exhibir l''autorització municipal pertinent i no presentar-la a requeriment de l''autoritat.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_007', 1,'ca-ES','No procedir a netejar el lloc una vegada finalitzada la jornada.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_008', 1,'ca-ES','Qualsevol altra acció o omissió que constitueixi incompliment dels preceptes d''aquesta ordenança i que no estigui tipificada com infracció greu o molt greu.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_009', 1,'ca-ES','Incomplir qualsevol de les condicions imposades a l''autorització per a l''exercici de la venda ambulant');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_010', 1,'ca-ES','Exercir l''activitat persones diferents a les autoritzades.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_011', 1,'ca-ES','No estar al corrent del pagament dels tributs corresponents per a la instal·lació del lloc en el mercadet.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_012', 1,'ca-ES','Instal·lar llocs o exercir l''activitat sense autorització municipal.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_013', 1,'ca-ES','El desacatament, resistència, coacció o amenaça a l''autoritat municipal, funcionaris i agents d''aquesta, en compliment de la seva missió.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_014', 1,'ca-ES','La cessió, traspàs, lloguer de lloc o qualsevol altra forma d''exercici de l''activitat que no sigui la realitzada pel titular de l''autorització municipal.');


    --TERRAZAS
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_001', 2, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_002', 2, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_003', 2, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_004', 2, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_005', 2, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_006', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_007', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_008', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_009', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_010', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_011', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_012', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_013', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_014', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_015', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_016', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_017', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_018', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_019', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_020', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_021', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_022', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_023', 2, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_024', 2, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_025', 2, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_026', 2, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_027', 2, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_028', 2, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_029', 2, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_030', 2, 3);

    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_001', 2,'ca-ES','No exhibir en lloc visible la cèdula de la terrassa, de manera que no sigui possible llegir-ne el contingut des de l’exterior del local. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_002', 2,'ca-ES','Excedir l’ocupació de la terrassa fins al 20 per cent dels mòduls o de la superfície autoritzats. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_003', 2,'ca-ES','Incomplir l’horari d’inici o de tancament de la terrassa en trenta minuts o menys. d) Mancar al decòrum en l''acurat manteniment de les plantes o els seus suports (jardineres). ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_004', 2,'ca-ES','Incomplir les obligacions de neteja en la terrassa i el seu entorn, tant durant l’activitat com immediatament després de tancar-la al públic. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_005', 2,'ca-ES','Qualsevol incompliment de les obligacions d’aquesta ordenança que no sigui constitutiu d’infracció greu o molt greu.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_006', 2,'ca-ES','Instal·lar una terrassa a l’espai d’ús públic sense llicència o fora del període autoritzat ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_007', 2,'ca-ES','Incomplir l’horari d’inici o tancament de la terrassa en més de trenta minuts i en menys de seixanta minuts. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_008', 2,'ca-ES','Incomplir tres o més vegades els requeriments específics formulats per l’Administració Municipal. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_009', 2,'ca-ES','Excedir l’ocupació de la terrassa en més del 20 per cent i fins al 50 per cent dels mòduls o de la superfície autoritzats. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_010', 2,'ca-ES','Reduir amb l’excés d’ocupació autoritzada, l’ample lliure de la vorera de forma que pertorbi el pas dels vianants. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_011', 2,'ca-ES','Instal·lar a les terrasses elements no autoritzats. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_012', 2,'ca-ES','Produir molèsties al veïnat derivades del funcionament de la terrassa amb incompliments de les condicions establertes en aquesta ordenança.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_013', 2,'ca-ES','Instal·lar equips musicals i altres elements de megafonia. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_014', 2,'ca-ES','Incomplir l’obligació de retirar i/o recollir els elements de les terrasses en finalitzar el seu horari de funcionament. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_015', 2,'ca-ES','Tancar els límits de la terrassa amb materials no autoritzats, siguin o no transparents. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_016', 2,'ca-ES','Exhibir qualsevol mena de publicitat no autoritzada als elements de la terrassa. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_017', 2,'ca-ES','Servir a la terrassa productes alimentaris no autoritzats a l’establiment de restauració o assimilat al que està vinculada. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_018', 2,'ca-ES','Presentar en els procediments relatius a les terrasses documents o dades que no s’ajustin a la realitat. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_019', 2,'ca-ES','No disposar de la pòlissa d’assegurança en vigor. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_020', 2,'ca-ES','Cedir l’explotació de la terrassa a una persona que no sigui el titular de la llicència.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_021', 2,'ca-ES','No efectuar la comunicació de la transmissió de llicència de terrassa.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_022', 2,'ca-ES','Tolerar la celebració d’actuacions en viu no autoritzades. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_023', 2,'ca-ES','Impedir o obstaculitzar la funció inspectora. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_024', 2,'ca-ES','Instal·lar una terrassa a l’espai d’ús públic sense llicència o fora del període autoritzat, si això comporta una pertorbació rellevant de la convivència que afecti de manera greu, immediata i directa la tranquil·litat o l’exercici de drets legítims d’altres persones, o el normal desenvolupament d’activitats de tota classe o la salubritat o l’ornament públics. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_025', 2,'ca-ES','Incomplir l’horari de tancament en més de seixanta minuts, si això comporta una pertorbació rellevant de la convivència que afecti de manera greu, immediata i directa la tranquil·litat o l’exercici de drets legítims d’altres persones, o el normal desenvolupament d’activitats de tota classe o la salubritat o l’ornament públics. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_026', 2,'ca-ES','Incomplir tres o més vegades els requeriments específics formulats per l’Administració municipal, si comporta una pertorbació rellevant de la convivència que afecti de manera greu, immediata i directa la tranquil·litat o l’exercici de drets legítims d’altres persones o al normal desenvolupament d’activitats de tota classe o a la salubritat o a l’ornament públics, i serà d’aplicació el disposat a l’article 66. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_027', 2,'ca-ES','Excedir l’ocupació de la terrassa en més del 50 per cent dels mòduls o de la superfície autoritzats ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_028', 2,'ca-ES','Reduir amb l’excés d’ocupació autoritzada l’ample lliure de la vorera de forma que pertorbi greument o impedeixi el pas dels vianants. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_029', 2,'ca-ES','Produir molèsties greus al veïnat derivades del funcionament de la terrassa amb incompliments de les condicions establertes en aquesta ordenança. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_030', 2,'ca-ES','Promoure la celebració d’actuacions en viu no autoritzades. OMTerrasses: Aprovació definitiva ');


    --OBRAS
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_001', 3, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_002', 3, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_003', 3, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_004', 3, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_005', 3, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_006', 3, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_007', 3, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_008', 3, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_009', 3, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_010', 3, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_011', 3, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_012', 3, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_013', 3, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_014', 3, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_015', 3, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_016', 3, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_017', 3, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_018', 3, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_019', 3, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_020', 3, 3);

    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_001', 3,'ca-ES','L’incompliment, en sòl urbà i en sòl urbanitzable delimitat, de les determinacions urbanístiques sobre règim d’indivisibilitat de finques i sobre edificació. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_002', 3,'ca-ES','La vulneració de l’ordenament jurídic urbanístic en sòl no urbanitzable no subjecte a protecció especial i en sòl urbanitzable sense planejament parcial definitivament aprovat, en els supòsits següents: Primer. En matèria d’ús del sòl i del subsòl, si l’actuació no comporta fer edificacions ni instal·lacions fixes. Segon. En matèria d’edificació, si l’actuació consisteix en la construcció d’elements auxiliars o complementaris d’un ús o una edificació preexistents legalment implantats. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_003', 3,'ca-ES','La vulneració, fins a un 10%, en sòl urbà o urbanitzable delimitat, dels paràmetres imperatius a què fa referència l’article 213.c. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_004', 3,'ca-ES','L’incompliment del deure de conservació dels terrenys, les urbanitzacions, les edificacions, els rètols i les instal·lacions en general en condicions de salubritat i decòrum públic. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_005', 3,'ca-ES','Els actes a què fan referència els articles 213 i 214 que siguin legalitzables i s’ajustin al que estableix l’article 216. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_006', 3,'ca-ES','Els actes de propaganda d’urbanitzacions, per mitjà d’anuncis, cartells, tanques publicitàries, fullets o cartes, per mitjans informàtics o per qualsevol altre sistema de divulgació o difusió que no expressin les dades referents a l’aprovació de l’instrument de planejament corresponent o que incloguin indicacions susceptibles d’induir els consumidors a error. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_007', 3,'ca-ES','La tala o l’abatiment d’arbres sense la llicència urbanística corresponent, si l’exigeixen el planejament urbanístic o les ordenances municipals. ');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_008', 3,'ca-ES','Els actes de parcel·lació, urbanització, edificació o ús del sòl conformes a la legislació i el planejament urbanístics que es duguin a terme sense el títol administratiu habilitant pertinent, sense efectuar la comunicació prèvia en substitució de la llicència urbanística requerida o sense ajustar-se al seu contingut.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_009', 3,'ca-ES','Els actes tipificats per l’article 213.a de la Llei d’urbanisme que es duguin a terme en sòl no urbanitzable altre que el que el planejament urbanístic classifica o ha de classificar com a sòl no urbanitzable en virtut de l’article 32.a de la Llei d’urbanisme, o en sòl urbanitzable no delimitat, o en terrenys reservats per a sistemes locals altres que els d’espais lliures, viari o equipaments comunitaris esportius públics.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_010', 3,'ca-ES','L’incompliment, en sòl urbà i en sòl urbanitzable delimitat, de les determinacions urbanístiques sobre urbanització, usos del sòl i del subsòl i parcel·lació urbanística.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_011', 3,'ca-ES','La vulneració, en més d’un 10% i fins a un 30%, en qualsevol classe de sòl, dels paràmetres imperatius a què fa referència l’article 213.c de la Llei d’urbanisme.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_012', 3,'ca-ES','L’incompliment del deure de conservació de terrenys, urbanitzacions, edificacions, rètols i instal·lacions en general, en condicions de seguretat.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_013', 3,'ca-ES','La tala o l’abatiment d’arbres integrants d’espais boscosos o d’arbredes protegits pel planejament urbanístic que no comporti la desaparició d’aquests espais o arbredes.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_014', 3,'ca-ES','La divisió o la segregació de terrenys en sòl no urbanitzable que no sigui objecte de cap règim de protecció especial, o bé en sòl urbanitzable no delimitat, en contra de les determinacions de la Llei d’urbanisme.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_015', 3,'ca-ES','Els supòsits tipificats per l’article 218.1 de la Llei d’urbanisme.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_016', 3,'ca-ES','Actes de parcel·lació urbanística, d’urbanització, d’ús del sòl i el subsòl i d’edificació contraris a l’ordenament jurídic en sòls no urbanitzables d’especial protecció, en terrenys reservats per a sistemes generals o bé per a sistemes locals d’espais lliures, sistema viari o equipaments comunitaris.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_017', 3,'ca-ES','La tala o abatiment d’arbres que comporti la desaparició d’espais boscosos o d’arbredes protegits.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_018', 3,'ca-ES','La vulneració, en més d’un 30%, en qualsevol classe de sòl, dels paràmetres imperatius establerts pel planejament urbanístic relatius a densitat d’habitatges, nombre d’establiments, sostre, alçària, volum, situació de les edificacions i ocupació permesa de la superfície de les finques o les parcel·les.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_019', 3,'ca-ES','La divisió o la segregació de terrenys en sòl no urbanitzable objecte d’algun règim de protecció especial, en contra de les determinacions d’aquesta Llei.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_020', 3,'ca-ES','La vulneració del règim d’usos i obres dels béns que el planejament urbanístic inclou en els catàlegs de béns protegits.');

        
    --OVP
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_001', 4, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_002', 4, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_003', 4, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_004', 4, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_005', 4, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_006', 4, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_007', 4, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_008', 4, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_009', 4, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_010', 4, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_011', 4, 3);

    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_001', 4,'ca-ES','L’ocupació excedeixi el 20 %de l''autoritzat.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_002', 4,'ca-ES','L’ocupació excedeixi el 10 % de l''autoritzat. (pg. Sant Joan de Déu)');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_003', 4,'ca-ES','Existeixen dues sancions lleus, o bé superen el 30% de l''autoritzat i són realitzades dins el període autoritzat.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_004', 4,'ca-ES','Existeixen dues sancions lleus, o bé superen el 15% de l''autoritzat i són realitzades dins el període autoritzat.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_005', 4,'ca-ES','No tenir la corresponent llicència municipal.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_006', 4,'ca-ES','Existeixen dues sancions greus, o bé superen el 40% autoritzat i són realitzades dins el període autoritzat.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_007', 4,'ca-ES','Instal·lació de tendals no autoritzats.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_008', 4,'ca-ES','Instal·lació de para-sols.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_009', 4,'ca-ES','No tenir la corresponent llicència municipal. (pg. Sant Joan de Déu)');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_010', 4,'ca-ES','Existeixen dues sancions greus o bé superen el 20% autoritzat i són realitzades dins el període autoritzat. (pg. Sant Joan de Déu)');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_011', 4,'ca-ES','La instal·lació de tendals i para-sols no autoritzats o que no segueixen el disseny homogeni autoritzat. (pg. Sant Joan de Déu)');

    

    --ACTIVIDADES
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_001', 5, 1);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_002', 5, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_003', 5, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_004', 5, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_005', 5, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_006', 5, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_007', 5, 2);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_008', 5, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_009', 5, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_010', 5, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_011', 5, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_012', 5, 3);
    insert into infracciones (inf_codigo, tipo_entidad_id, tipo_inf_id) values ('INS_INF_013', 5, 3);

    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_001', 5,'ca-ES','No prestar el operador afectado la asistencia que le fuera requerida por la autoridad competente para la ejecución de las medidas reparadoras, preventivas o de evitación, de acuerdo con lo establecido en el Art. 9 ,Ley 26/2007, de 23 de octubre');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_002', 5,'ca-ES','No adoptar las medidas preventivas o de evitación exigidas por la autoridad competente al operador en aplicación del Art. 17 ,Ley 26/2007, de 23 de octubre, cuando no sea constitutiva de infracción muy grave.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_003', 5,'ca-ES','No ajustarse a las instrucciones recibidas de la autoridad competente en aplicación del Art. 18 ,Ley 26/2007, de 23 de octubre al poner en práctica las medidas preventivas o las de evitación a que esté obligado el operador, cuando no sea constitutiva de infracción muy grave.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_004', 5,'ca-ES','No adoptar las medidas reparadoras exigidas al operador por la autoridad competente en aplicación del Art. 19 ,Ley 26/2007, de 23 de octubre, cuando no sea constitutiva de infracción muy grave.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_005', 5,'ca-ES','No ajustarse, a las instrucciones recibidas de la autoridad competente en aplicación del Art. 21 ,Ley 26/2007, de 23 de octubre a la hora de poner en práctica las medidas reparadoras a que esté obligado el operador, cuando no sea constitutiva de infracción muy grave.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_006', 5,'ca-ES','No informar a la autoridad competente de la existencia de un daño medioambiental o de una amenaza inminente de daño producido o que pueda producir el operador y de los que tuviera conocimiento, o hacerlo con injustificada demora, cuando no sea constitutiva de infracción muy grave.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_007', 5,'ca-ES','No facilitar la información requerida por la autoridad competente al operador, o hacerlo con retraso, de acuerdo con lo previsto en los Art. 18,Art. 21 ,Ley 26/2007, de 23 de octubre.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_008', 5,'ca-ES','No adoptar las medidas preventivas o de evitación exigidas por la autoridad competente al operador en aplicación del Art. 17 ,Ley 26/2007, de 23 de octubre, cuando ello tenga como resultado el daño que se pretendía evitar.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_009', 5,'ca-ES','No ajustarse a las instrucciones recibidas de la autoridad competente en aplicación del Art. 18 ,Ley 26/2007, de 23 de octubre a la hora de poner en práctica las medidas preventivas o de evitación a que esté obligado el operador, cuando ello tenga como resultado el daño que se pretendía evitar.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_010', 5,'ca-ES','No adoptar las medidas reparadoras exigibles al operador en aplicación de los Art. 19,Art. 20 ,Ley 26/2007, de 23 de octubre, cuando ello tenga como resultado un detrimento de la eficacia reparadora de tales medidas.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_011', 5,'ca-ES','No ajustarse a las instrucciones recibidas de la autoridad competente en aplicación del Art. 21 ,Ley 26/2007, de 23 de octubre al poner en práctica las medidas reparadoras a que esté obligado el operador, cuando ello tenga como resultado un detrimento de la eficacia reparadora de tales medidas.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_012', 5,'ca-ES','No informar a la autoridad competente de la existencia de un daño medioambiental o de una amenaza inminente de daño producido o que pueda producir el operador y de los que tuviera conocimiento, o hacerlo con injustificada demora, cuando ello tuviera como consecuencia que sus efectos se agravaran o llegaran a producirse efectivamente.');
    insert into infracciones_idioma (inf_codigo, tipo_entidad_id, idioma, literal) values ('INS_INF_013', 5,'ca-ES','El incumplimiento de la obligación de concertar en los términos previstos en esta ley las garantías financieras a que esté obligado el operador, así como el hecho de que no se mantengan en vigor el tiempo que subsista dicha obligación.');

  

    
    /*==============================================================*/
    /* AREAS DE TRABAJO                                             */
    /* Añadir queries configuradas en el Excel                      */
    /* OPCIONAL                                                     */
    /*==============================================================*/    
    insert into tipos_areas_trabajo (tipo_area_id, tipo_area_codigo) values (2,'AREA MERCADOS');
    insert into tipos_areas_trabajo (tipo_area_id, tipo_area_codigo) values (3,'BARRIOS');



    /*==============================================================*/
    /* GEOMETRIAS AREAS DE TRABAJO                                  */
    /* Añadir queries configuradas en el Excel                      */
    /* OPCIONAL                                                     */
    /*==============================================================*/
    INSERT INTO public.areas_trabajo select nextval('areas_trabajo_area_id_seq') as area_id, 2 as tipo_area_id, nom_areaas area_descripcion, geom as area_geom from data.areas_mercados;
    INSERT INTO public.areas_trabajo select nextval('areas_trabajo_area_id_seq') as area_id, 3 as tipo_area_id, descripcionas area_descripcion, aa_geom as area_geom from data.barrios;




    /*==============================================================*/
    /* CONFIGURACIÓN ENTIDADES                                      */
    /* Añadir queries configuradas en el Excel                      */
    /* OPCIONAL                                                     */
    /*==============================================================*/
    UPDATE tipos_entidad_config SET entidad_activa=true, tipo_area_id=2, hist_campo_fecha_entidad='exp_fecha_alta', hist_intervalo_historico='-1 years', hist_intervalo_activo=null, hist_intervalo_unidad='year', hist_tipo_id=1 WHERE tipo_entidad_id=1;	
    UPDATE tipos_entidad_config SET entidad_activa=true, tipo_area_id=1, hist_campo_fecha_entidad='terraza_fecha_ini;terraza_fecha_fin', hist_intervalo_historico='-1 years', hist_intervalo_activo=null, hist_intervalo_unidad='year', hist_tipo_id=2 WHERE tipo_entidad_id=2;	
    UPDATE tipos_entidad_config SET entidad_activa=true, tipo_area_id=1, hist_campo_fecha_entidad='exp_fecha_alta', hist_intervalo_historico='-1 years', hist_intervalo_activo=null, hist_intervalo_unidad='year', hist_tipo_id=1 WHERE tipo_entidad_id=3;	
    UPDATE tipos_entidad_config SET entidad_activa=true, tipo_area_id=1, hist_campo_fecha_entidad='exp_fecha_alta', hist_intervalo_historico='-1 month', hist_intervalo_activo='-1 month', hist_intervalo_unidad='month', hist_tipo_id=1 WHERE tipo_entidad_id=4;	
    UPDATE tipos_entidad_config SET entidad_activa=true, tipo_area_id=3, hist_campo_fecha_entidad=null, hist_intervalo_historico=null, hist_intervalo_activo=null, hist_intervalo_unidad=null, hist_tipo_id=3 WHERE tipo_entidad_id=5;	





    /*==============================================================*/
    /* Setear el campo vigencia de las entidades                    */
    /*==============================================================*/
    select f_setearVigencia(1) into result;
    select f_setearVigencia(2) into result;
    select f_setearVigencia(3) into result;
    select f_setearVigencia(4) into result;
    select f_setearVigencia(5) into result;
    


  
END $$;


