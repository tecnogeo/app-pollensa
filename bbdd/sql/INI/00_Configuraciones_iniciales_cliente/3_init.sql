/*==============================================================*/
/* Extensions                                                   */
/*==============================================================*/
CREATE EXTENSION postgis;
CREATE EXTENSION "uuid-ossp";


/*==============================================================*/
/* Tabla: SIS_GLOBALES                                          */
/*==============================================================*/
create table SIS_GLOBALES (
   VARIABLE_NOMBRE      varchar(40)          not null,
   VARIABLE_VALOR       varchar(100)         not null
);
insert into SIS_GLOBALES (VARIABLE_NOMBRE, VARIABLE_VALOR) values ('VERSION.BLINSPECT.BLSURVEYS', '1.6.0');


/*==============================================================*/
/* Tabla: IDIOMAS                                                */
/*==============================================================*/
create table IDIOMAS(
   IDIOMA                     CHAR(5)                       not null,
   IDIOMA_DESCRIPCION         CHARACTER VARYING             not null,
   constraint PK_IDIOMAS primary key (IDIOMA)
);
insert into idiomas (IDIOMA, IDIOMA_DESCRIPCION) values ('ca-ES', 'Català');
insert into idiomas (IDIOMA, IDIOMA_DESCRIPCION) values ('es-ES', 'Español, alfabetización internacional');

/*==============================================================*/
/* Tabla: CONFIG_CLIENTE                                        */
/*==============================================================*/
create table CONFIG_CLIENTE(
   EPSG                       INTEGER                       not null,
   IDIOMA                     CHAR(5)                       REFERENCES IDIOMAS(IDIOMA),
   FORMATO_NUMEXP             CHARACTER VARYING(100)        not null
);
