
CREATE OR REPLACE FUNCTION surveys.f_crearRelacionEntidadDiccionario(
    nombre_entidad text,
    campo_entidad text,
    nombre_dicc text
)
RETURNS TEXT AS $$
DECLARE
    epsg INTEGER;
    sql_create_relationship text;

BEGIN

    -- tenir en compte que no existeixi ja la taula
    sql_create_relationship := 'ALTER TABLE surveys.'|| nombre_entidad ||' 
                        ADD CONSTRAINT FK_'|| nombre_entidad ||'_'|| campo_entidad ||' 
                        FOREIGN KEY ('|| campo_entidad|| ') 
                        REFERENCES surveys.'|| nombre_dicc ||' (id);';

    EXECUTE sql_create_relationship;

    RETURN 'OK';

EXCEPTION 
	WHEN others THEN				
		RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;
END
$$
LANGUAGE plpgsql;


--select surveys.f_crearRelacionEntidadDiccionario('arboles','nombre_comun', 'dicc_especies_nombre_comun');