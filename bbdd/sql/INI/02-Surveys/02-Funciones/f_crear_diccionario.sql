CREATE OR REPLACE FUNCTION surveys.f_crearDiccionario(
    nombre_dicc text,
    alias_dicc text
)
RETURNS TEXT AS $$
DECLARE

    sql_query text;
    sql_result text;

BEGIN

    -- validar que el nom del diccionari comenci per dicc_
    sql_query := 'select substring('''|| nombre_dicc||''', 1, 5)';
    EXECUTE sql_query INTO sql_result;

    IF sql_result = 'dicc_' THEN 
      
        -- crear tabla diccionario
        sql_query := 'CREATE TABLE surveys.' || nombre_dicc || ' ( 
                        id                    INTEGER                         NOT NULL,
                        valor                 CHARACTER VARYING(50)           NOT NULL,                    
                        constraint pk_dicc_'|| nombre_dicc ||' primary key (id)
                    );';
        EXECUTE sql_query;

        --Insert en la tabla inventario_entidades
        sql_query := 'INSERT INTO surveys.inventario_entidades (tipo_tabla, nombre_tabla, alias_tabla) values (''diccionario'', lower('''|| nombre_dicc ||'''), '''|| alias_dicc ||''');';        
        EXECUTE sql_query;


        RETURN 'OK';
    
    ELSE

        RETURN 'ERROR: La tabla diccionario debe empezar por la particula ''dicc_''';

    END IF;

    

EXCEPTION 
	WHEN others THEN				
		RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;

END
$$
LANGUAGE plpgsql;

--select surveys.f_crearDiccionario('dicc_especies_nombre_comun', 'Espècies nom comú');
