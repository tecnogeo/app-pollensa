CREATE OR REPLACE FUNCTION surveys.f_eliminarRelacionEntidadDiccionario(
    nombre_entidad text,
    campo_entidad text
)
RETURNS TEXT AS $$
DECLARE    
    sql_query text;

BEGIN

    -- eliminem la fk de la taula
    sql_query := 'ALTER TABLE surveys.' || nombre_entidad || ' DROP CONSTRAINT FK_'|| nombre_entidad ||'_'|| campo_entidad ||';';
    EXECUTE sql_query;


    RETURN 'OK';

EXCEPTION 
	WHEN others THEN
        RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;
END
$$
LANGUAGE plpgsql;


--select surveys.f_eliminarRelacionEntidadDiccionario('arboles', 'nombre_comun');
