CREATE OR REPLACE FUNCTION surveys.f_eliminarTabla(
    nombre_entidad text    
)
RETURNS TEXT AS $$
DECLARE    
    sql_query text;
    sql_result text;
BEGIN    

     -- validar si es un diccionario (empieza por dicc_)
    sql_query := 'select substring('''|| nombre_entidad||''', 1, 5)';
    EXECUTE sql_query INTO sql_result;

    
    IF sql_result <> 'dicc_' THEN 

        -- Eliminar tabla entidad_acciones
        sql_query := 'DROP TABLE surveys.'|| nombre_entidad ||'_acciones CASCADE;';
        EXECUTE sql_query;
        
        -- Eliminar tabla dicc_entidad_acciones
        sql_query := 'DROP TABLE surveys.dicc_'|| nombre_entidad ||'_acciones CASCADE;';
        EXECUTE sql_query;

    END IF;
  

    -- Eliminar tabla  
    sql_query := 'DROP TABLE surveys.'|| nombre_entidad ||' CASCADE;';
    EXECUTE sql_query;

    -- Eliminar registro de la tabla inventario_entidades
    sql_query := 'DELETE FROM surveys.inventario_entidades 
                  WHERE lower(nombre_tabla) = lower(''' || nombre_entidad || ''')
                  OR lower(nombre_tabla) = lower(''dicc_' || nombre_entidad || '_acciones'');';
    EXECUTE sql_query;


    RETURN 'OK';

EXCEPTION 
	WHEN others THEN		
        RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;
END
$$
LANGUAGE plpgsql;


--select surveys.f_eliminarTabla('arboles');
