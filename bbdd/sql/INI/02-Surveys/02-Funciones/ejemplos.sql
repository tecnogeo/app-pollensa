-- 1) Crear tabla entidad
select surveys.f_crearEntidad('arboles', 'Arbres', 'point');

-- 2) añadir con QGIS atributos adicionales (nombre_común)

-- 3) Crear tabla diccionario y relación
select surveys.f_crearDiccionario('dicc_especies_nombre_comun', 'Espècies nom comú');
select surveys.f_crearRelacionEntidadDiccionario('arboles','nombre_comun', 'dicc_especies_nombre_comun');

-- 4) Cargar datos diccionario


--=================================================================


-- Eliminar tabla individual (solo se borra la tabla indicada)
select surveys.f_eliminarTabla('arboles');
select surveys.f_eliminarTabla('dicc_especies_nombre_comun');

-- Eliminar relación 
select surveys.f_eliminarRelacionEntidadDiccionario('arboles', 'nombre_comun');

-- Eliminar entidad y todos sus diccionarios
select surveys.f_eliminarEntidad('arboles')