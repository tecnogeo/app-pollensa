CREATE OR REPLACE FUNCTION surveys.f_crearEntidad(
    nombre_entidad text,
    alias_entidad text,
    tipo_geom text
)
RETURNS TEXT AS $$
DECLARE
    epsg INTEGER;
    sql_query text;

BEGIN    

    -- Crear tabla entidad con los campos estandar
    SELECT c.epsg into epsg from config_cliente c;
    sql_query := 'CREATE TABLE surveys.' || nombre_entidad || ' ( 
                    uuid                    CHARACTER(36)                   NOT NULL,
                    id_visible              SERIAL                          NOT NULL,
                    fecha_alta              TIMESTAMP                       NULL,
                    fecha_revision          TIMESTAMP                       NULL,
                    fecha_baja              TIMESTAMP                       NULL,
                    observaciones           CHARACTER VARYING(500)          NULL,
                    usuario                 CHARACTER VARYING(30)           NULL,
                    geom                    Geometry('|| tipo_geom ||','||epsg||')    NULL,
                    imagen_1                 BYTEA                           NULL,                    
                    imagen_2                 BYTEA                           NULL,                    
                    imagen_3                 BYTEA                           NULL,
                    constraint PK_' || nombre_entidad || ' primary key (uuid)
                );';    
    EXECUTE sql_query;

    sql_query := 'CREATE INDEX IDX_GEOM_'|| nombre_entidad ||' ON surveys.'|| nombre_entidad ||' USING GIST (geom);';    
    EXECUTE sql_query;

    --Insert en la tabla inventario_entidades
    sql_query := 'INSERT INTO surveys.inventario_entidades (tipo_tabla, nombre_tabla, alias_tabla) values (''entidad'', lower('''|| nombre_entidad ||'''), '''|| alias_entidad ||''');';    
    EXECUTE sql_query;

    
    -- creamos tabla de acciones para la entidad
    sql_query := 'create table surveys.'|| nombre_entidad ||'_acciones (   
      uuid                CHARACTER(36)               not null,
      id_visible          SERIAL                      not null,
      uuid_entidad        CHARACTER(36)               REFERENCES surveys.'|| nombre_entidad ||'(uuid),  
      tipo_accion         INTEGER                     null,
      fecha_inicio        TIMESTAMP                   null,
      fecha_revision      TIMESTAMP                   null,
      fecha_fin           TIMESTAMP                   null,
      accion_finalizada   BOOLEAN                     null,
      observaciones       CHARACTER VARYING(500)      null,
      usuario             CHARACTER VARYING(30)       null,
      imagen              BYTEA                       null,
      constraint pk_'|| nombre_entidad ||'_acciones primary key (uuid)
    );';    
    EXECUTE sql_query;

    sql_query := 'select surveys.f_crearDiccionario(''dicc_'|| nombre_entidad ||'_acciones'', ''Acciones'');';
    EXECUTE sql_query;


    sql_query := 'select surveys.f_crearRelacionEntidadDiccionario('''|| nombre_entidad ||'_acciones'',''tipo_accion'', ''dicc_'|| nombre_entidad ||'_acciones'');';
    EXECUTE sql_query;


    RETURN 'OK';


EXCEPTION
	WHEN others THEN		
        RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;        

END
$$
LANGUAGE plpgsql;


--select surveys.f_crearEntidad('arboles', 'Arbres', 'point');
