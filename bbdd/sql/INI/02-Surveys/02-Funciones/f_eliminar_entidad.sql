CREATE OR REPLACE FUNCTION surveys.f_eliminarEntidad(
    nombre_entidad text 
)
RETURNS TEXT AS $$
DECLARE    
    sql_query text;
    sql_query_delete_dicc text;
    tabla_diccionario text;
    sql_record record;
BEGIN   

    -- Eliminar tabla entidad_acciones
    sql_query := 'DROP TABLE surveys.'|| nombre_entidad ||'_acciones CASCADE;';
    EXECUTE sql_query;
        
    -- Eliminar tabla dicc_entidad_acciones
    sql_query := 'DROP TABLE surveys.dicc_'|| nombre_entidad ||'_acciones CASCADE;';
    EXECUTE sql_query;

    -- Recuperar los diccionarios (si tiene) de los atributos adicionales y eliminarlos
    sql_query := 'SELECT
                    tc.table_schema, 
                    tc.constraint_name, 
                    tc.table_name, 
                    kcu.column_name, 
                    ccu.table_schema AS foreign_table_schema,
                    ccu.table_name AS foreign_table_name,
                    ccu.column_name AS foreign_column_name 
                  FROM 
                    information_schema.table_constraints AS tc 
                    JOIN information_schema.key_column_usage AS kcu
                    ON tc.constraint_name = kcu.constraint_name
                    AND tc.table_schema = kcu.table_schema
                    JOIN information_schema.constraint_column_usage AS ccu
                    ON ccu.constraint_name = tc.constraint_name
                    AND ccu.table_schema = tc.table_schema
                  WHERE tc.constraint_type = ''FOREIGN KEY'' 
                  AND  ccu.table_schema = ''surveys''
                  AND tc.table_name='''|| nombre_entidad ||''';';

    FOR sql_record IN EXECUTE sql_query
    LOOP
        tabla_diccionario := sql_record.foreign_table_name;
        sql_query_delete_dicc := 'DROP TABLE surveys.' || tabla_diccionario || ' CASCADE;';
        EXECUTE sql_query_delete_dicc;

        -- Eliminar registro de la tabla inventario_entidades
        sql_query_delete_dicc := 'DELETE FROM surveys.inventario_entidades 
                    WHERE lower(nombre_tabla) = lower(''' || tabla_diccionario || ''');';
        EXECUTE sql_query_delete_dicc;

    END LOOP;


    -- Eliminar tabla entidad
    sql_query := 'DROP TABLE surveys.'|| nombre_entidad ||';';
    EXECUTE sql_query;

    -- Eliminar registro de la tabla inventario_entidades
    sql_query := 'DELETE FROM surveys.inventario_entidades 
                  WHERE lower(nombre_tabla) = lower(''' || nombre_entidad || ''')
                  OR lower(nombre_tabla) = lower(''dicc_' || nombre_entidad || '_acciones'');';
    EXECUTE sql_query;

    RETURN 'OK';

EXCEPTION 
	WHEN others THEN		
        RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;
END
$$
LANGUAGE plpgsql;


--select surveys.f_eliminarEntidad('arboles');
