DO $$
DECLARE   
   
BEGIN

   /*==============================================================*/
   /* Tabla: inventario_entidades                                  */
   /*==============================================================*/
   create table surveys.inventario_entidades (
      ID                  SERIAL                      not null,
      TIPO_TABLA          CHARACTER VARYING(20)       not null,
      NOMBRE_TABLA        CHARACTER VARYING(63)       not null,
      ALIAS_TABLA         CHARACTER VARYING(100)      not null,      
      constraint pk_inventario_entidades primary key (ID)
   );



END $$;