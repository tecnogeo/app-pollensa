Iniciar nueva versión implica establecer esta en:
	- SQLBlinspect_changelog.txt (mientras no se distribuya, se marca como DESARROLLO).

Generar nueva versión implica:
	- Eliminar las marca (DESARROLLO) de changelog.txt.
	- Indicar la fecha de distribución (XX/XX/20XX)
	- Añadir update con la versión correspondiente en la tabla sis_version.	



BLInspect 1.6.0  (07/10/2022)
-------------------
    -[2022/10/07_ERF] Se añaden tablas DISPOSITIVOS_FIRMA, TIPOS_ESTADOS_DISPOSITIVOS, TECNICOS_FIRMA para firma biometrica
    -[2022/10/07_ERF] se añaden los campos EXP_CODIGO_INTERNO en las entidades expediente
    -[2022/10/07_ERF] se añaden los campos INS_FIRMADO, INS_DOCGUI_FIRMA, INS_DEVICENAME_FIRMA, INS_EMAIL_REPRESENTANTE en la entidad inspección
BLInspect 1.5.0  (07/07/2022)
-------------------
    -[2022/07/07_ERF]  Se añaden atributos genéricos ENT_TEXTO_1, ENT_TEXTO_2, ENT_TEXTO_3, ENT_NUM_1, ENT_NUM_2, ENT_NUM_3, ENT_FECHA_1, ENT_FECHA_2, ENT_FECHA_3
    -[2022/07/07_ERF]  Se añaden atributo exp_refcat_otras

BLInspect 1.4.0  (12/11/2021)
-------------------
    -[2021/04/06_ERF]  Se añade esquema surveys para configuración de Urban Surveys

BLInspect 1.3.0  (06/04/2021)
-------------------
    -[2021/04/06_ERF]  BUG 58453 / AESAD 1024920. Añadir atributo adicional obras EXP_OBR_FECHA_NOTI

BLInspect 1.2.0  (09/01/2020)
-------------------
    -[2020/09/01_ERF]  BUG 56378. Cuando se elminen entidades NO expediente poner la vigencia a -1.
    -[2020/03/30_ERF]  Añadidas vistas para Feature Analyzer.
    -[2020/03/30_ERF]  Añadido modelo 5 entidades y modelo inspección único. Task 54752.
    -[2020/01/30_ERF]  Tabla genérica de areas_trabajo.
    -[2020/01/20_ERF]  Añadido multidioma en los literales de BD.
    -[2020/01/13_ERF]  Añadidos 35 campos para tipología de infracciones. Bug 53616. 
    -[2020/01/13_ERF]  Añadido cálculo del histórico de las entidades. Bug 53619. 
	

BLInspect 1.1.00  (23/10/2019)
-------------------
    -[2019/10/08_ERF]  Añadidos cambios en el modelo para crear vista feature analyzer y reorganización de los directorios.


BLInspect 1.0.00  (12/08/2019)
-------------------
    -[2019/07/10_ERF]  Primera versión.
	