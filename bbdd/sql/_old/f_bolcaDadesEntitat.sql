CREATE OR REPLACE FUNCTION public.bolcaDadesEntitat(
    taula_entitat text
)
RETURNS VOID AS $$
DECLARE

    taula_temp text;
    esquema_entitat text;

    q_epsg_entitat text;
    epsg_entitat record;

    q_desc_taula text;
    r_desc_taula record;

    column_name text;
    data_type text;    
    primer_atribut boolean;

    str_fields_insert text;
    str_fields_select text;
    str_fields_update text;
    
    q_inserta_nous text;
    q_inserta_tots text;
    q_actualitza text;

    q_existeix text;
    existeix boolean;
    files_afectades integer;

BEGIN

    taula_temp := taula_entitat || '_temp';
    esquema_entitat := 'public';

    --1. Obtenir EPSG taula entitat.
    q_epsg_entitat := 'select find_srid(''' || esquema_entitat|| ''', '''|| taula_entitat || ''', ''aa_geom'')';
    EXECUTE q_epsg_entitat INTO epsg_entitat;

    --2. Descripció de la taula
    q_desc_taula := 'SELECT 
                        lower(column_name) as columname,
                        lower(data_type) as datatype                        
                    FROM information_schema.columns
                    WHERE table_schema = ''' || esquema_entitat || '''
                    AND lower(table_name) = lower(''' || taula_entitat || ''') 
                    AND column_name <> ''ent_uid''
                    AND column_name <> ''ent_fecha_revision''
                    AND column_name <> ''ent_ultima_insp''
                    ORDER BY ordinal_position asc';
    
    str_fields_insert := 'ent_uid';
    str_fields_select := 'uuid_generate_v1()';
    primer_atribut := true;

    --3. Construcció dinàmica de les queries
    FOR r_desc_taula IN EXECUTE q_desc_taula
    LOOP
        column_name := r_desc_taula.columname;
        data_type := r_desc_taula.datatype;        
        
        IF primer_atribut IS TRUE THEN
            str_fields_update := column_name || ' = tmp.' || column_name;
            str_fields_insert := str_fields_insert || ', ' || column_name;
            str_fields_select := str_fields_select || ', ' || column_name;   

            primer_atribut := false;
        
        ELSE         
                                        
            IF column_name = 'aa_geom' THEN
                str_fields_insert := str_fields_insert || ', ' || column_name;
                str_fields_select := str_fields_select || ', ST_Transform(aa_geom, '|| epsg_entitat ||')';
            
            ELSIF data_type = 'bytea' THEN
                str_fields_insert := str_fields_insert || ', ' || column_name;
                str_fields_select := str_fields_select || ', case when decode('|| column_name ||', ''base64'') = '''' THEN NULL ELSE decode('|| column_name ||', ''base64'') END ';
                str_fields_update := str_fields_update || ', ' || column_name || ' = (case when decode(tmp.'|| column_name ||', ''base64'') = '''' THEN NULL ELSE decode(tmp.'|| column_name ||', ''base64'') END)';
            
            ELSE
                str_fields_insert := str_fields_insert || ', ' || column_name;
                str_fields_select := str_fields_select || ', ' || column_name;   
                str_fields_update := str_fields_update || ', ' || column_name || ' = tmp.' || column_name;             
            
            END IF;
        
        END IF;        

    END LOOP;

    q_inserta_tots := 'INSERT INTO '|| taula_entitat ||' (' || str_fields_insert || ') 
                       SELECT ' || str_fields_select || ' FROM '|| taula_temp ||';';
    
    q_actualitza := 'UPDATE '|| taula_entitat ||' e 
                    SET '|| str_fields_update || ' 
                    FROM '|| taula_temp ||' AS tmp 
                    WHERE e.id_elemento = tmp.id_elemento
                    AND (tmp.exp_estado_expediente <> e.exp_estado_expediente OR tmp.exp_estado_expediente <> 2);';
    
    q_inserta_nous := 'INSERT INTO '|| taula_entitat ||' (' || str_fields_insert || ')  
                    SELECT ' || str_fields_select || 'FROM '|| taula_temp ||' l 
                    WHERE NOT EXISTS (SELECT 
                                        FROM '|| taula_entitat ||'
                                        WHERE id_elemento = l.id_elemento
                                        ) 
                    AND l.exp_estado_expediente <> 2;';
    

    --5. Mirem si la taula entitat té dades per saber quines queries executar
    q_existeix := 'SELECT EXISTS (SELECT 1 FROM public.'||taula_entitat ||');';
	EXECUTE q_existeix INTO existeix;
    RAISE NOTICE 'La taula % té dades: %', taula_entitat, existeix;

    IF existeix IS FALSE THEN 

        --5.a) No té dades: fem el bolcat directe de totes les dades
        -- RAISE NOTICE 'q_inserta_tots: %', q_inserta_tots;
        EXECUTE q_inserta_tots;

        GET DIAGNOSTICS files_afectades = ROW_COUNT;
        RAISE NOTICE 'Insertats % files de la taula temporal', files_afectades;

    ELSE
        --5.b) Si té dades
        --actualizem: actualitzem els registres de la taula entitat que tinguin estat <> 2 a partir de la temporal        
        -- RAISE NOTICE 'q_actualitza: %', q_actualitza;
        EXECUTE q_actualitza;

        GET DIAGNOSTICS files_afectades = ROW_COUNT;
		RAISE NOTICE 'Actualitzades % files de la taula temporal', files_afectades;

        --)insertem nous: Insertem a la taula ENTITAT els registres de la temporal ESTAT<>TANCAT que NO estàn a la taula ENTITAT
        -- RAISE NOTICE 'q_inserta_nous: %', q_inserta_nous;
        EXECUTE q_inserta_nous;

        GET DIAGNOSTICS files_afectades = ROW_COUNT;
        RAISE NOTICE 'Insertats % files noves de la taula temporal', files_afectades;

    END IF;


EXCEPTION WHEN others THEN   
    RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;

END
$$
LANGUAGE plpgsql;



--select bolcaDadesEntitat('mercat');

