/*=============================================================================================*/
/*=============================================================================================*/
/* Entitat: MERCAT                                                                             */
/*=============================================================================================*/
/*=============================================================================================*/

DO $$ 
DECLARE
    
    /* Sustituir valor EPSG cuando corresponda: 25831, 25830... */
    epsg NUMERIC := 25831;

    sql_create_area text;
    sql_create_entity text;    
    sql_create_entity_insp text;

BEGIN 


/*==============================================================*/
/* Table: MERCAT                                                */
/*==============================================================*/
sql_create_entity := 'CREATE table MERCAT (
                        ENT_UID                 CHARACTER(36)                   not null,
                        ENT_FECHA_REVISION      TIMESTAMP                       null,
                        ENT_ULTIMA_INSP         TIMESTAMP                       null,
                        AA_IDA                  NUMERIC                         not null,
                        AA_GEOM                 Geometry(Polygon,'||epsg||')    null,
                        AA_DES                  CHARACTER VARYING(100)          null,
                        ID_ELEMENTO             CHARACTER VARYING(40)           not null,
                        TIPO_ELEM               CHARACTER VARYING(40)           null,
                        EXP_DESCRIPCION         CHARACTER VARYING(100)          null,
                        EXP_DIRECCION           CHARACTER VARYING(255)          null,
                        EXP_DIRECCION_NOTIFICACION CHARACTER VARYING(255)       null,
                        EXP_ESTADO_EXPEDIENTE   NUMERIC                         not null,
                        EXP_ESTADO_EXPEDIENTE_DESC CHARACTER VARYING(100)       null,
                        EXP_FECHA_ALTA          TIMESTAMP                       null,
                        EXP_FECHA_BAJA          TIMESTAMP                       null,
                        EXP_INT_EMAIL           CHARACTER VARYING(255)          null,
                        EXP_INT_TELEF           NUMERIC                         null,
                        EXP_NOMBRE_COMPLETO     CHARACTER VARYING(100)          null,
                        EXP_NUMERO              CHARACTER VARYING(25)           null,
                        EXP_NUMERO_DOCUMENTO    CHARACTER VARYING(10)           null,
                        EXP_OBSERVACIONES       CHARACTER VARYING(1000)         null,
                        EXP_REFCAT              CHARACTER VARYING(20)           null,
                        EXP_TIPO_DOCUMENTO      CHARACTER VARYING(50)           null,
                        EXP_TIPO_PERSONA        CHARACTER VARYING(100)          null,
                        EXP_MER_BAJA            BOOL                            null,
                        EXP_MER_BAJA_OBS        CHARACTER VARYING(500)          null,
                        EXP_MER_PAGO            BOOL                            null,
                        EXP_MER_TIPO            NUMERIC                         not null,
                        EXP_MER_TIPO_DESC       CHARACTER VARYING(100)          null,
                        EXP_MER_IMG_AUTORIZADO  BYTEA                           null,
                        constraint PK_MERCAT primary key (ENT_UID)
                     );';

EXECUTE sql_create_entity;

/*==============================================================*/
/* Table: MERCAT_TEMP                                           */
/*==============================================================*/
create table MERCAT_TEMP (
   OGC_FID              SERIAL                          not null,
   AA_IDA               NUMERIC                         not null,
   AA_GEOM              Geometry(Polygon, 4326)         null,
   AA_DES               CHARACTER VARYING(100)          null,   
   ID_ELEMENTO          CHARACTER VARYING(40)           not null,   
   TIPO_ELEM            CHARACTER VARYING(40)           null,
   EXP_DESCRIPCION      CHARACTER VARYING(100)          null,
   EXP_DIRECCION        CHARACTER VARYING(255)          null,
   EXP_DIRECCION_NOTIFICACION CHARACTER VARYING(255)    null,
   EXP_ESTADO_EXPEDIENTE NUMERIC                        not null,
   EXP_ESTADO_EXPEDIENTE_DESC CHARACTER VARYING(100)    null,
   EXP_FECHA_ALTA       TIMESTAMP                       null,
   EXP_FECHA_BAJA       TIMESTAMP                       null,
   EXP_INT_EMAIL        CHARACTER VARYING(255)          null,
   EXP_INT_TELEF        NUMERIC                         null,
   EXP_NOMBRE_COMPLETO  CHARACTER VARYING(100)          null,
   EXP_NUMERO           CHARACTER VARYING(25)           null,
   EXP_NUMERO_DOCUMENTO CHARACTER VARYING(10)           null,
   EXP_OBSERVACIONES    CHARACTER VARYING(1000)         null,
   EXP_REFCAT           CHARACTER VARYING(20)           null,
   EXP_TIPO_DOCUMENTO   CHARACTER VARYING(50)           null,
   EXP_TIPO_PERSONA     CHARACTER VARYING(100)          null,
   EXP_MER_BAJA         BOOL                            null,
   EXP_MER_BAJA_OBS     CHARACTER VARYING(500)          null,
   EXP_MER_PAGO         bool                            null,
   EXP_MER_TIPO         NUMERIC                         not null,
   EXP_MER_TIPO_DESC    CHARACTER VARYING(100)          null,
   EXP_MER_IMG_AUTORIZADO CHARACTER VARYING             null,
   constraint PK_MERCAT_TEMP primary key (OGC_FID)
);


/*==============================================================*/
/* Table: INSPECCIO_MERCAT                                      */
/*==============================================================*/
sql_create_entity_insp := 'CREATE table INSPECCIO_MERCAT (
                            INS_UID              CHARACTER(36)              not null,
                            ENT_UID              CHARACTER(36)              not null,
                            INS_FECHA_REVISION   TIMESTAMP                  null,
                            INS_FECHA_INSPECCION TIMESTAMP                  null,
                            INS_TECNICO          CHARACTER VARYING(50)      null,
                            INS_UBICACION        GEOMETRY(POINT,'||epsg||') null,
                            INS_UBICACION_PREC   NUMERIC                    null,
                            INS_REPRESENTANTE    CHARACTER VARYING(50)      null,
                            INS_TIPO_DOCUMENTO   CHARACTER VARYING(50)      null,
                            INS_NUM_DOCUMENTO    CHARACTER VARYING(20)      null,
                            INS_ASISTENCIA       BOOLEAN                    null,
                            INS_MOD_DATOS_PERS   CHARACTER VARYING(1000)    null,
                            INS_OBSERVACIONES    CHARACTER VARYING(1000)    null,
                            INS_ENVIADO_GEWEB    BOOLEAN                    null,
                            INS_FINALIZADA       BOOLEAN                    null,
                            INS_IMG_1            BYTEA                      null,
                            INS_IMG_1_SIZE       NUMERIC                    null,
                            INS_IMG_2            BYTEA                      null,
                            INS_IMG_2_SIZE       NUMERIC                    null,
                            INS_IMG_3            BYTEA                      null,
                            INS_IMG_3_SIZE       NUMERIC                    null,
                            INS_INFRACCION       BOOLEAN                    null,
                            INS_INF_001          BOOLEAN                    null,
                            INS_INF_002          BOOLEAN                    null,
                            INS_INF_003          BOOLEAN                    null,
                            INS_INF_004          BOOLEAN                    null,
                            INS_INF_005          BOOLEAN                    null,
                            INS_INF_006          BOOLEAN                    null,
                            INS_INF_007          BOOLEAN                    null,
                            INS_INF_008          BOOLEAN                    null,
                            INS_INF_009          BOOLEAN                    null,
                            INS_INF_010          BOOLEAN                    null,
                            INS_INF_011          BOOLEAN                    null,
                            INS_INF_012          BOOLEAN                    null,
                            INS_INF_013          BOOLEAN                    null,
                            INS_INF_014          BOOLEAN                    null,
                            constraint PK_INSPECCIO_MERCAT primary key (INS_UID)
                            );';

EXECUTE sql_create_entity_insp;

alter table INSPECCIO_MERCAT
   add constraint FK_INSPECCI_REFERENCE_MERCAT foreign key (ENT_UID)
      references MERCAT (ENT_UID)
      on delete restrict on update restrict;


/*==============================================================*/
/* Table: MERCAT_AREA_TREBALL                                   */
/*==============================================================*/
sql_create_area := 'CREATE table MERCAT_AREA_TREBALL (
                        AREA_ID              NUMERIC                           not null,
                        AREA_DESCRIPCION     CHARACTER VARYING(100)            null,
                        AREA_GEOM            Geometry(Polygon,'||epsg||')      null,
                        constraint PK_MERCAT_AREA_TREBALL primary key (AREA_ID)
                    );';
                    
EXECUTE sql_create_area;

--Siempre por defecto insertamos una "Sin asignar" con geometría null. Por si se dejan en Sit-Expedientes de llenar el campo.
insert into MERCAT_AREA_TREBALL (area_id, area_descripcion, area_geom) values (99, 'Sin asignar', null);


alter table MERCAT
   add constraint FK_MERCAT_REFERENCE_MERCAT_AREA foreign key (EXP_MER_TIPO)
      references MERCAT_AREA_TREBALL (AREA_ID)
      on delete restrict on update restrict;


-->EJEMPLO (HACER CON QGIS)-------------------------------------------------
-- INSERT INTO public.mercat_area_treball(area_id, area_descripcion, area_geom) VALUES (1, 'Mercat de Pollença', '0106000020E7640000010000000103000000010000000500000059555A928DEBC6C0B57179BC5EE75041B7F794F70BEAC6C0541E44285DE75041685C6DE93CE2C6C0704964405FE7504150F4A8C8FEE3C6C0AD45428F60E7504159555A928DEBC6C0B57179BC5EE75041');
-- INSERT INTO public.mercat_area_treball(area_id, area_descripcion, area_geom) VALUES (2, 'Mercat del Port de Pollença', '0106000020E76400000100000001030000000100000005000000561217888CFDC6C0E49AA54D5AE750419AE96E49ADFCC6C0453B906259E75041DE6A7A5CFAF5C6C020D436155BE7504161461B04B1F6C6C0051FAE035CE75041561217888CFDC6C0E49AA54D5AE75041');
-- INSERT INTO public.mercat_area_treball(area_id, area_descripcion, area_geom) VALUES (3, 'Mercat nocturn', '0106000020E7640000010000000103000000010000000600000007BBCD8D74F6C6C0A80F12BD5BE75041B713CE42E2F5C6C05CD3C81F5BE75041872F3B04A7F1C6C0A24E5C355CE75041F3FAD3A63FF2C6C065DAAEF45CE750413DA83C3B8CF6C6C0E99C3EDA5BE7504107BBCD8D74F6C6C0A80F12BD5BE75041');
-- INSERT INTO public.mercat_area_treball(area_id, area_descripcion, area_geom) VALUES (4, 'Fira', '0106000020E76400000100000001030000000100000005000000E01BC6FF0603C7C008E6CB0259E7504104C5DFE63102C7C097522B2058E7504138ED1014C7FCC6C062DE7F6C59E750418E40699289FDC6C089F823395AE75041E01BC6FF0603C7C008E6CB0259E75041');
----------------------------------------------------------------------------



END $$;