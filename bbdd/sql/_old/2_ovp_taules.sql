/*=============================================================================================*/
/*=============================================================================================*/
/* Entitat: OVP_TAULES                                                                         */
/*=============================================================================================*/
/*=============================================================================================*/

/*==============================================================*/
/* Table: OVP_TAULES                                            */
/*==============================================================*/

/*==============================================================*/
/* Table: OVP_TAULES_TEMP                                       */
/*==============================================================*/

/*==============================================================*/
/* Table: OVP_TAULES_AREA_TREBALL                               */
/*==============================================================*/

/*==============================================================*/
/* Table: INSPECCIO_OVP_TAULES                                  */
/*==============================================================*/

/*==============================================================*/
/* Inserts: OVP_TAULES_AREA_TREBALL                             */
/*==============================================================*/

/*==============================================================*/
/* Inserts: INSPECCIO_CATALEG                                   */
/*==============================================================*/
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_001', 'No té llicència', '', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_002', 'No acredita el pagament de les taxes', '', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_003', 'Situar mercaderies de qualsevol tipus directament damunt el paviment', 'LLEU', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_004', 'No disposar en lloc visible la llicència d''ocupació', 'LLEU', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_005', 'Instal·lar tendals, mampares o tanques que pertorbin la circulació de vianants', 'LLEU', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_006', 'L''incompliment de les condicions de la llicència d''ocupació', 'GREU', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_007', 'Exposar o vendre productes alimentaris que no es conservin aïllats de l''acció directa del medi ambient', 'GREU', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_008', 'El dipòsit a la via pública d''objectes, escombreries, materials o vehicles que obstaculitzin la circulació dels vianants o de vehicles', 'GREU', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_009', 'Ocupar la via pública sense llicència quan aquesta sigui preceptiva', 'MOLTGREU', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_010', 'Excedir-se en la superfície d''ocupació o en el nombre d''elements autoritzats ', 'MOLTGREU', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_011', 'Excedir-se de l''horari autoritzat per a l''ocupació', 'MOLTGREU', 'OVP_TAULES');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_012', 'Incomplir els Decrets de Batlia que, en ús de les seves atribucions, adoptin mesures d''urgència o d''interès general en matèria d''ocupació de la via pública', 'MOLTGREU', 'OVP_TAULES');


/*==============================================================*/
/* Funció: BOLCA_DADES                                          */
/*==============================================================*/