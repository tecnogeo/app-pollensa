--1. SUSTITUIR 
    --NOMBRE BBDD: DATA_CT
    --USUARIO: USERCT
    --PASSWORD: USERCT$123


--2. EJECUTAR

    --Conexión a la BBDD postgres
    ------------------------------------
    -- 1. Create database DATA
    CREATE DATABASE "DATA_CT";
    -- 2. Crear usuario
    CREATE USER USERCT WITH PASSWORD 'USERCT$123';

    -- 3. Asignar permisos
    GRANT ALL PRIVILEGES ON DATABASE "DATA_CT" to USERCT;
    GRANT CONNECT ON DATABASE "DATA_CT" TO USERCT;

    --Conexión a la BBDD DATA creada
    ------------------------------------
    -- 4. Asignar permisos
    GRANT USAGE ON SCHEMA public TO USERCT;
    GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO USERCT;
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO USERCT;
    GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO USERCT;
    alter default privileges in schema public grant all on tables to USERCT;
    alter default privileges in schema public grant all on sequences to USERCT;
    alter default privileges in schema public grant all on FUNCTIONS to USERCT;
    
    -- 5. Creación esquema data y permisos
    CREATE schema data;
    GRANT USAGE ON SCHEMA data TO USERCT;
    GRANT CREATE ON SCHEMA data TO USERCT;
    GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA data TO USERCT;
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA data TO USERCT;
    GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA data TO USERCT;
    ALTER DEFAULT PRIVILEGES IN SCHEMA data GRANT ALL ON TABLES TO  USERCT;
    ALTER DEFAULT PRIVILEGES IN SCHEMA data GRANT ALL ON SEQUENCES TO  USERCT;
    ALTER DEFAULT PRIVILEGES IN SCHEMA data GRANT ALL ON FUNCTIONS TO  USERCT;


     -- 6. Creación esquema surveys y permisos
    CREATE schema surveys;
    GRANT USAGE ON SCHEMA surveys TO USERCT;
    GRANT CREATE ON SCHEMA surveys TO USERCT;
    GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA surveys TO USERCT;
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA surveys TO USERCT;
    GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA surveys TO USERCT;
    ALTER DEFAULT PRIVILEGES IN SCHEMA surveys GRANT ALL ON TABLES TO USERCT;
    ALTER DEFAULT PRIVILEGES IN SCHEMA surveys GRANT ALL ON SEQUENCES TO USERCT;
    ALTER DEFAULT PRIVILEGES IN SCHEMA surveys GRANT ALL ON FUNCTIONS TO USERCT;



/*****************************************************************************************************************/


    --Conexión a la BBDD postgres
    ------------------------------------
    -- 1. Create database SMAPPE
    CREATE DATABASE "SMAPPE_DATA_CT";
    -- 2. Create usuario database SMAPPE
    CREATE USER smappe_USERCT WITH SUPERUSER CREATEROLE PASSWORD 'smappeUSERCT$123';

    -- 3. Asignar permisos
    GRANT ALL PRIVILEGES ON DATABASE "SMAPPE_DATA_CT" to smappe_USERCT;
    GRANT CONNECT ON DATABASE "SMAPPE_DATA_CT" TO smappe_USERCT;


    ---Conexión a la BBDD SMAPPE creada
    ------------------------------------
    --4. Permisos
    GRANT USAGE ON SCHEMA public TO smappe_USERCT;
    GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO smappe_USERCT;
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO smappe_USERCT;
    GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO smappe_USERCT;


/*****************************************************************************************************************/
    -- Ejecutar 
    -- 3_init.sql
    
    -- 4_conf_cliente.sql
    insert into config_cliente (EPSG, IDIOMA, FORMATO_NUMEXP) values (25831, 'ca-ES', 'NumeroGeneral/Anyo');



-- mas info
-- https://tableplus.com/blog/2018/04/postgresql-how-to-grant-access-to-users.html
-- https://aws.amazon.com/es/blogs/database/managing-postgresql-users-and-roles/