DO $$ 
DECLARE
    
    /* Sustituir valor EPSG cuando corresponda: 25831, 25830... */
    epsg NUMERIC := 25831;

    qsql text;
    sql_create_entity text;    
    sql_create_entity_insp text;

BEGIN 


/*==============================================================*/
/* Table: INSPECCIO_CATALEG_TIPUS                               */
/*==============================================================*/
create table INSPECCIO_CATALEG_TIPUS (
   ict_id            SERIAL                      not null,
   ict_desc          CHARACTER VARYING(100)          null,   
   constraint PK_INF_TIPUS primary key (ict_id)
);

insert into inspeccio_cataleg_tipus (ict_id, ict_desc) values (0, 'Sense infracció');
insert into inspeccio_cataleg_tipus (ict_desc) values ('Lleu');
insert into inspeccio_cataleg_tipus (ict_desc) values ('Molt greu');
insert into inspeccio_cataleg_tipus (ict_desc) values ('Greu');


/*==============================================================*/
/*Table: INSPECCIO_CATALEG                                      */
/*add field inf_tipo_falta_id and FK to INSPECCIO_CATALEG_TIPUS */ 
/*==============================================================*/
ALTER TABLE INSPECCIO_CATALEG ADD COLUMN INF_TIPO_FALTA_ID INTEGER;

UPDATE INSPECCIO_CATALEG SET INF_TIPO_FALTA_ID = 1 WHERE inf_tipo_falta = 'LLEU';
UPDATE INSPECCIO_CATALEG SET INF_TIPO_FALTA_ID = 2 WHERE inf_tipo_falta = 'GREU';
UPDATE INSPECCIO_CATALEG SET INF_TIPO_FALTA_ID = 3 WHERE inf_tipo_falta = 'MOLTGREU';

ALTER TABLE INSPECCIO_CATALEG 
ADD CONSTRAINT FK_INSPECCIO_CATALEG_TIPUS FOREIGN KEY (INF_TIPO_FALTA_ID) REFERENCES INSPECCIO_CATALEG_TIPUS (ict_id);


/*==============================================================*/
/* Table: MERCAT                                                */
/* add field aa_geom_centroid                                   */
/*==============================================================*/
qsql := 'ALTER TABLE MERCAT ADD COLUMN AA_GEOM_CENTROID Geometry(Point,'||epsg||');';
EXECUTE qsql;


/*==============================================================*/
/* Funció trigger per calcular centroide                        */
/*==============================================================*/
CREATE OR REPLACE FUNCTION calc_centroide() RETURNS TRIGGER AS $BODY$
   BEGIN
      --calculate centroid from aa_geom	
      NEW.aa_geom_centroid = ST_CENTROID(NEW.aa_geom);
      RETURN NEW;
   END;
$BODY$ LANGUAGE plpgsql;


CREATE TRIGGER calc_centroide 
	BEFORE INSERT OR UPDATE ON mercat
    FOR EACH ROW EXECUTE PROCEDURE calc_centroide();

UPDATE mercat set aa_geom_centroid = ST_CENTROID(aa_geom);


/*==============================================================*/
/* Table: VERSIONS BD                                           */
/*==============================================================*/
UPDATE SIS_VERSION SET VERSION = '1.1.00', FECHA = current_timestamp WHERE MODULO = 'BL.INSPECT';


EXCEPTION WHEN others THEN   
    RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;

END
$$
LANGUAGE plpgsql;
