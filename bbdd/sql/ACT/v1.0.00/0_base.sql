/*==============================================================*/
/* Extensions                                                   */
/*==============================================================*/
CREATE EXTENSION postgis;
CREATE EXTENSION "uuid-ossp";



/*==============================================================*/
/* Table: INSPECCIO_CATALEG                                     */
/*==============================================================*/
create table INSPECCIO_CATALEG (
   INF_ID               SERIAL                      not null,
   INF_CODIGO           CHAR(11)                    null,
   INF_DESCRIPCION      CHARACTER VARYING(1000)     null,
   INF_TIPO_FALTA       CHARACTER VARYING(1000)     null,
   INF_ENTIDAD          CHARACTER VARYING(50)       null,
   constraint PK_INSPECCIO_CATALEG primary key (INF_ID)
);

insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_001', 'Incomplir l''horari autoritzat.', 'LLEU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_002', 'Començar a instal·lar o muntar els llocs abans de les 07:30 hores.', 'LLEU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_003', 'L''ús de qualsevol dispositiu sonor amb fins de propaganda, reclam, avís o distracció.', 'LLEU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_004', 'Col·locar la mercaderia en els espais destinats a passadissos i espais entre llocs.', 'LLEU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_005', 'Aparcar el vehicle del titular durant l''horari de celebració del mercadet a l''espai reservat per a la ubicació del lloc.', 'LLEU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_006', 'No exhibir l''autorització municipal pertinent i no presentar-la a requeriment de l''autoritat.', 'LLEU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_007', 'No procedir a netejar el lloc una vegada finalitzada la jornada.', 'LLEU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_008', 'Qualsevol altra acció o omissió que constitueixi incompliment dels preceptes d''aquesta ordenança i que no estigui tipificada com infracció greu o molt greu.', 'LLEU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_009', 'Incomplir qualsevol de les condicions imposades a l''autorització per a l''exercici de la venda ambulant', 'GREU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_010', 'Exercir l''activitat persones diferents a les autoritzades.', 'GREU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_011', 'No estar al corrent del pagament dels tributs corresponents per a la instal·lació del lloc en el mercadet.', 'GREU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_012', 'Instal·lar llocs o exercir l''activitat sense autorització municipal.', 'GREU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_013', 'El desacatament, resistència, coacció o amenaça a l''autoritat municipal, funcionaris i agents d''aquesta, en compliment de la seva missió.', 'GREU', 'MERCAT');
insert into inspeccio_cataleg (inf_codigo, inf_descripcion, inf_tipo_falta, inf_entidad) values ('INS_INF_014', 'La cessió, traspàs, lloguer de lloc o qualsevol altra forma d''exercici de l''activitat que no sigui la realitzada pel titular de l''autorització municipal.', 'MOLTGREU', 'MERCAT');



/*==============================================================*/
/* Table: VERSIONS BD                                           */
/*==============================================================*/
create table SIS_VERSION (
   ID              SERIAL                       not null,
   MODULO          CHARACTER VARYING(60)        null,
   VERSION         CHARACTER VARYING(10)        null,
   FECHA           TIMESTAMP                    null,  
   constraint PK_SIS_VERSION primary key (ID)
);

insert into SIS_VERSION (MODULO, VERSION, FECHA) values ('BL.INSPECT', '1.0.00', current_timestamp);