-- donar permisos al usuari data sobre la bd smappe_(executar a la bd postgres amb usuari postgres)
GRANT ALL PRIVILEGES ON DATABASE "SMAPPE_DATA_BLINSPECT_CI" to blinspect_ci;
GRANT CONNECT ON DATABASE "SMAPPE_DATA_BLINSPECT_CI" to blinspect_ci;

-- (executar bd smappe amb usuario postgres)
GRANT USAGE ON SCHEMA public TO blinspect_ci;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO blinspect_ci;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO blinspect_ci;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO blinspect_ci;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON TABLES TO blinspect_ci;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON SEQUENCES TO blinspect_ci;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON FUNCTIONS TO blinspect_ci;

-- afegir extensió postgres_fdw
CREATE EXTENSION "postgres_fdw";

-- crear foreign server
CREATE SERVER smappe_blinspect_ci
FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (host 'sitdesenvol.absis.es', dbname 'SMAPPE_DATA_BLINSPECT_CI', port '5432');

-- crear user mapping 
CREATE USER MAPPING FOR blinspect_ci  
SERVER smappe_blinspect_ci  
OPTIONS (user 'blinspect_ci', password 'BLINSPECT_CI$123');

CREATE USER MAPPING FOR postgres  
SERVER smappe_blinspect_ci  
OPTIONS (user 'postgres', password 'Hexatecno1');

-- crear foreign table users
CREATE FOREIGN TABLE mapp_user (
	id character varying(36),
    name character varying(128),
    fullname character varying(64),
    email character varying(128)
) SERVER smappe_blinspect_ci OPTIONS (schema_name 'public', table_name 'mapp_user');



-- función para actualizar de mapp_user a tecnicos_firma
CREATE OR REPLACE FUNCTION public.f_actualizaUsuariosSmappe()
RETURNS BOOLEAN AS $$
DECLARE
BEGIN

	-- insertar nuevos
	INSERT INTO tecnicos_firma (usuario, nombre, email)
	SELECT name, fullname, email from mapp_user s
	WHERE NOT EXISTS (select * from tecnicos_firma WHERE s.name = usuario)

	-- actualizar creados
	UPDATE tecnicos_firma te SET
	usuario = s.name,
	nombre = s.fullname,
	email = s.email
	FROM mapp_user s
	WHERE s.name = te.usuario


RETURN TRUE;
EXCEPTION WHEN others THEN   
    RAISE WARNING '% - %', SQLERRM, SQLSTATE;
    RETURN FALSE;
END
$$
LANGUAGE plpgsql;


-- Añadir DNI y NUMERO DOCUMENTO por cada usuario
UPDATE tecnicos_firma set tipo_documento = 'DNI', numero_documento = '12345678G' where usuario = 'adminTIG'
