
--ACTUALIZACIÓN 1.4.0
DO $$ 
DECLARE  

    user_db text;

BEGIN

    -- obtención usuario creado para la bbdd
    SELECT distinct(grantee) 
	INTO user_db 
	FROM information_schema.role_table_grants 
	WHERE table_name='sis_globales' 
	AND grantee <> 'postgres';

    -- Creación esquema surveys y asignación de permisos
    CREATE schema surveys;
    EXECUTE 'GRANT USAGE ON SCHEMA surveys TO ' || user_db || ';';
    EXECUTE 'GRANT CREATE ON SCHEMA surveys TO ' || user_db || ';';
    EXECUTE 'GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA surveys TO ' || user_db || ';';
    EXECUTE 'GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA surveys TO ' || user_db || ';';
    EXECUTE 'GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA surveys TO ' || user_db || ';';
    EXECUTE 'ALTER DEFAULT PRIVILEGES IN SCHEMA surveys GRANT ALL ON TABLES TO ' || user_db || ';';
    EXECUTE 'ALTER DEFAULT PRIVILEGES IN SCHEMA surveys GRANT ALL ON SEQUENCES TO ' || user_db || ';';
    EXECUTE 'ALTER DEFAULT PRIVILEGES IN SCHEMA surveys GRANT ALL ON FUNCTIONS TO ' || user_db || ';';


    -- Actualizar versión
    UPDATE SIS_GLOBALES SET VARIABLE_VALOR = '1.4.0', VARIABLE_NOMBRE = 'VERSION.BLINSPECT.BLSURVEYS' WHERE  VARIABLE_NOMBRE = 'VERSION.BLINSPECT';

    
    -- crear tabla inventario_entidades
    create table surveys.inventario_entidades (
      ID                  SERIAL                      not null,
      TIPO_TABLA          CHARACTER VARYING(20)       not null,
      NOMBRE_TABLA        CHARACTER VARYING(63)       not null,
      ALIAS_TABLA         CHARACTER VARYING(100)      not null,      
      constraint pk_inventario_entidades primary key (ID)
    );



EXCEPTION WHEN others THEN   
    RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;

END
$$
LANGUAGE plpgsql;