--función creación campo ins_identificador
CREATE OR REPLACE FUNCTION f_ins_identificador_year_seq()
	RETURNS text AS
$$
DECLARE
	
	last_row_year text;
	identificador text;
	current_year text = to_char(now(), 'YYYY');
BEGIN


LOOP
BEGIN
	RETURN current_year ||'-'|| to_char(nextval('ins_identificador_year_'|| current_year ||'_seq'), 'FM0000000');

EXCEPTION WHEN undefined_table THEN   -- error code 42P01
	EXECUTE 'CREATE SEQUENCE ins_identificador_year_' || current_year || '_seq MINVALUE 1 START 1';
END;
END LOOP;

EXCEPTION 
	WHEN others THEN        
		RAISE WARNING '% - %', SQLERRM, SQLSTATE;
		RETURN 'WARNING '|| SQLERRM || ' - ' || SQLSTATE;
END;
$$ LANGUAGE plpgsql;
	

--ACTUALIZACIÓN
DO $$ 
DECLARE  
	user_mappent text;
	q text;
BEGIN 

	--Borrar las vistas
	DROP VIEW V_FA_INSPECCIONES;
	DROP VIEW V_INFRACCIONES_MERCADOS;
	DROP VIEW V_INFRACCIONES_TERRAZAS;
	DROP VIEW V_INFRACCIONES_OBRAS;
	DROP VIEW V_INFRACCIONES_OVP;
	DROP VIEW V_INFRACCIONES_ACTIVIDADES;
	DROP VIEW V_FA_ENTIDADES;
	DROP VIEW V_FA_INSPECCIONES_GEOM;
	
	--Asignar privilegios al usuario mappent para futuras tablas y secuencias
	SELECT distinct(grantee) 
	INTO user_mappent 
	FROM information_schema.role_table_grants 
	WHERE table_name='sis_globales' 
	AND grantee <> 'postgres' ;
	
	q := 'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON tables TO '|| user_mappent|| ';';
	EXECUTE q;
    q := 'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON sequences TO '|| user_mappent || ';';
	EXECUTE q;

    -- Actualizar versión
    UPDATE SIS_GLOBALES SET VARIABLE_VALOR = '1.3.0' WHERE  VARIABLE_NOMBRE = 'VERSION.BLINSPECT';

    -- Insertar nuevo tipo de histórico
    insert into tipos_historico (tipo_hist_id, tipo_hist_descripcion) values (4, 'Histórico respecto el estado del expediente');

    -- Añadir nuevo campo entidad OBRA
    ALTER TABLE OBRAS ADD COLUMN EXP_OBR_FECHA_NOTI TIMESTAMP;
    ALTER TABLE OBRAS_TEMP ADD COLUMN EXP_OBR_FECHA_NOTI TIMESTAMP;

	--Actualizar columna_localizacion tipos_entidad_config a exp_direccion
	update tipos_entidad set columna_localizacion = 'exp_direccion' where tipo_entidad_id = 3 or tipo_entidad_id = 4; 
	update tipos_entidad set columna_localizacion = 'exp_direccion,terraza_ubicacion' where tipo_entidad_id = 2;
    
	--Actualizar función check_configuracion añadiendo el nuevo tipo de historico
	ALTER TABLE TIPOS_ENTIDAD_CONFIG DROP CONSTRAINT check_configuracion;
	
	ALTER TABLE TIPOS_ENTIDAD_CONFIG
   	ADD CONSTRAINT check_configuracion CHECK( 
      (hist_tipo_id = 1 AND (hist_campo_fecha_entidad IS NOT NULL AND
                        hist_campo_fecha_entidad ~ '^[\w]*$' AND
                        hist_intervalo_historico IS NOT NULL AND 
                        hist_intervalo_historico::text ~ '^(-[0-9] [\w]+)$'::text AND
                        hist_intervalo_unidad IS NOT NULL
                     )) 
      OR (hist_tipo_id = 2 AND (hist_campo_fecha_entidad IS NOT NULL AND 
                        hist_campo_fecha_entidad ~ '^(\w+\;\w+)$' AND
                        hist_intervalo_historico IS NOT NULL AND 
                        hist_intervalo_historico::text ~ '^(-[0-9] [\w]+)$'::text AND
                        hist_intervalo_activo IS NULL AND 
                        hist_intervalo_unidad IS NOT NULL						   
                     ))
      OR(hist_tipo_id = 3 AND (hist_campo_fecha_entidad IS NULL AND
                        hist_intervalo_historico IS NULL AND 
                        hist_intervalo_activo IS NULL AND
                        hist_intervalo_unidad IS NULL
                        ))
	  OR(hist_tipo_id = 4 AND (hist_campo_fecha_entidad IS NULL AND
                        hist_intervalo_historico IS NULL AND 
                        hist_intervalo_activo IS NULL AND
                        hist_intervalo_unidad IS NULL
                        ))
   );

	--TABLAS DICCIONARIO

	-- TIPOS_MODIFICACIONES_INSP
   	CREATE TABLE TIPOS_MODIFICACIONES_INSP (
      	MOD_ID            INTEGER                     not null,
     	MOD_CODIGO        CHARACTER VARYING(50)       not null,   
      	constraint PK_TIPOS_MODIFICACIONES_INSP primary key (MOD_ID)
	);
	insert into TIPOS_MODIFICACIONES_INSP (mod_id, mod_codigo) values (1, 'SIN_MODIFICACIONES');
	insert into TIPOS_MODIFICACIONES_INSP (mod_id, mod_codigo) values (2, 'PENDIENTE_MODIFICAR');
	insert into TIPOS_MODIFICACIONES_INSP (mod_id, mod_codigo) values (3, 'MODIFICADO');
   
	-- TIPOS_MODIFICACIONES_INSP_IDIOMA
	CREATE TABLE TIPOS_MODIFICACIONES_INSP_IDIOMA (
		MOD_ID        INTEGER                     REFERENCES TIPOS_MODIFICACIONES_INSP(MOD_ID),
		IDIOMA        CHAR(5)                     REFERENCES IDIOMAS(IDIOMA),
		LITERAL       CHARACTER VARYING(50)       null,
		constraint PK_TIPOS_MODIFICACIONES_INSP_IDIOMA primary key (MOD_ID, IDIOMA)
	);
	insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (1, 'ca-ES', 'Sense modificacions');
	insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (1, 'es-ES', 'Sin modificaciones');
	insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (2, 'ca-ES', 'Sol·licitud de modificació');
	insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (2, 'es-ES', 'Solicitud de modificación');
	insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (3, 'ca-ES', 'Modificat');
	insert into TIPOS_MODIFICACIONES_INSP_IDIOMA (mod_id, idioma, literal) values (3, 'es-ES', 'Modificado');

	
   --TIPOS_ASISTENCIA_INSP
   	CREATE TABLE TIPOS_ASISTENCIA_INSP (
      ASISTENCIA_ID            INTEGER                     not null,
      ASISTENCIA_CODIGO        CHARACTER VARYING(50)       not null,   
      constraint PK_TIPOS_ASISTENCIA_INSP primary key (ASISTENCIA_ID)
   	);
   	insert into TIPOS_ASISTENCIA_INSP (asistencia_id, asistencia_codigo) values (1, 'EN_PRESENCIADE');
   	insert into TIPOS_ASISTENCIA_INSP (asistencia_id, asistencia_codigo) values (2, 'NO_IDENTIFICACION');
   	insert into TIPOS_ASISTENCIA_INSP (asistencia_id, asistencia_codigo) values (3, 'NO_NECESARIO');
   
   
    --TIPOS_MOD_INSP_IDIOMA   
   	CREATE TABLE TIPOS_ASISTENCIA_INSP_IDIOMA (
      ASISTENCIA_ID INTEGER                     REFERENCES TIPOS_ASISTENCIA_INSP(ASISTENCIA_ID),
      IDIOMA        CHAR(5)                     REFERENCES IDIOMAS(IDIOMA),
      LITERAL       CHARACTER VARYING(50)       null,
      constraint PK_TIPOS_ASISTENCIA_INSP_IDIOMA primary key (ASISTENCIA_ID, IDIOMA)
   	);
   	insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (1, 'ca-ES', 'En presència de');
   	insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (1, 'es-ES', 'En presencia de');
   	insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (2, 'ca-ES', 'No es vol identificar');
   	insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (2, 'es-ES', 'No se quiere identificar');
   	insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (3, 'ca-ES', 'No és necessari');
   	insert into TIPOS_ASISTENCIA_INSP_IDIOMA (asistencia_id, idioma, literal) values (3, 'es-ES', 'No es necesario');



	--Añadir campo identificador
	ALTER TABLE INSPECCIONES ADD COLUMN INS_IDENTIFICADOR CHARACTER VARYING(12) NOT NULL DEFAULT f_ins_identificador_year_seq();

	--Añadir campos revisión jurídica
	ALTER TABLE INSPECCIONES ADD COLUMN INS_REVISION BOOLEAN NULL;
	ALTER TABLE INSPECCIONES ADD COLUMN INS_REVISION_DESC CHARACTER VARYING(1000) NULL;

	--Añadir campo modificaciones realizadas en el expediente	
	ALTER TABLE INSPECCIONES ADD COLUMN INS_MODIFICACIONES_ESTADO INTEGER NULL;
    ALTER TABLE INSPECCIONES ADD CONSTRAINT FK_TIPO_MODIFICACION_INSP FOREIGN KEY (INS_MODIFICACIONES_ESTADO) REFERENCES TIPOS_MODIFICACIONES_INSP (MOD_ID);
	UPDATE INSPECCIONES SET INS_MODIFICACIONES_ESTADO = 1 WHERE INS_MOD_DATOS_PERS IS NULL OR INS_MOD_DATOS_PERS = '';
	UPDATE INSPECCIONES SET INS_MODIFICACIONES_ESTADO = 2 WHERE INS_MOD_DATOS_PERS IS NOT NULL;

	--Modificar campo asitencia + campo descripción
	ALTER TABLE INSPECCIONES ALTER COLUMN INS_ASISTENCIA SET DATA TYPE INTEGER USING INS_ASISTENCIA::INTEGER;	
	UPDATE INSPECCIONES SET INS_ASISTENCIA = 3 WHERE INS_ASISTENCIA = 0 OR INS_ASISTENCIA IS NULL;
	ALTER TABLE INSPECCIONES ADD CONSTRAINT FK_TIPO_ASISTENCIA_INSP FOREIGN KEY (INS_ASISTENCIA) REFERENCES TIPOS_ASISTENCIA_INSP (ASISTENCIA_ID); 	
	ALTER TABLE INSPECCIONES ADD COLUMN INS_ASISTENCIA_DESC CHARACTER VARYING(1000) NULL;

	--Inspeccion genérica
	ALTER TABLE INSPECCIONES ADD COLUMN INS_GENERICA BOOLEAN;
	UPDATE INSPECCIONES SET INS_GENERICA = false;


	-- [!!TODO] RECREAR VISTAS



EXCEPTION WHEN others THEN   
    RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;

END
$$
LANGUAGE plpgsql;




-- Actualizar función f_setearVigencia()
CREATE OR REPLACE FUNCTION f_setearVigencia(tipo_entidad_id integer)
	RETURNS BOOLEAN AS $$
DECLARE 
	tabla_entidad text;
	tabla_entidad_temp text;
	entidad_esexpediente boolean;
	
	campo_fecha text;
	intervalo_activo interval;
	intervalo_historico interval;
	intervalo_unidad varchar(50);
	tipo_historico integer;
	esquema_entidad text;
	campo_fecha_inicio text;
	campo_fecha_fin text;

	q text;
	q_actualiza_eliminados text;
	r record;

	b_existe boolean;
	b_existe2 boolean;
	b_resultado boolean;
		
	str_fecha_entidad text;
	str_fecha_inicio_activo text;
	str_fecha_final_activo text;
	str_fecha_inicio_historico text;
	str_fecha_actual text;

BEGIN
	b_resultado := TRUE;

	--Obtener el nombre y esquema de la tabla que vamos a actualizar
	q := 'SELECT tabla_entidad, esquema_entidad, es_expediente FROM tipos_entidad WHERE tipo_entidad_id = '|| tipo_entidad_id || '';
	EXECUTE q INTO tabla_entidad, esquema_entidad, entidad_esexpediente;
	tabla_entidad_temp := tabla_entidad || '_temp';

	--Obtener la configuración del histórico para esa entidad
	q := 'SELECT hist_campo_fecha_entidad, hist_intervalo_activo, hist_intervalo_historico, hist_intervalo_unidad, hist_tipo_id 
		  FROM tipos_entidad_config WHERE tipo_entidad_id = '|| tipo_entidad_id || '';
	EXECUTE q INTO campo_fecha, intervalo_activo, intervalo_historico, intervalo_unidad, tipo_historico;

	CASE tipo_historico
	WHEN 1 THEN --(1) Histórico respecto una fecha

		--validamos que el campo fecha configurado existe en la entidad
		SELECT f_validaCampo(esquema_entidad, tabla_entidad, campo_fecha) INTO b_existe;
		IF b_existe IS TRUE THEN

			IF intervalo_activo IS NULL THEN --si es el caso 1 y el campo intervalo_activo está a null, corresponde al (mes/año...) actual cogiendo la unidad_intervalo.
				intervalo_activo = '0 ' || intervalo_unidad;
			END IF;

			str_fecha_entidad := '(date_trunc('''|| intervalo_unidad ||''', '|| campo_fecha ||'))::date';
			str_fecha_inicio_activo := '(date_trunc('''|| intervalo_unidad ||''', current_date)+'''|| intervalo_activo ||''')::date';
			str_fecha_final_activo := '(date_trunc('''|| intervalo_unidad ||''', current_date))::date';		
			str_fecha_inicio_historico := '(date_trunc('''|| intervalo_unidad ||''', current_date)+'''|| intervalo_historico ||''')::date';
					
			q := 'UPDATE ' || tabla_entidad || ' SET ent_vigencia = 
				CASE 
					WHEN '|| str_fecha_inicio_activo || ' <= '|| str_fecha_entidad ||' AND '|| str_fecha_entidad ||' <= ' || str_fecha_final_activo ||' THEN 1 
					WHEN '|| str_fecha_inicio_historico || ' <= '|| str_fecha_entidad ||' AND '|| str_fecha_entidad ||' <= ' || str_fecha_inicio_activo ||' THEN 0
					ELSE -1 
				END';

			--RAISE NOTICE '%', q;
			EXECUTE q;
		
		ELSE 
			b_resultado := FALSE;
			RAISE WARNING 'El campo [%] no existe en la entidad [%].', campo_fecha, tabla_entidad;
		END IF;

	WHEN 2 THEN --(2) Histórico respecto un intervalo		
		
		q:='select split_part(hist_campo_fecha_entidad, '';'', 1) from tipos_entidad_config where tipo_entidad_id =' || tipo_entidad_id || ' ;';
		EXECUTE q into campo_fecha_inicio;		
		
		q:='select split_part(hist_campo_fecha_entidad, '';'', 2) from tipos_entidad_config where tipo_entidad_id =' || tipo_entidad_id || ' ;';
		EXECUTE q into campo_fecha_fin;

		--validamos campos
		SELECT f_validaCampo(esquema_entidad, tabla_entidad, campo_fecha_fin) INTO b_existe;
		SELECT f_validaCampo(esquema_entidad, tabla_entidad, campo_fecha_inicio) INTO b_existe2;
	
		IF b_existe IS TRUE AND b_existe2 IS TRUE THEN

			str_fecha_inicio_historico := '(date_trunc('''|| intervalo_unidad ||''', current_date)+'''|| intervalo_historico ||''')::date';
			str_fecha_entidad := '(date_trunc('''|| intervalo_unidad ||''', '|| campo_fecha_inicio|| '))::date';
			str_fecha_actual := '(date_trunc('''|| intervalo_unidad ||''', current_date))::date';
		
			q := 'UPDATE ' || tabla_entidad || ' SET ent_vigencia = 
				CASE 
					WHEN '|| campo_fecha_inicio || ' <= current_date and current_date <= '|| campo_fecha_fin || ' THEN 1 
					WHEN '|| str_fecha_inicio_historico || ' <= '|| str_fecha_entidad ||' AND '|| str_fecha_entidad ||' < ' || str_fecha_actual || ' THEN 0 
					ELSE -1 
				END';

			--RAISE NOTICE '%', q;
			EXECUTE q;

		ELSE
			b_resultado := FALSE;
			RAISE WARNING 'El campo [%] o [%] no existen en la entidad [%].', campo_fecha_fin, campo_fecha_inicio, tabla_entidad;
		END IF;
				
	
	WHEN 3 THEN --(3) Sin historico (todos son vigentes)
		q := 'UPDATE ' || tabla_entidad || ' SET ent_vigencia = 1' ;
		
		--RAISE NOTICE '%', q;
		EXECUTE q;	


	WHEN 4 THEN --(4) Histórico respecto el estado del expediente
		q := 'UPDATE ' || tabla_entidad || ' SET ent_vigencia = 
				CASE 
					WHEN exp_estado_expediente = 1 THEN 1 
					WHEN exp_estado_expediente = 2 THEN 0 
					ELSE -1
				END';
		
		--RAISE NOTICE '%', q;
		EXECUTE q;	

	END CASE;

	--Finalmente ejecutamos una query que SOLO aplica a las entidades que NO son expedientes
	--Validamos si hay entidades que se hayan eliminado en los datos origen y les
	--ponemos ent_vigencia = -1
	IF entidad_esexpediente IS FALSE THEN
        q_actualiza_eliminados := 'UPDATE '|| tabla_entidad ||' e 
								   SET ent_vigencia = -1  
								   FROM '|| tabla_entidad_temp ||' AS tmp 
								   WHERE NOT EXISTS (
									 SELECT  *
									 FROM  '|| tabla_entidad_temp ||' tmp
									 WHERE  e.id_elemento = tmp.id_elemento
								   );';
		--RAISE NOTICE '%', q_actualiza_eliminados;
		EXECUTE q_actualiza_eliminados;

    END IF;

RETURN b_resultado;

EXCEPTION 
    WHEN others THEN        
        RAISE WARNING '% - %', SQLERRM, SQLSTATE;
		RETURN FALSE;
END;
$$ LANGUAGE plpgsql;