DO $$ 
DECLARE
    
    epsg integer;
    int_tipo_area_id integer;
    r record;
    q text;
    sql_create_entity text;    
    sql_create_entity_insp text;
    

BEGIN 

    DROP TABLE SIS_VERSION;


    /*==============================================================*/
    /* Tabla: TIPOS_AREAS_TRABAJO                                    */
    /*==============================================================*/  
    create table TIPOS_AREAS_TRABAJO (
        TIPO_AREA_ID               SERIAL                   not null,
        TIPO_AREA_CODIGO           CHARACTER VARYING(50)    UNIQUE,
        constraint PK_TIPOS_AREAS_TRABAJO primary key (TIPO_AREA_ID)
    );
    insert into tipos_areas_trabajo (tipo_area_codigo) values ('SIN AREA'); --BARRIO, DISTRITOS, AT_MERCADO,

    /*==============================================================*/
    /* Tabla: AREA_TRABAJO                                          */
    /*==============================================================*/  
    SELECT c.epsg into epsg from config_cliente c;
    
    q := 'create table AREAS_TRABAJO (
            AREA_ID              SERIAL                           not null,
            TIPO_AREA_ID         INTEGER                          REFERENCES TIPOS_AREAS_TRABAJO(TIPO_AREA_ID),
            AREA_DESCRIPCION     CHARACTER VARYING(100)           not null,
            AREA_GEOM            Geometry(Polygon, '||epsg||')    null,
            constraint PK_AREAS_TRABAJO primary key (AREA_ID)
            );';
    EXECUTE q;

    CREATE INDEX IDX_GEOM_AREAS_TRABAJO ON AREAS_TRABAJO USING GIST (AREA_GEOM);
    

/*==============================================================*/
   /* Tabla: TIPOS_ENTIDAD                                         */
   /*==============================================================*/
   create table TIPOS_ENTIDAD (
      TIPO_ENTIDAD_ID              INTEGER                     not null,
      TIPO_ENTIDAD_CODIGO          CHARACTER VARYING(20)       not null,
      TABLA_ENTIDAD                CHARACTER VARYING(100)      not null,
      ESQUEMA_ENTIDAD              CHARACTER VARYING(50)       not null,
      ES_EXPEDIENTE                BOOLEAN                     not null,
      COLUMNA_ENT_UID              CHARACTER VARYING(50)       not null, 
      COLUMNA_CODIGO               CHARACTER VARYING(50)       not null,
      COLUMNA_NOMBRE               CHARACTER VARYING(50)       not null,
      COLUMNA_LOCALIZACION         CHARACTER VARYING(50)       not null,
      CAMPOS_ADICIONALES            CHARACTER VARYING(255)     null,
      constraint PK_TIPOS_ENTIDAD primary key (TIPO_ENTIDAD_ID)
   );

   insert into TIPOS_ENTIDAD (TIPO_ENTIDAD_ID, TIPO_ENTIDAD_CODIGO, TABLA_ENTIDAD, ESQUEMA_ENTIDAD, ES_EXPEDIENTE, COLUMNA_ENT_UID, COLUMNA_CODIGO, COLUMNA_NOMBRE, COLUMNA_LOCALIZACION, CAMPOS_ADICIONALES ) values (1, 'MERCADOS', 'mercados', 'public', true, 'ent_mercados_uid', 'exp_numero', 'exp_nombre_completo', 'exp_mer_codigo', null);
   insert into TIPOS_ENTIDAD (TIPO_ENTIDAD_ID, TIPO_ENTIDAD_CODIGO, TABLA_ENTIDAD, ESQUEMA_ENTIDAD, ES_EXPEDIENTE, COLUMNA_ENT_UID, COLUMNA_CODIGO, COLUMNA_NOMBRE, COLUMNA_LOCALIZACION, CAMPOS_ADICIONALES ) values (2, 'TERRAZAS', 'terrazas', 'public', true, 'ent_terrazas_uid', 'exp_numero', 'exp_terr_nombre_local', 'exp_direccion_notificacion,terraza_ubicacion', 'ins_terrazas_mesas,ins_terrazas_sillas,ins_terrazas_superficie,ins_terrazas_otros');
   insert into TIPOS_ENTIDAD (TIPO_ENTIDAD_ID, TIPO_ENTIDAD_CODIGO, TABLA_ENTIDAD, ESQUEMA_ENTIDAD, ES_EXPEDIENTE, COLUMNA_ENT_UID, COLUMNA_CODIGO, COLUMNA_NOMBRE, COLUMNA_LOCALIZACION, CAMPOS_ADICIONALES ) values (3, 'OBRAS', 'obras', 'public', true, 'ent_obras_uid', 'exp_numero', 'exp_nombre_completo', 'exp_direccion_notificacion', 'ins_obras_estado');
   insert into TIPOS_ENTIDAD (TIPO_ENTIDAD_ID, TIPO_ENTIDAD_CODIGO, TABLA_ENTIDAD, ESQUEMA_ENTIDAD, ES_EXPEDIENTE, COLUMNA_ENT_UID, COLUMNA_CODIGO, COLUMNA_NOMBRE, COLUMNA_LOCALIZACION, CAMPOS_ADICIONALES ) values (4, 'OVP', 'ovp', 'public', true, 'ent_ovp_uid', 'exp_numero', 'exp_nombre_completo', 'exp_direccion_notificacion', null);
   insert into TIPOS_ENTIDAD (TIPO_ENTIDAD_ID, TIPO_ENTIDAD_CODIGO, TABLA_ENTIDAD, ESQUEMA_ENTIDAD, ES_EXPEDIENTE, COLUMNA_ENT_UID, COLUMNA_CODIGO, COLUMNA_NOMBRE, COLUMNA_LOCALIZACION, CAMPOS_ADICIONALES ) values (5, 'ACTIVIDADES', 'actividades', 'public', false, 'ent_actividades_uid', 'ent_actividad_numero_documento', 'ent_actividad_nombre_act', 'ent_actividad_direccion', null);

    /*==============================================================*/
    /* Tabla: TIPOS_ENTIDAD_IDIOMA                                  */
    /*==============================================================*/
    create table TIPOS_ENTIDAD_IDIOMA (   
        TIPO_ENTIDAD_ID      INTEGER              REFERENCES TIPOS_ENTIDAD(TIPO_ENTIDAD_ID),
        IDIOMA               CHAR(5)              REFERENCES IDIOMAS(IDIOMA),
        LITERAL              VARCHAR(100)         not null,
        constraint PK_TIPOS_ENTIDAD_IDIOMA primary key (IDIOMA, TIPO_ENTIDAD_ID)
    );
    insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (1, 'ca-ES', 'Mercats');
    insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (1, 'es-ES', 'Mercados');
    insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (2, 'ca-ES', 'Terrasses');
    insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (2, 'es-ES', 'Terrazas');   
    insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (3, 'ca-ES', 'Obres urbanístiques');
    insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (3, 'es-ES', 'Obras urbanísticas');
    insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (4, 'ca-ES', 'Ocupació de la via pública');
    insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (4, 'es-ES', 'Ocupación de la vía pública');
    insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (5, 'ca-ES', 'Activitats econòmiques');
    insert into TIPOS_ENTIDAD_IDIOMA (tipo_entidad_id, idioma, literal) values (5, 'es-ES', 'Actividades económicas');



    /*==============================================================*/
    /* Tabla: TIPOS_HISTORICO                                       */
    /*==============================================================*/
    create table TIPOS_HISTORICO (
        TIPO_HIST_ID          SERIAL                     not null,
        TIPO_HIST_DESCRIPCION CHARACTER VARYING(100)         null,
        constraint PK_TIPOS_HISTORICO primary key (TIPO_HIST_ID)
    );

    insert into tipos_historico (tipo_hist_id, tipo_hist_descripcion) values (1, 'Histórico respecto un campo fecha');
    insert into tipos_historico (tipo_hist_id, tipo_hist_descripcion) values (2, 'Histórico respecto un intervalo');
    insert into tipos_historico (tipo_hist_id, tipo_hist_descripcion) values (3, 'Sin intervalo');

    
    
    /*==============================================================*/
    /* Tabla: TIPOS_ENTIDAD_CONFIG                                */
    /*==============================================================*/
    create table TIPOS_ENTIDAD_CONFIG (
        TIPO_ENTIDAD_ID             INTEGER              REFERENCES TIPOS_ENTIDAD(TIPO_ENTIDAD_ID),
        ENTIDAD_ACTIVA              BOOLEAN              not null,
        TIPO_AREA_ID                INTEGER              REFERENCES TIPOS_AREAS_TRABAJO(TIPO_AREA_ID),
        HIST_CAMPO_FECHA_ENTIDAD    TEXT                 null,
        HIST_INTERVALO_HISTORICO    INTERVAL             null,
        HIST_INTERVALO_ACTIVO       INTERVAL             null,
        HIST_INTERVALO_UNIDAD       TEXT                 null,
        HIST_TIPO_ID                INTEGER              REFERENCES TIPOS_HISTORICO(TIPO_HIST_ID),
        constraint PK_TIPOS_ENTIDAD_CONFIG primary key (TIPO_ENTIDAD_ID)
    );

    ALTER TABLE TIPOS_ENTIDAD_CONFIG
    ADD CONSTRAINT check_configuracion CHECK( 
        (hist_tipo_id = 1 AND (hist_campo_fecha_entidad IS NOT NULL AND
                            hist_campo_fecha_entidad ~ '^[\w]*$' AND
                            hist_intervalo_historico IS NOT NULL AND 
                            hist_intervalo_historico::text ~ '^(-[0-9] [\w]+)$'::text AND
                            hist_intervalo_unidad IS NOT NULL
                            )) 
        OR (hist_tipo_id = 2 AND (hist_campo_fecha_entidad IS NOT NULL AND 
                                hist_campo_fecha_entidad ~ '^(\w+\;\w+)$' AND
                                hist_intervalo_historico IS NOT NULL AND 
                                hist_intervalo_historico::text ~ '^(-[0-9] [\w]+)$'::text AND
                                hist_intervalo_activo IS NULL AND 
                                hist_intervalo_unidad IS NOT NULL						   
                            ))
    OR(hist_tipo_id = 3 AND (hist_campo_fecha_entidad IS NULL AND
                        hist_intervalo_historico IS NULL AND 
                        hist_intervalo_activo IS NULL AND
                        hist_intervalo_unidad IS NULL
                        ))
    );
    --Insertamos valores por defecto para cada entidad. Si se requiere, se puede modificar a posteriori la tabla.        
   --mercados(campo fecha): activos año actual, historico año anterior
   insert into TIPOS_ENTIDAD_CONFIG (tipo_entidad_id, entidad_activa, tipo_area_id, hist_campo_fecha_entidad, hist_intervalo_historico, hist_intervalo_activo, hist_intervalo_unidad, hist_tipo_id )
   values (1, true, 1, 'exp_fecha_alta', '-1 year', null, 'year', 1);
   
   --terrazas(intervalo): activos intervalo actual, historico año anterior hasta fecha_ini
   insert into TIPOS_ENTIDAD_CONFIG (tipo_entidad_id, entidad_activa, tipo_area_id, hist_campo_fecha_entidad, hist_intervalo_historico, hist_intervalo_activo, hist_intervalo_unidad, hist_tipo_id )
   values (2, true, 1, 'terraza_fecha_ini;terraza_fecha_fin', '-1 year', null, 'year', 2);

   --obras (intervalo): activos intervalo actual, historico año anterior hasta fecha_ini
   insert into TIPOS_ENTIDAD_CONFIG (tipo_entidad_id, entidad_activa, tipo_area_id, hist_campo_fecha_entidad, hist_intervalo_historico, hist_intervalo_activo, hist_intervalo_unidad, hist_tipo_id )
   values (3, true, 1, 'exp_fecha_alta;exp_obr_fecha_fin', '-1 year', null, 'year', 2);

   --ovp (campo fecha): activos año actual, historico año anterior
   insert into TIPOS_ENTIDAD_CONFIG (tipo_entidad_id, entidad_activa, tipo_area_id, hist_campo_fecha_entidad, hist_intervalo_historico, hist_intervalo_activo, hist_intervalo_unidad, hist_tipo_id )
   values (4, true, 1, 'exp_fecha_alta', '-1 year', null, 'year', 1);

   --actividades: sin historico
   insert into TIPOS_ENTIDAD_CONFIG (tipo_entidad_id, entidad_activa, tipo_area_id, hist_campo_fecha_entidad, hist_intervalo_historico, hist_intervalo_activo, hist_intervalo_unidad, hist_tipo_id )
   values (5, true, 1, null, null, null, null, 3);

    --Pasamos los datos actuales de la tabla mercat_area_treball al nuevo modelo.
    insert into tipos_areas_trabajo (tipo_area_codigo) values ('Zonas mercados') returning tipo_area_id INTO int_tipo_area_id;
    q:= 'update tipos_entidad_config set tipo_area_id =' || int_tipo_area_id || ' where tipo_entidad_id = 1;'; --mercados


    q := 'INSERT INTO areas_trabajo (tipo_area_id, area_descripcion, area_geom)
            SELECT '|| int_tipo_area_id ||' as tipo_area_id, area_descripcion, area_geom 
            FROM mercat_area_treball WHERE area_id <> 99;';
    EXECUTE q;

    DROP TABLE mercat_area_treball CASCADE;


    /*===================================================================*/
    /* Tabla: TIPOS_INFRACCIONES                                           */
    /* refactor                                                          */
    /*===================================================================*/
    DROP VIEW IF EXISTS FA_INSPECCIO_MERCAT;
    ALTER TABLE INSPECCIO_CATALEG_TIPUS RENAME TO TIPOS_INFRACCIONES;

    ALTER TABLE TIPOS_INFRACCIONES RENAME COLUMN ict_id TO tipo_inf_id;
    ALTER TABLE TIPOS_INFRACCIONES RENAME COLUMN ict_desc TO tipo_inf_codigo;

    update TIPOS_INFRACCIONES set tipo_inf_codigo='SIN_INFRACCION' where tipo_inf_id=0;
    update TIPOS_INFRACCIONES set tipo_inf_codigo='INFRACION_LEVE' where tipo_inf_id=1;
    update TIPOS_INFRACCIONES set tipo_inf_codigo='INFRACION_GRAVE' where tipo_inf_id=2;
    update TIPOS_INFRACCIONES set tipo_inf_codigo='INFRACION_MUYGRAVE' where tipo_inf_id=3;

    /*==============================================================*/
    /* Tabla: TIPOS_INFRACCION_IDIOMA                               */
    /*==============================================================*/
    create table TIPOS_INFRACCIONES_IDIOMA (
        TIPO_INF_ID                   SERIAL                      not null,
        IDIOMA                        CHAR(5)                     REFERENCES IDIOMAS (IDIOMA),
        LITERAL                       CHARACTER VARYING(100)      null,
        constraint PK_TIPOS_INFRACCIONES_IDIOMA primary key (tipo_inf_id, idioma)
    );

    insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (0, 'ca-ES', 'Sense infracció');
    insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (0, 'es-ES', 'Sin infracción');
    insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (1, 'ca-ES', 'Lleu');
    insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (1, 'es-ES', 'Leve');
    insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (2, 'ca-ES', 'Greu');
    insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (2, 'es-ES', 'Grave');
    insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (3, 'ca-ES', 'Molt greu');
    insert into TIPOS_INFRACCIONES_IDIOMA (tipo_inf_id, idioma, literal) values (3, 'es-ES', 'Muy grave');



    /*===================================================================*/
    /* Tabla: INFRACCIONES                                               */
    /* refactor                                                          */
    /*===================================================================*/
    /*ALTER TABLE INSPECCIO_CATALEG DROP CONSTRAINT "fk_inspeccio_cataleg_tipus";
    ALTER TABLE INSPECCIO_CATALEG RENAME COLUMN INF_TIPO_FALTA_ID TO TIPO_INF_ID;    
    ALTER TABLE INSPECCIO_CATALEG ADD CONSTRAINT FK_TIPOS_INFRACCION FOREIGN KEY (TIPO_INF_ID) REFERENCES TIPOS_INFRACCION (tipo_inf_id);

    ALTER TABLE INSPECCIO_CATALEG ADD COLUMN TIPO_ENTIDAD_ID INTEGER;
    UPDATE INSPECCIO_CATALEG SET TIPO_ENTIDAD_ID = 1 WHERE inf_entidad = 'MERCAT';

    ALTER TABLE INSPECCIO_CATALEG DROP COLUMN INF_ENTIDAD;
    ALTER TABLE INSPECCIO_CATALEG DROP COLUMN INF_DESCRIPCION;
    ALTER TABLE INSPECCIO_CATALEG DROP COLUMN INF_TIPO_FALTA;
    ALTER TABLE INSPECCIO_CATALEG DROP COLUMN INF_ID;

    ALTER TABLE INSPECCIO_CATALEG 
    ADD CONSTRAINT PK_INSPECCIO_CATALEG PRIMARY KEY (inf_codigo, tipo_entidad_id);
    
    ALTER TABLE INSPECCIO_CATALEG 
    ADD CONSTRAINT FK_TIPOS_ENTIDAD FOREIGN KEY (tipo_entidad_id) REFERENCES TIPOS_ENTIDAD (tipo_entidad_id); */

    DROP TABLE INSPECCIO_CATALEG CASCADE;

    create table INFRACCIONES (   
        INF_CODIGO           CHAR(11)                    not null,   
        TIPO_ENTIDAD_ID      INTEGER                     REFERENCES TIPOS_ENTIDAD(TIPO_ENTIDAD_ID),
        TIPO_INF_ID          INTEGER                     REFERENCES TIPOS_INFRACCIONES(TIPO_INF_ID),   
        constraint PK_INFRACCIONES primary key (INF_CODIGO, TIPO_ENTIDAD_ID)
    );


    /*==============================================================*/
    /* Tabla: INFRACCIONES_IDIOMA                                   */
    /*==============================================================*/
    create table INFRACCIONES_IDIOMA (   
        INF_CODIGO           CHAR(11)                    not null,
        TIPO_ENTIDAD_ID      INTEGER                     not null,
        IDIOMA               CHAR(5)                     REFERENCES IDIOMAS(IDIOMA),
        LITERAL              TEXT                        null,   
        constraint FK_INFRACCIONES foreign key (INF_CODIGO, TIPO_ENTIDAD_ID) references INFRACCIONES (INF_CODIGO, TIPO_ENTIDAD_ID),
        constraint PK_INFRACCIONES_IDIOMA primary key (INF_CODIGO, TIPO_ENTIDAD_ID, IDIOMA)
    );

    /*==============================================================*/
    /* Tabla: TIPOS_ESTADOS_INSP                                    */
    /*==============================================================*/
    CREATE TABLE TIPOS_ESTADOS_INSP (
        ESTADO_ID            BOOLEAN                     not null,
        ESTADO_CODIGO        CHARACTER VARYING(50)       null,   
        constraint PK_TIPOS_ESTADOS_INSP primary key (ESTADO_ID)
    );
    insert into TIPOS_ESTADOS_INSP (estado_id, estado_codigo) values (true, 'FINALIZADA');
    insert into TIPOS_ESTADOS_INSP (estado_id, estado_codigo) values (false, 'PENDIENTE');
    
    /*==============================================================*/
    /* Tabla: TIPOS_ESTADOS_INSP_IDIOMA                            */
    /*==============================================================*/
    CREATE TABLE TIPOS_ESTADOS_INSP_IDIOMA (
        ESTADO_ID     BOOLEAN                     REFERENCES TIPOS_ESTADOS_INSP(ESTADO_ID),
        IDIOMA        CHAR(5)                     REFERENCES IDIOMAS(IDIOMA),
        LITERAL       CHARACTER VARYING(50)       null,
        constraint PK_TIPOS_ESTADOS_INSP_IDIOMA primary key (ESTADO_ID, IDIOMA)
    );
    insert into TIPOS_ESTADOS_INSP_IDIOMA (estado_id, idioma, literal) values (false, 'ca-ES', 'Pendent');
    insert into TIPOS_ESTADOS_INSP_IDIOMA (estado_id, idioma, literal) values (true, 'ca-ES', 'Finalitzada');
    insert into TIPOS_ESTADOS_INSP_IDIOMA (estado_id, idioma, literal) values (false, 'es-ES', 'Pendiente');
    insert into TIPOS_ESTADOS_INSP_IDIOMA (estado_id, idioma, literal) values (true, 'es-ES', 'Finalizada');




    /*===================================================================*/
    /* Tabla: MERCADOS                                                   */
    /* refactor, añadir atributo ent_vigencia + corregir integer         */
    /*===================================================================*/
    ALTER TABLE MERCAT ADD COLUMN ENT_VIGENCIA INTEGER;
    ALTER TABLE MERCAT ADD COLUMN EXP_MER_CODIGO CHARACTER VARYING(15);
    ALTER TABLE MERCAT ALTER COLUMN AA_IDA SET DATA TYPE INTEGER;
    ALTER TABLE MERCAT ALTER COLUMN EXP_ESTADO_EXPEDIENTE SET DATA TYPE INTEGER;
    ALTER TABLE MERCAT ALTER COLUMN EXP_INT_TELEF SET DATA TYPE CHARACTER VARYING(12);
    ALTER TABLE MERCAT ALTER COLUMN EXP_MER_TIPO SET DATA TYPE CHARACTER VARYING(100);
    ALTER TABLE MERCAT ALTER COLUMN EXP_MER_TIPO DROP NOT NULL;
    ALTER TABLE MERCAT ALTER COLUMN EXP_NUMERO SET NOT NULL;
    UPDATE MERCAT set EXP_MER_TIPO = EXP_MER_TIPO_DESC;
    ALTER TABLE MERCAT DROP COLUMN EXP_MER_TIPO_DESC;
    ALTER TABLE MERCAT DROP COLUMN AA_GEOM_CENTROID;
    DROP TRIGGER IF EXISTS calc_centroide ON MERCAT CASCADE;

    ALTER TABLE MERCAT RENAME TO MERCADOS;
    CREATE INDEX IDX_GEOM_MERCADOS ON MERCADOS USING GIST (AA_GEOM);
    

    /*===================================================================*/
    /* Tabla: MERCADOS_TEMP                                               */
    /* refactor, corregir integer                                        */
    /*===================================================================*/               
    ALTER TABLE MERCAT_TEMP ALTER COLUMN AA_IDA SET DATA TYPE INTEGER;
    ALTER TABLE MERCAT_TEMP ADD COLUMN EXP_MER_CODIGO CHARACTER VARYING(15);
    ALTER TABLE MERCAT_TEMP ALTER COLUMN EXP_ESTADO_EXPEDIENTE SET DATA TYPE INTEGER;
    ALTER TABLE MERCAT_TEMP ALTER COLUMN EXP_INT_TELEF SET DATA TYPE CHARACTER VARYING(12);
    ALTER TABLE MERCAT_TEMP ALTER COLUMN EXP_MER_TIPO SET DATA TYPE CHARACTER VARYING(100);
    ALTER TABLE MERCAT_TEMP ALTER COLUMN EXP_MER_TIPO DROP NOT NULL;
    ALTER TABLE MERCAT_TEMP ALTER COLUMN EXP_NUMERO SET NOT NULL;
    UPDATE MERCAT_TEMP set EXP_MER_TIPO = EXP_MER_TIPO_DESC;
    ALTER TABLE MERCAT_TEMP DROP COLUMN EXP_MER_TIPO_DESC;

    ALTER TABLE MERCAT_TEMP RENAME TO MERCADOS_TEMP;


    /*==============================================================*/
   /* Tabla: TERRAZAS                                              */
   /*==============================================================*/
   sql_create_entity := 'CREATE table TERRAZAS (
                           ENT_UID                       CHARACTER(36)                   not null,
                           ENT_FECHA_REVISION            TIMESTAMP                       null,
                           ENT_ULTIMA_INSP               TIMESTAMP                       null,
                           ENT_VIGENCIA                  INTEGER                         null,
                           AA_IDA                        INTEGER                         not null,
                           AA_GEOM                       Geometry(Polygon,'||epsg||')    null,                           
                           AA_DES                        CHARACTER VARYING(100)          null,
                           ID_ELEMENTO                   CHARACTER VARYING(40)           not null,
                           TIPO_ELEM                     CHARACTER VARYING(40)           null,
                           EXP_DESCRIPCION               CHARACTER VARYING(100)          null,
                           EXP_DIRECCION                 CHARACTER VARYING(255)          null,
                           EXP_DIRECCION_NOTIFICACION    CHARACTER VARYING(255)          null,
                           EXP_ESTADO_EXPEDIENTE         INTEGER                         not null,
                           EXP_ESTADO_EXPEDIENTE_DESC    CHARACTER VARYING(100)          null,
                           EXP_FECHA_ALTA                TIMESTAMP                       null,
                           EXP_FECHA_BAJA                TIMESTAMP                       null,
                           EXP_INT_EMAIL                 CHARACTER VARYING(255)          null,
                           EXP_INT_TELEF                 CHARACTER VARYING(12)           null,
                           EXP_NOMBRE_COMPLETO           CHARACTER VARYING(100)          null,
                           EXP_NUMERO                    CHARACTER VARYING(25)           not null,
                           EXP_NUMERO_DOCUMENTO          CHARACTER VARYING(10)           null,
                           EXP_OBSERVACIONES             CHARACTER VARYING(1000)         null,
                           EXP_REFCAT                    CHARACTER VARYING(20)           null,
                           EXP_TIPO_DOCUMENTO            CHARACTER VARYING(50)           null,
                           EXP_TIPO_PERSONA              CHARACTER VARYING(100)          null,
                           EXP_TERR_NOMBRE_LOCAL         CHARACTER VARYING(100)          null,
                           EXP_TERR_REPR_NOMBRE          CHARACTER VARYING(100)          null,
                           EXP_TERR_REPR_TIPO_DOC        CHARACTER VARYING(50)           null,
                           EXP_TERR_REPR_NUMERO_DOC      CHARACTER VARYING(10)           null,
                           EXP_TERR_REPR_TELEFONO        CHARACTER VARYING(12)           null,
                           TERRAZA_CODIGO_SOLICITUD      CHARACTER VARYING(50)           not null,
                           TERRAZA_FECHA_INI             TIMESTAMP                       null,
                           TERRAZA_FECHA_FIN             TIMESTAMP                       null,
                           TERRAZA_SUPERFICIE_AUT        NUMERIC                         null,
                           TERRAZA_MESAS_AUT             INTEGER                         null,
                           TERRAZA_SILLAS_AUT            INTEGER                         null,
                           TERRAZA_OTROS_ELEM            CHARACTER VARYING(100)          null,
                           TERRAZA_UBICACION             CHARACTER VARYING(100)          null,
                           TERRAZA_OBSERVACIONES         CHARACTER VARYING(255)          null,
                           TERRAZA_ESTADO_SOLICITUD      CHARACTER VARYING(50)           null,
                           TERRAZA_MOTIVO_DENEGACION     CHARACTER VARYING(255)          null,
                           constraint PK_TERRAZAS primary key (ENT_UID)
                        );';

   EXECUTE sql_create_entity;
   CREATE INDEX IDX_GEOM_TERRAZAS ON TERRAZAS USING GIST (AA_GEOM);



     /*==============================================================*/
   /* Table: TERRAZAS_TEMP                                           */
   /*==============================================================*/
   create table TERRAZAS_TEMP (
      OGC_FID                     SERIAL                          not null,
      AA_IDA                      INTEGER                         not null,
      AA_GEOM                     Geometry(Polygon, 4326)         null,
      AA_DES                      CHARACTER VARYING(100)          null,   
      ID_ELEMENTO                 CHARACTER VARYING(40)           not null,   
      TIPO_ELEM                   CHARACTER VARYING(40)           null,
      EXP_DESCRIPCION             CHARACTER VARYING(100)          null,
      EXP_DIRECCION               CHARACTER VARYING(255)          null,
      EXP_DIRECCION_NOTIFICACION  CHARACTER VARYING(255)          null,
      EXP_ESTADO_EXPEDIENTE       INTEGER                         not null,
      EXP_ESTADO_EXPEDIENTE_DESC  CHARACTER VARYING(100)          null,
      EXP_FECHA_ALTA              TIMESTAMP                       null,
      EXP_FECHA_BAJA              TIMESTAMP                       null,
      EXP_INT_EMAIL               CHARACTER VARYING(255)          null,
      EXP_INT_TELEF               CHARACTER VARYING(12)           null,
      EXP_NOMBRE_COMPLETO         CHARACTER VARYING(100)          null,
      EXP_NUMERO                  CHARACTER VARYING(25)           not null,
      EXP_NUMERO_DOCUMENTO        CHARACTER VARYING(10)           null,
      EXP_OBSERVACIONES           CHARACTER VARYING(1000)         null,
      EXP_REFCAT                  CHARACTER VARYING(20)           null,
      EXP_TIPO_DOCUMENTO          CHARACTER VARYING(50)           null,
      EXP_TIPO_PERSONA            CHARACTER VARYING(100)          null,
      EXP_TERR_NOMBRE_LOCAL       CHARACTER VARYING(100)          null,
      EXP_TERR_REPR_NOMBRE        CHARACTER VARYING(100)          null,
      EXP_TERR_REPR_TIPO_DOC      CHARACTER VARYING(50)           null,
      EXP_TERR_REPR_NUMERO_DOC    CHARACTER VARYING(10)           null,
      EXP_TERR_REPR_TELEFONO      CHARACTER VARYING(12)           null,
      TERRAZA_CODIGO_SOLICITUD    CHARACTER VARYING(50)           not null,
      TERRAZA_FECHA_INI           TIMESTAMP                       null,
      TERRAZA_FECHA_FIN           TIMESTAMP                       null,
      TERRAZA_SUPERFICIE_AUT      NUMERIC                         null,
      TERRAZA_MESAS_AUT           INTEGER                         null,
      TERRAZA_SILLAS_AUT          INTEGER                         null,
      TERRAZA_OTROS_ELEM          CHARACTER VARYING(100)          null,
      TERRAZA_UBICACION           CHARACTER VARYING(100)          null,
      TERRAZA_OBSERVACIONES       CHARACTER VARYING(255)          null,
      TERRAZA_ESTADO_SOLICITUD    CHARACTER VARYING(50)           null,
      TERRAZA_MOTIVO_DENEGACION   CHARACTER VARYING(255)          null,
      
      constraint PK_TERRAZAS_TEMP primary key (OGC_FID)
   );



    /*==============================================================*/
   /* Tabla: OBRAS                                                 */
   /*==============================================================*/
   sql_create_entity := 'CREATE table OBRAS (
                           ENT_UID                       CHARACTER(36)                   not null,
                           ENT_FECHA_REVISION            TIMESTAMP                       null,
                           ENT_ULTIMA_INSP               TIMESTAMP                       null,
                           ENT_VIGENCIA                  INTEGER                         null,
                           AA_IDA                        INTEGER                         not null,
                           AA_GEOM                       Geometry(Point,'||epsg||')      null,                          
                           AA_DES                        CHARACTER VARYING(100)          null,
                           ID_ELEMENTO                   CHARACTER VARYING(40)           not null,
                           TIPO_ELEM                     CHARACTER VARYING(40)           null,
                           EXP_DESCRIPCION               CHARACTER VARYING(100)          null,
                           EXP_DIRECCION                 CHARACTER VARYING(255)          null,
                           EXP_DIRECCION_NOTIFICACION    CHARACTER VARYING(255)          null,
                           EXP_ESTADO_EXPEDIENTE         INTEGER                         not null,
                           EXP_ESTADO_EXPEDIENTE_DESC    CHARACTER VARYING(100)          null,
                           EXP_FECHA_ALTA                TIMESTAMP                       null,
                           EXP_FECHA_BAJA                TIMESTAMP                       null,
                           EXP_INT_EMAIL                 CHARACTER VARYING(255)          null,
                           EXP_INT_TELEF                 CHARACTER VARYING(12)           null,
                           EXP_NOMBRE_COMPLETO           CHARACTER VARYING(100)          null,
                           EXP_NUMERO                    CHARACTER VARYING(25)           not null,
                           EXP_NUMERO_DOCUMENTO          CHARACTER VARYING(10)           null,
                           EXP_OBSERVACIONES             CHARACTER VARYING(1000)         null,
                           EXP_REFCAT                    CHARACTER VARYING(20)           null,
                           EXP_TIPO_DOCUMENTO            CHARACTER VARYING(50)           null,
                           EXP_TIPO_PERSONA              CHARACTER VARYING(100)          null,
                           EXP_OBR_REPR_NOMBRE           CHARACTER VARYING(100)          null,
                           EXP_OBR_REPR_TIPO_DOC         CHARACTER VARYING(50)           null,
                           EXP_OBR_REPR_NUMERO_DOC       CHARACTER VARYING(10)           null,
                           EXP_OBR_REPR_TELEFONO         CHARACTER VARYING(12)           null,
                           EXP_OBR_REPR_EMAIL            CHARACTER VARYING(100)          null,
                           EXP_OBR_REPR_DIRECCION_NOTIF  CHARACTER VARYING(255)          null,
                           EXP_OBR_TIPO                  CHARACTER VARYING(50)           null,
                           EXP_OBR_CONSISTE_EN           CHARACTER VARYING(150)          null,
                           EXP_OBR_FECHA_FIN             TIMESTAMP                       null,                           
                           constraint PK_OBRAS primary key (ENT_UID)
                        );';

   EXECUTE sql_create_entity;

   CREATE INDEX IDX_GEOM_OBRAS ON OBRAS USING GIST (AA_GEOM);


   /*==============================================================*/
   /* Table: OBRAS_TEMP                                            */
   /*==============================================================*/
   create table OBRAS_TEMP (
      OGC_FID                       SERIAL                          not null,
      AA_IDA                        INTEGER                         not null,
      AA_GEOM                       Geometry(Point, 4326)           null,
      AA_DES                        CHARACTER VARYING(100)          null,   
      ID_ELEMENTO                   CHARACTER VARYING(40)           not null,   
      TIPO_ELEM                     CHARACTER VARYING(40)           null,
      EXP_DESCRIPCION               CHARACTER VARYING(100)          null,
      EXP_DIRECCION                 CHARACTER VARYING(255)          null,
      EXP_DIRECCION_NOTIFICACION    CHARACTER VARYING(255)          null,
      EXP_ESTADO_EXPEDIENTE         INTEGER                         not null,
      EXP_ESTADO_EXPEDIENTE_DESC    CHARACTER VARYING(100)          null,
      EXP_FECHA_ALTA                TIMESTAMP                       null,
      EXP_FECHA_BAJA                TIMESTAMP                       null,
      EXP_INT_EMAIL                 CHARACTER VARYING(255)          null,
      EXP_INT_TELEF                 CHARACTER VARYING(12)           null,
      EXP_NOMBRE_COMPLETO           CHARACTER VARYING(100)          null,
      EXP_NUMERO                    CHARACTER VARYING(25)           not null,
      EXP_NUMERO_DOCUMENTO          CHARACTER VARYING(10)           null,
      EXP_OBSERVACIONES             CHARACTER VARYING(1000)         null,
      EXP_REFCAT                    CHARACTER VARYING(20)           null,
      EXP_TIPO_DOCUMENTO            CHARACTER VARYING(50)           null,
      EXP_TIPO_PERSONA              CHARACTER VARYING(100)          null,
      EXP_OBR_REPR_NOMBRE           CHARACTER VARYING(100)          null,
      EXP_OBR_REPR_TIPO_DOC         CHARACTER VARYING(50)           null,
      EXP_OBR_REPR_NUMERO_DOC       CHARACTER VARYING(10)           null,
      EXP_OBR_REPR_TELEFONO         CHARACTER VARYING(12)           null,
      EXP_OBR_REPR_EMAIL            CHARACTER VARYING(100)          null,
      EXP_OBR_REPR_DIRECCION_NOTIF  CHARACTER VARYING(255)          null,
      EXP_OBR_TIPO                  CHARACTER VARYING(50)           null,
      EXP_OBR_CONSISTE_EN           CHARACTER VARYING(150)          null,
      EXP_OBR_FECHA_FIN             TIMESTAMP                       null,      
      constraint PK_OBRAS_TEMP primary key (OGC_FID)
   );

   /*==============================================================*/
   /* Tabla: OVP                                                   */
   /*==============================================================*/
   sql_create_entity := 'CREATE table OVP (
                           ENT_UID                          CHARACTER(36)                   not null,
                           ENT_FECHA_REVISION               TIMESTAMP                       null,
                           ENT_ULTIMA_INSP                  TIMESTAMP                       null,
                           ENT_VIGENCIA                     INTEGER                         null,
                           AA_IDA                           INTEGER                         not null,
                           AA_GEOM                          Geometry(Polygon,'||epsg||')    null,                           
                           AA_DES                           CHARACTER VARYING(100)          null,
                           ID_ELEMENTO                      CHARACTER VARYING(40)           not null,
                           TIPO_ELEM                        CHARACTER VARYING(40)           null,
                           EXP_DESCRIPCION                  CHARACTER VARYING(100)          null,
                           EXP_DIRECCION                    CHARACTER VARYING(255)          null,
                           EXP_DIRECCION_NOTIFICACION       CHARACTER VARYING(255)          null,
                           EXP_ESTADO_EXPEDIENTE            INTEGER                         not null,
                           EXP_ESTADO_EXPEDIENTE_DESC       CHARACTER VARYING(100)          null,
                           EXP_FECHA_ALTA                   TIMESTAMP                       null,
                           EXP_FECHA_BAJA                   TIMESTAMP                       null,
                           EXP_INT_EMAIL                    CHARACTER VARYING(255)          null,
                           EXP_INT_TELEF                    CHARACTER VARYING(12)           null,
                           EXP_NOMBRE_COMPLETO              CHARACTER VARYING(100)          null,
                           EXP_NUMERO                       CHARACTER VARYING(25)           not null,
                           EXP_NUMERO_DOCUMENTO             CHARACTER VARYING(10)           null,
                           EXP_OBSERVACIONES                CHARACTER VARYING(1000)         null,
                           EXP_REFCAT                       CHARACTER VARYING(20)           null,
                           EXP_TIPO_DOCUMENTO               CHARACTER VARYING(50)           null,
                           EXP_TIPO_PERSONA                 CHARACTER VARYING(100)          null,
                           EXP_OVP_TIPO_OCUPACION           CHARACTER VARYING(50)           null,
                           EXP_OVP_MOTIVO                   CHARACTER VARYING(150)          null,
                           EXP_OVP_TIPO_VEHICULO            CHARACTER VARYING(50)           null,
                           EXP_OVP_REF_LICENCIA             CHARACTER VARYING(20)           null,
                           EXP_OVP_CARRIL_LARGO             NUMERIC                         null,
                           EXP_OVP_CARRIL_ANCHO             NUMERIC                         null,    
                           EXP_OVP_CARRIL_FECHA_INI         TIMESTAMP                       null,
                           EXP_OVP_CARRIL_FECHA_FIN         TIMESTAMP                       null,
                           EXP_OVP_CARRIL_CORTE             BOOLEAN                         null,
                           EXP_OVP_CARRIL_CORTE_DESC        CHARACTER VARYING(200)          null,
                           EXP_OVP_APARCAMIENTO_LARGO       NUMERIC                         null,
                           EXP_OVP_APARCAMIENTO_ANCHO       NUMERIC                         null,
                           EXP_OVP_APARCAMIENTO_FECHA_INI   TIMESTAMP                       null,
                           EXP_OVP_APARCAMIENTO_FECHA_FIN   TIMESTAMP                       null,
                           EXP_OVP_ACERA_LARGO              NUMERIC                         null,
                           EXP_OVP_ACERA_ANCHO              NUMERIC                         null,
                           EXP_OVP_ACERA_FECHA_INI          TIMESTAMP                       null,
                           EXP_OVP_ACERA_FECHA_FIN          TIMESTAMP                       null,
                           constraint PK_OVP primary key (ENT_UID)
                        );';

   EXECUTE sql_create_entity;

   CREATE INDEX IDX_GEOM_OVP ON OVP USING GIST (AA_GEOM);


   /*==============================================================*/
   /* Table: OVP_TEMP                                              */
   /*==============================================================*/
   create table OVP_TEMP (
      OGC_FID                          SERIAL                          not null,
      AA_IDA                           INTEGER                         not null,
      AA_GEOM                          Geometry(Polygon, 4326)           null,
      AA_DES                           CHARACTER VARYING(100)          null,   
      ID_ELEMENTO                      CHARACTER VARYING(40)           not null,   
      TIPO_ELEM                        CHARACTER VARYING(40)           null,
      EXP_DESCRIPCION                  CHARACTER VARYING(100)          null,
      EXP_DIRECCION                    CHARACTER VARYING(255)          null,
      EXP_DIRECCION_NOTIFICACION       CHARACTER VARYING(255)          null,
      EXP_ESTADO_EXPEDIENTE            INTEGER                         not null,
      EXP_ESTADO_EXPEDIENTE_DESC       CHARACTER VARYING(100)          null,
      EXP_FECHA_ALTA                   TIMESTAMP                       null,
      EXP_FECHA_BAJA                   TIMESTAMP                       null,
      EXP_INT_EMAIL                    CHARACTER VARYING(255)          null,
      EXP_INT_TELEF                    CHARACTER VARYING(12)           null,
      EXP_NOMBRE_COMPLETO              CHARACTER VARYING(100)          null,
      EXP_NUMERO                       CHARACTER VARYING(25)           not null,
      EXP_NUMERO_DOCUMENTO             CHARACTER VARYING(10)           null,
      EXP_OBSERVACIONES                CHARACTER VARYING(1000)         null,
      EXP_REFCAT                       CHARACTER VARYING(20)           null,
      EXP_TIPO_DOCUMENTO               CHARACTER VARYING(50)           null,
      EXP_TIPO_PERSONA                 CHARACTER VARYING(100)          null,
      EXP_OVP_TIPO_OCUPACION           CHARACTER VARYING(50)           null,
      EXP_OVP_MOTIVO                   CHARACTER VARYING(150)          null,
      EXP_OVP_TIPO_VEHICULO            CHARACTER VARYING(50)           null,
      EXP_OVP_REF_LICENCIA             CHARACTER VARYING(20)           null,
      EXP_OVP_CARRIL_LARGO             NUMERIC                         null,
      EXP_OVP_CARRIL_ANCHO             NUMERIC                         null,    
      EXP_OVP_CARRIL_FECHA_INI         TIMESTAMP                       null,
      EXP_OVP_CARRIL_FECHA_FIN         TIMESTAMP                       null,
      EXP_OVP_CARRIL_CORTE             BOOLEAN                         null,
      EXP_OVP_CARRIL_CORTE_DESC        CHARACTER VARYING(200)          null,
      EXP_OVP_APARCAMIENTO_LARGO       NUMERIC                         null,
      EXP_OVP_APARCAMIENTO_ANCHO       NUMERIC                         null,
      EXP_OVP_APARCAMIENTO_FECHA_INI   TIMESTAMP                       null,
      EXP_OVP_APARCAMIENTO_FECHA_FIN   TIMESTAMP                       null,
      EXP_OVP_ACERA_LARGO              NUMERIC                         null,
      EXP_OVP_ACERA_ANCHO              NUMERIC                         null,
      EXP_OVP_ACERA_FECHA_INI          TIMESTAMP                       null,
      EXP_OVP_ACERA_FECHA_FIN          TIMESTAMP                       null,           
      constraint PK_OVP_TEMP primary key (OGC_FID)
   );


   /*==============================================================*/
   /* Tabla: ACTIVIDADES                                           */
   /*==============================================================*/
   sql_create_entity := 'CREATE table ACTIVIDADES (
                           ENT_UID                          CHARACTER(36)                   not null,
                           ENT_FECHA_REVISION               TIMESTAMP                       null,
                           ENT_ULTIMA_INSP                  TIMESTAMP                       null,
                           ENT_VIGENCIA                     INTEGER                         null,
                           AA_IDA                           INTEGER                         not null,
                           AA_GEOM                          Geometry(Point,'||epsg||')      null,                           
                           AA_DES                           CHARACTER VARYING(100)          null,
                           ID_ELEMENTO                      CHARACTER VARYING(40)           not null,
                           TIPO_ELEM                        CHARACTER VARYING(40)           null,
                           ENT_ACTIVIDAD_IDENTIFICADOR      CHARACTER VARYING(13)           null,
                           ENT_ACTIVIDAD_NOMBRE_ACT         CHARACTER VARYING(155)          null,
                           ENT_EMPRESA_RAZON_SOCIAL         CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_NUMERO_DOCUMENTO   CHARACTER VARYING(21)           null,
                           ENT_ACTIVIDAD_DIRECCION          CHARACTER VARYING(255)          null,
                           ENT_ACTIVIDAD_REFCAT             CHARACTER VARYING(25)           null,
                           ENT_ACTIVIDAD_EPIGRAFE           CHARACTER VARYING(255)          null,
                           ENT_ACTIVIDAD_CATEGORIA_1        CHARACTER VARYING(255)          null,
                           ENT_ACTIVIDAD_ACT_PRINCIPAL      BOOLEAN                         null,
                           ENT_ACTIVIDAD_LOCAL_ALQUILADO    BOOLEAN                         null,
                           ENT_ACTIVIDAD_WEB                CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_EMAIL_CONTACTO     CHARACTER VARYING(150)          null,
                           ENT_ACTIVIDAD_PERSONA_CONTACTO   CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_TELEFONO_CONT      CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_HORARIO_INVIERNO   CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_HORARIO_VERANO     CHARACTER VARYING(100)          null,
                           ENT_ACTIVIDAD_PUBLICABLE         BOOLEAN                         null,
                           ENT_ACTIVIDAD_FECHA_ALTA         TIMESTAMP                       null,
                           ENT_ACTIVIDAD_FECHA_BAJA         TIMESTAMP                       null,
                           ENT_ACTIVIDAD_OTROS_EPIGRAFES    text                            null,
                           constraint PK_ACTIVIDADES primary key (ENT_UID)
                        );';

   EXECUTE sql_create_entity;

   CREATE INDEX IDX_GEOM_ACTIVIDADES ON ACTIVIDADES USING GIST (AA_GEOM);




   /*==============================================================*/
   /* Table: ACTIVIDADES_TEMP                                      */
   /*==============================================================*/
   create table ACTIVIDADES_TEMP (
      OGC_FID                          SERIAL                          not null,
      AA_IDA                           INTEGER                         not null,
      AA_GEOM                          Geometry(Point, 4326)           null,
      AA_DES                           CHARACTER VARYING(100)          null,   
      ID_ELEMENTO                      CHARACTER VARYING(40)           not null,   
      TIPO_ELEM                        CHARACTER VARYING(40)           null,
      ENT_ACTIVIDAD_IDENTIFICADOR      CHARACTER VARYING(13)           null,
      ENT_ACTIVIDAD_NOMBRE_ACT         CHARACTER VARYING(155)          null,
      ENT_EMPRESA_RAZON_SOCIAL         CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_NUMERO_DOCUMENTO   CHARACTER VARYING(21)           null,
      ENT_ACTIVIDAD_DIRECCION          CHARACTER VARYING(255)          null,
      ENT_ACTIVIDAD_REFCAT             CHARACTER VARYING(25)           null,
      ENT_ACTIVIDAD_EPIGRAFE           CHARACTER VARYING(255)          null,
      ENT_ACTIVIDAD_CATEGORIA_1        CHARACTER VARYING(255)          null,
      ENT_ACTIVIDAD_ACT_PRINCIPAL      BOOLEAN                         null,
      ENT_ACTIVIDAD_LOCAL_ALQUILADO    BOOLEAN                         null,
      ENT_ACTIVIDAD_WEB                CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_EMAIL_CONTACTO     CHARACTER VARYING(150)          null,
      ENT_ACTIVIDAD_PERSONA_CONTACTO   CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_TELEFONO_CONT      CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_HORARIO_INVIERNO   CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_HORARIO_VERANO     CHARACTER VARYING(100)          null,
      ENT_ACTIVIDAD_PUBLICABLE         BOOLEAN                         null,
      ENT_ACTIVIDAD_FECHA_ALTA         TIMESTAMP                       null,
      ENT_ACTIVIDAD_FECHA_BAJA         TIMESTAMP                       null,
      ENT_ACTIVIDAD_OTROS_EPIGRAFES    text                            null,      
      constraint PK_ACTIVIDADES_TEMP primary key (OGC_FID)
   );


    /*===================================================================*/
    /* Tabla: INSPECCIONES                                               */
    /* add fields of infraction types                                    */
    /*===================================================================*/
    ALTER TABLE INSPECCIO_MERCAT DROP CONSTRAINT FK_INSPECCI_REFERENCE_MERCAT;
    ALTER TABLE INSPECCIO_MERCAT RENAME TO INSPECCIONES;
    
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_015 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_016 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_017 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_018 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_019 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_020 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_021 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_022 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_023 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_024 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_025 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_026 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_027 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_028 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_029 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_030 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_031 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_032 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_033 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_034 BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_INF_035 BOOLEAN;

    ALTER TABLE INSPECCIONES RENAME COLUMN ENT_UID TO ENT_MERCADOS_UID;
    ALTER TABLE INSPECCIONES ALTER COLUMN ENT_MERCADOS_UID DROP NOT NULL;

    ALTER TABLE INSPECCIONES ADD COLUMN ENT_TERRAZAS_UID CHARACTER(36);
    ALTER TABLE INSPECCIONES ADD COLUMN ENT_OBRAS_UID CHARACTER(36);
    ALTER TABLE INSPECCIONES ADD COLUMN ENT_OVP_UID CHARACTER(36);
    ALTER TABLE INSPECCIONES ADD COLUMN ENT_ACTIVIDADES_UID CHARACTER(36);
    
    ALTER TABLE INSPECCIONES ADD COLUMN INS_PDF_GENERADO BOOLEAN;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_JSON_GENERADO BOOLEAN;

    ALTER TABLE INSPECCIONES ADD COLUMN TIPO_ENTIDAD_ID INTEGER;  
    UPDATE INSPECCIONES SET TIPO_ENTIDAD_ID = 1; --por si hay alguna inspeccion de mercados ya hecha. (solo puede haber de mercados)

    ALTER TABLE INSPECCIONES ADD COLUMN INS_TERRAZAS_MESAS INTEGER;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_TERRAZAS_SILLAS INTEGER;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_TERRAZAS_SUPERFICIE NUMERIC;
    ALTER TABLE INSPECCIONES ADD COLUMN INS_TERRAZAS_OTROS CHARACTER VARYING(255);
    ALTER TABLE INSPECCIONES ADD COLUMN INS_OBRAS_ESTADO CHARACTER VARYING(255);            

    ALTER TABLE INSPECCIONES ADD CONSTRAINT FK_INSPECCIONES_TIPOSENTIDAD FOREIGN KEY(TIPO_ENTIDAD_ID) REFERENCES TIPOS_ENTIDAD(TIPO_ENTIDAD_ID);
    ALTER TABLE INSPECCIONES ADD CONSTRAINT FK_INSPECCIONES_MERCADOS FOREIGN KEY(ENT_MERCADOS_UID) REFERENCES MERCADOS(ENT_UID);
    ALTER TABLE INSPECCIONES ADD CONSTRAINT FK_INSPECCIONES_TERRAZAS FOREIGN KEY(ENT_TERRAZAS_UID) REFERENCES TERRAZAS(ENT_UID);
    ALTER TABLE INSPECCIONES ADD CONSTRAINT FK_INSPECCIONES_OBRAS FOREIGN KEY(ENT_OBRAS_UID) REFERENCES OBRAS(ENT_UID);
    ALTER TABLE INSPECCIONES ADD CONSTRAINT FK_INSPECCIONES_OVP FOREIGN KEY(ENT_OVP_UID) REFERENCES OVP(ENT_UID);
    ALTER TABLE INSPECCIONES ADD CONSTRAINT FK_INSPECCIONES_ACTIVIDADES FOREIGN KEY(ENT_ACTIVIDADES_UID) REFERENCES ACTIVIDADES(ENT_UID);
    ALTER TABLE INSPECCIONES ADD CONSTRAINT FK_TIPOS_ESTADOS_INSPECCIONES FOREIGN KEY(INS_FINALIZADA) REFERENCES TIPOS_ESTADOS_INSP(ESTADO_ID);
    


EXCEPTION WHEN others THEN   
    RAISE EXCEPTION '% - %', SQLERRM, SQLSTATE;

END
$$
LANGUAGE plpgsql;