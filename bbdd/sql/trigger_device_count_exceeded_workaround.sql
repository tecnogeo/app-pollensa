
--=======================================
--Workaround error max count device exceeded
--EJECUTAR EN TODAS LAS BBDD SMAPPE_{{TENANT}}


--Eliminar registros tabla sx_device
delete from sx_device;

--Crear trigger
CREATE OR REPLACE FUNCTION device_count_exceeded_workaround() RETURNS TRIGGER AS $BODY$
   BEGIN
      --delete register after insert
      DELETE FROM sx_device;
      RETURN NEW;
   END;
$BODY$ LANGUAGE plpgsql;


CREATE TRIGGER device_count_exceeded_workaround 
	AFTER INSERT ON sx_device
    FOR EACH ROW EXECUTE PROCEDURE device_count_exceeded_workaround();


--=======================================
-- Un cop actualitzat i que la llicència la tinguem en .json
-- caldrà eliminar el trigger
DROP TRIGGER IF EXISTS device_count_exceeded_workaround ON sx_device CASCADE;


--Provat a sitdesenvol.absis.es a les BBDD:
-- SMAPPE_CALAFELL_DESENVOL
-- SMAPPE_CALAFELL_CERTEC
-- SMAPPE_INI_CT
-- SMAPPE_DATA