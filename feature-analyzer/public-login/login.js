(function () {

    // var authUrl = 'https://sitdesenvol.absis.es/api/v1/oauth2/token';
    // var targetPage = 'https://sitdesenvol.absis.es/BrowserClient/';
    var authUrl = 'https://{{SERVIDOR_MAPPENTERPRISE}}/api/v1/oauth2/token';
    var targetPage = 'https://{{SERVIDOR_MAPPENTERPRISE}}/AppEngine/';
	
    var userName = 'public';
    var password = 'public$123';
	


    var queryDict = {};
    var output = $('#h3_out');
    

    function login(header, clientId) {
        return $.ajax({
            url: authUrl,
            headers: header,
            data: { grant_type: 'password', username: userName, password: password, client_id: clientId },
            type: 'POST'
        }).then(redirect).catch(showError);
    }

    function redirect(token) {
        output.text('Login successful, redirecting in 3 seconds..')
        token.expiration_date = Date.now() + (token.expires_in * 1000);
        window.localStorage.setItem('apps_storage', JSON.stringify(token));

        setTimeout(function() {
            // window.location = targetPage + '#id=' + queryDict['id'] + '&tenant=' + queryDict['tenant'];
			window.location = targetPage + '#appId=' + queryDict['id'] + '&tenant=' + queryDict['tenant'];
        }, 3000);
    }

    function showError(error) {
		
		console.log(error);
        output.text(error);
    }

    $(function() {
       
         location.search.substr(1).split("&").forEach(function (item) { 
             queryDict[item.split("=")[0]] = item.split("=")[1] 
         });
		
        
        login(queryDict, 'App');
    });
})();