
BLInspect 1.6.0 (07/10/2022)
-----------------------------------	
    -[2022/10/07_ERF]  Nueva versión sin cambios.

BLInspect 1.5.0 (07/07/2022)
-----------------------------------	
    -[2022/07/07_ERF]  Adaptación lenguaje inclusivo


BLInspect 1.4.0 (23/11/2021)
-----------------------------------
    -[2021/11/23_ERF]  Creación app semilla para BL.Urban Surveys