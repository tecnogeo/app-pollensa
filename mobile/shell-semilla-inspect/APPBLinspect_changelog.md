
BLInspect 1.6.0 (07/07/2022)
-----------------------------------	
    -[2022/10/07_ERF]  Integración firma biometrica con VidSigner.
    -[2022/10/07_ERF]  Añadido campo exp_codigo_interno.

BLInspect 1.5.0 (07/07/2022)
-----------------------------------	
    -[2022/07/07_ERF]  Añadidos atributos adicionales genericos en el formulario de la entidad
    -[2022/07/07_ERF]  Añadido atributo exp_refcat_otras en el formulario de la entidad
    -[2022/07/07_ERF]  Adaptación lenguaje inclusivo

BLInspect 1.4.0 (23/11/2021)
-----------------------------------
    -[2021/11/23_ERF]  Creación app semilla para BL.Urban Surveys

BLInspect 1.3.0 (XX/XX/2020)
-----------------------------------	
    -[2021/04/19_ERF]  Agrupación atributos entidadForm.xaml, nuevo campo entidad obras, corregida fecha en listado de inspecciones.
    -[2021/05/10_ERF]  Afegides inspeccions d'ofici

BLInspect 1.2.0 (05/08/2020)
-----------------------------------	
    -[2020/08/05_ERF]  Bug 56167. Corregido StartupScript en InspeccionEntidadForm.xaml.
    -[2020/05/20_ERF]  Se distribuye una app semilla que sirve para configurar la APP de cada entidad: MERCADOS, TERRAZAS, OBRAS, OVP, ACTIVIDADES.
    -[2020/05/01_ERF]  Refactor ficheros para que sea genérico.
    -[2020/04/01_ERF]  Pasamos a tener 2 listados: uno para las entidades actuales y otro para el histórico. Se evalua el campo ent_vigencia => actuales=1, histórico=0. Bug 53618.	


BLInspect 1.1.00 (23/10/2019)
-----------------------------------	
    -[2019/10/23_ERF]  No hay cambios. Se itera versión por cambios en la BD.


BLInspect 1.0.00 (01/07/2019)
-----------------------------------	
    -[2019/07/01_ERF]  Versión inicial entidad mercados.