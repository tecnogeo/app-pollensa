/*version 1.3.0*/

module.exports = {
  apps : [{
    name: 'bl-data-apirest',
    script: 'node_modules/bl-data-apirest/bin/www',
    args: 'one two',    
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
		"NODE_ENV": "development",
		"PORT": 3004,
		"LOG_LEVEL": "debug",
		"LOG_PATH": "c:/reporting-service/logs",
		"CONFIG_PATH" : "c:/reporting-service/config/db/",
		"SERVICE_URLPDF" : "https://sitdesenvol.absis.es/bl-pdf-puppeter/api/urlpdf?url=",
		"SERVICE_TEMPLATE" : "https://sitdesenvol.absis.es/bl-html-templates/template/",
    "SERVICE_PDF" : "https://sitdesenvol.absis.es/bl-html-templates/pdf/",
    "SERVICE_APIREST" : "https://sitdesenvol.absis.es/bl-data-apirest/"
    },
    env_production: {
      NODE_ENV: 'production'
    }
  },
  {
    name: 'bl-html-templates',
    script: 'node_modules/bl-html-templates/server.js',    
    args: 'one two',    
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
		"NODE_ENV": "development",
		"PORT": 3001,
		"LOG_LEVEL": "debug",
		"LOG_PATH": "c:/reporting-service/logs",
		"ICONS_PATH": "c:/reporting-service/config/public/"
    },
    env_production: {
      NODE_ENV: 'production'
    }
  },
  {
    name: 'bl-reporting-manager',
    script: 'node_modules/bl-reporting-manager/app.js',    
    args: 'one two',    
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
		"NODE_ENV": "development",
		"PORT": 3003,
		"LOG_LEVEL": "debug",
		"LOG_PATH": "c:/reporting-service/logs",
		"CONFIG_PATH": "c:/reporting-service/config/manager/"
    },
    env_production: {
      NODE_ENV: 'production'
    }
  },
  {
    name: 'bl-pdf-puppeteer',
    script: 'bl-pdf-puppeteer/example/index.js',    
    args: 'one two',    
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
		"NODE_ENV": "development",
		"PORT": 3000
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],
  deploy : { /*
    production : {
      user : 'administrador',
      host : '192.168.17.62',
      //ref  : 'origin/master',
      //repo : 'https://elisaruiz@bitbucket.org/tecnogeo/bl-data-apirest.git',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    } */
  }
};
